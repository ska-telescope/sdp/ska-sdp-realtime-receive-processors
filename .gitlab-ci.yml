# GitLab CI in conjunction with GitLab Runner can use Docker Engine to test and build any application.
# Docker, when used with GitLab CI, runs each job in a separate and isolated container using the predefined image that is set up in .gitlab-ci.yml.
# In this case we use the latest python docker image to build and test this project.
image: $SKA_PYTHON_PYTANGO_BUILDER_IMAGE
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  CI_POETRY_VERSION: 1.8.2

stages:
  - build
  - docs
  - test
  - lint
  - publish
  - pages
  - scan
  - release

include:
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/rules.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/python.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/oci-image.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/docs.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/finaliser.gitlab-ci.yml'
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/release.gitlab-ci.yml'

python-test:
  services:
    # Keep service in sync with docker/test-services.docker-compose.yml
    - alias: kafka
      # Keep image in sync with Kafka version used by Kafka Chart included by SDP under
      # https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/blob/master/charts/ska-sdp/Chart.yaml
      name: bitnami/kafka:3.6.2-debian-12-r3
      # Based off https://github.com/bitnami/containers/tree/main/bitnami/kafka#apache-kafka-development-setup-example
      variables:
        ALLOW_PLAINTEXT_LISTENER: "yes"
        KAFKA_CFG_ADVERTISED_LISTENERS: PLAINTEXT://localhost:9092
        KAFKA_CFG_AUTO_CREATE_TOPICS_ENABLE: "true"
        KAFKA_CFG_CONTROLLER_QUORUM_VOTERS: 0@kafka:9093
        KAFKA_CFG_CONTROLLER_LISTENER_NAMES: CONTROLLER
        KAFKA_CFG_LISTENERS: PLAINTEXT://:9092,CONTROLLER://:9093
        KAFKA_CFG_LISTENERS_SECURITY_PROTOCOL_MAP: CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT
        KAFKA_CFG_NODE_ID: 0
        KAFKA_CFG_PROCESS_ROLES: controller,broker
        BITNAMI_DEBUG: "yes"
    - alias: etcd
      name: artefact.skao.int/ska-sdp-etcd:3.5.9
      command:
        - /usr/bin/etcd
        - "--advertise-client-urls=http://0.0.0.0:2379"
        - "--listen-client-urls=http://0.0.0.0:2379"
        - "--initial-advertise-peer-urls=http://0.0.0.0:2380"
        - "--listen-peer-urls=http://0.0.0.0:2380"
        - "--initial-cluster=default=http://0.0.0.0:2380"
  variables:
    # Needed to allow kafka & zookeeper to talk to each other
    # See https://docs.gitlab.com/runner/configuration/feature-flags.html
    FF_NETWORK_PER_BUILD: "true"
    # To debug the above services, uncomment the below line. For more info see
    # https://docs.gitlab.com/ee/ci/services/#capturing-service-container-logs
    CI_DEBUG_SERVICES: "true"

python-benchmark:
  extends: python-test
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_PIPELINE_SOURCE != "schedule"
      when: manual
      # Don't block the rest of the pipeline
      allow_failure: true
  script:
    - make python-benchmark
    - make python-benchmark-plot

    # Overrides for docs-build-rtd
docs-build-rtd:
  
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: always

# update using skart if requested
skart-update:
  stage: .pre
  tags:
    - k8srunner
  rules:
    - if: '$DO_SKART_UPDATE'
      when:  on_success
    - when: never
  before_script:
    - pip3 install --upgrade --index-url=https://artefact.skao.int/repository/pypi-internal/simple skart
    - poetry config virtualenvs.create false
  script:
    - make deps-update
  artifacts:
    paths:
      - poetry.lock
      - pyproject.toml 