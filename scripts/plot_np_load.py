import matplotlib.pyplot as plt
import numpy as np
from common_plot import parse_plot_args

args = parse_plot_args()
runs = args.runs

fig, axs = plt.subplots(ncols=len(runs), tight_layout=True, sharey=True)
for i, r in enumerate(runs):
    ax: plt.Axes = axs[i] if len(runs) > 1 else axs

    benchmarks = r.query_results(
        "np.load",
        get_name=lambda d: d["params"]["arr_size"],
    )
    arr_sizes = np.array(list(b.get_param("arr_size") for b in benchmarks))
    medians = np.array(list(b.get_stat("median") for b in benchmarks))
    q1s = np.array(list(b.get_stat("q1") for b in benchmarks))
    q3s = np.array(list(b.get_stat("q3") for b in benchmarks))
    ax.errorbar(
        arr_sizes,
        medians * 1e6,
        yerr=np.array([medians - q1s, q3s - medians]) * 1e6,
    )

    ax.set_title(r.name)
    ax.set_xlabel("Array size")
    ax.set_ylabel("Microseconds")

fig.suptitle("np.load() time")

if args.output:
    plt.savefig(args.output)
else:
    plt.show()
