import matplotlib.pyplot as plt
from common_plot import parse_plot_args
from matplotlib import cm

args = parse_plot_args()
runs = args.runs

fig, axs = plt.subplots(
    ncols=len(runs),
    figsize=(6 * len(runs), 4),
    subplot_kw={"projection": "3d"},
)
for i, r in enumerate(runs):
    ax: plt.Axes = axs[i] if len(runs) > 1 else axs

    benchmarks = r.query_results("aiokafka")
    X = list(b.get_param("message_count") for b in benchmarks)
    Y = list(b.get_param("message_size") for b in benchmarks)
    Z = list(
        b.get_param("message_count")
        * b.get_param("message_size")
        / b.get_stat("median")
        for b in benchmarks
    )
    ax.plot_trisurf(X, Y, Z, cmap=cm.coolwarm, linewidth=0, antialiased=True)
    ax.set_title(r.name)
    ax.set_xlabel("Message count")
    ax.set_ylabel("Message size")
    ax.set_zlabel("Bytes per second")

fig.suptitle("aiokafka() throughput (Bytes/sec)")

if args.output:
    plt.savefig(args.output)
else:
    plt.show()
