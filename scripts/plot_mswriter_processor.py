import matplotlib.pyplot as plt
from common_plot import parse_plot_args

args = parse_plot_args()
runs = args.runs


def get_label(benchmark_params: dict | None):
    if benchmark_params is None:
        return "Baseline"
    else:
        return benchmark_params["data_batch_size"]


fig, axs = plt.subplots(
    nrows=len(runs),
    figsize=(8, 4 * len(runs)),
    tight_layout=True,
    sharey=True,
)
for i, r in enumerate(runs):
    ax: plt.Axes = axs[i] if len(runs) > 1 else axs

    benchmarks = r.query_results(
        "mswriter_pointings",
        param_filter=lambda p: p is None or p["ob_time_min"] == 5,
        get_name=lambda d: get_label(d["params"]),
    )
    stats = list(b.to_bxpstats() for b in benchmarks)
    ax.bxp(stats, showfliers=False)
    ax.set_title(r.name)
    ax.set_xlabel("Pointing batch size")
    ax.set_ylabel("Sec.")

fig.suptitle("MSWriterProcessor: AA0.5, 5min @ 10Hz")

if args.output:
    plt.savefig(args.output)
else:
    plt.show()
