import argparse
import json
import logging
from dataclasses import dataclass
from os.path import getmtime
from pathlib import Path
from typing import Callable

log = logging.getLogger(__name__)


class BenchmarkRun:
    """Represents a single invocation of ``make python-benchmark``"""

    def __init__(self, path: Path | str, name: str | None = None) -> None:
        if isinstance(path, str):
            path = Path(path)

        self.path = path
        with open(str(path), encoding="utf-8") as f:
            self._data = json.load(f)

        self.name = name

    @property
    def results(self):
        """Raw benchmark data"""
        return self._data["benchmarks"]

    def query_results(
        self,
        group: str,
        param_filter: Callable[[dict | None], bool] = lambda _: True,
        get_name: Callable[[dict], str] = lambda b: b["name"],
    ):
        """
        Query the results in this run by group, and optionally by
        the value of their parameters.
        """
        benchmarks = list(
            BenchmarkResult(get_name(b), b)
            for b in self.results
            if b["group"] == group and param_filter(b["params"])
        )
        if not benchmarks:
            log.error("No benchmarks matching group=%s", group)
            raise RuntimeError

        return benchmarks


class BenchmarkResult:
    """Represents the result of a single benchmark"""

    def __init__(self, name: str, raw_data: dict) -> None:
        self.name = name
        self._raw_data = raw_data

    def get_param(self, name: str):
        """Gets the specified benchmark parameter, or None if not present"""
        return self._raw_data["params"].get(name)

    def get_stat(self, name: str):
        return self._raw_data["stats"].get(name)

    def to_bxpstats(
        self,
        preprocess: Callable[
            [float, "BenchmarkResult"], float
        ] = lambda s, _: s,
    ):
        """
        Convert this result to something that can be plotted using
        matplotlib.Axes.bxp() as a box and whisker plot.
        """
        stats = self._raw_data["stats"]
        return {
            "label": self.name,
            "med": preprocess(stats["median"], self),
            "q1": preprocess(stats["q1"], self),
            "q3": preprocess(stats["q3"], self),
            "whislo": preprocess(stats["ld15iqr"], self),
            "whishi": preprocess(stats["hd15iqr"], self),
        }


@dataclass
class PlotArgs:
    """Arguments for plotting benchmarks"""

    output: str | None
    runs: list[BenchmarkRun]


def parse_plot_args(description: str | None = None):
    """Parse CLI args and return an instance of PlotArgs"""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "--output",
        help="The file to save the plot to, with a matplotlib supported"
        "extension. Not providing will show the plot instead",
    )
    parser.add_argument(
        "runs",
        nargs="*",
        type=lambda r: BenchmarkRun(*reversed(r.split("=", 1))),
        default=[get_latest_benchmark_run()],
        help="Files to plot. Optionally name=file to add a title above "
        "the plot. If not specified will search for the latest run.",
    )

    args = parser.parse_args()
    return PlotArgs(**vars(args))


BENCHMARK_PATH = Path(__file__).parent.parent / ".benchmarks"


def get_latest_benchmark_run():
    """Find the latest benchmark run file by modified date"""
    files = sorted(
        BENCHMARK_PATH.glob("**/*.json"),
        key=getmtime,
        reverse=True,
    )
    latest = next(iter(files))
    log.info("Found latest benchmark run: %s", latest)

    return BenchmarkRun(latest)


# From https://stackoverflow.com/a/1094933
def sizeof_fmt(num, suffix="B"):
    """Format bytes with a human readable suffix"""
    for unit in ("", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"):
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"


N_ANT_TO_AA = {4: "AA0.5", 68: "AA2", 144: "AA*"}


def get_aa_label(num_antennas: int):
    """
    For the given number of antennas, return the appropriate
    Array Assembly name.
    """
    return N_ANT_TO_AA.get(num_antennas, f"Non-std {num_antennas} ant.")
