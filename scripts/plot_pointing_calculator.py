import matplotlib.pyplot as plt
from common_plot import get_aa_label, parse_plot_args

args = parse_plot_args()
runs = args.runs

fig, axs = plt.subplots(
    ncols=len(runs),
    figsize=(6 * len(runs), 4),
    tight_layout=True,
    sharey=True,
)
for i, r in enumerate(runs):
    ax: plt.Axes = axs[i] if len(runs) > 1 else axs

    benchmarks = r.query_results(
        "pointing_calculation",
        param_filter=lambda p: p["ob_time_min"] == 5,
        get_name=lambda d: get_aa_label(d["params"]["n_antennas"]),
    )
    stats = list(b.to_bxpstats() for b in benchmarks)
    ax.bxp(stats, showfliers=False)
    ax.set_title(r.name)
    ax.set_xlabel("Array Assembly")
    ax.set_ylabel("Sec.")

fig.suptitle("Raw pointing calculation time: 5min @ 10Hz")

if args.output:
    plt.savefig(args.output)
else:
    plt.show()
