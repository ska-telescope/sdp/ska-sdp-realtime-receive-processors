import asyncio
import time
from datetime import datetime
from io import BytesIO

import numpy as np
import pytest
import pytest_asyncio
from aiokafka import AIOKafkaProducer
from numpy.testing import assert_allclose
from realtime.receive.core.antenna import Antenna

from realtime.receive.processors.sdp.pointing_helpers import (
    PointingCalculator,
    PointingConsumer,
    PointingKafkaSourceConfig,
    PointingType,
)
from tests.common_kafka import KAFKA_HOST, auto_delete_producer
from tests.common_pointings import STANDARD_POINTING_DTYPE

COMMANDED_TOPIC = "test_commanded"
ACTUAL_TOPIC = "test_actual"
KAFKA_SOURCE = PointingKafkaSourceConfig(
    server=KAFKA_HOST,
    commanded_topic=COMMANDED_TOPIC,
    actual_topic=ACTUAL_TOPIC,
)
ANTENNA_NAME = "ANT001"
ONE_ANTENNA = [
    Antenna(
        station_label=ANTENNA_NAME,
        interface="",
        diameter=12,
        location={},
        fixed_delays=[],
        niao=0,
        station_id=0,
    )
]


@pytest_asyncio.fixture(name="producer")
async def kafka_producer():
    """Kafka producer"""
    async with auto_delete_producer(COMMANDED_TOPIC, ACTUAL_TOPIC) as producer:
        yield producer


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "invalid_fields",
    [
        [("antenna_id", np.str_), ("antenna_name", np.float64)],
        [("ts", np.float64)],
        [("az", np.str_)],
        [("el", np.int32)],
    ],
)
async def test_pointings_with_invalid_dtype_are_dropped(
    invalid_fields: list[tuple[str, np.dtype]],
    producer: AIOKafkaProducer,
    caplog: pytest.LogCaptureFixture,
):
    consumer = PointingConsumer(KAFKA_SOURCE, ONE_ANTENNA)
    await consumer.start(time.time())

    fields = {
        "antenna_name": np.str_,
        "antenna_id": int,
        "ts": "datetime64[ns]",
        "az": np.double,
        "el": np.double,
    }
    for name, dtype in invalid_fields:
        fields[name] = dtype

    await _write_kafka_pointings(
        producer,
        COMMANDED_TOPIC,
        np.zeros(
            1,
            dtype=list(fields.items()),
        ),
    )
    await asyncio.sleep(0.1)
    await consumer.stop()

    assert 0 == consumer.num_samples(0, PointingType.COMMANDED)

    for name, dtype in invalid_fields:
        assert name in caplog.text


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "fields_to_drop", (["antenna_id", "antenna_name"], ["ts"], ["az"], ["el"])
)
async def test_pointings_with_missing_fields_are_dropped(
    fields_to_drop: list[str],
    producer: AIOKafkaProducer,
    caplog: pytest.LogCaptureFixture,
):
    consumer = PointingConsumer(KAFKA_SOURCE, ONE_ANTENNA)
    await consumer.start(time.time())

    dtypes = {
        "antenna_id": int,
        "antenna_name": np.str_,
        "ts": "datetime64[ns]",
        "az": np.double,
        "el": np.double,
    }
    for name in fields_to_drop:
        del dtypes[name]

    await _write_kafka_pointings(
        producer, COMMANDED_TOPIC, np.zeros(1, dtype=list(dtypes.items()))
    )
    await asyncio.sleep(0.1)
    await consumer.stop()

    assert 0 == consumer.num_samples(0, PointingType.COMMANDED)
    assert all(field in caplog.text for field in fields_to_drop)


@pytest.mark.asyncio
@pytest.mark.parametrize("antenna_name", ("", "a", ANTENNA_NAME[:-1], ANTENNA_NAME + "a"))
async def test_unknown_antennae_are_dropped(
    producer: AIOKafkaProducer, antenna_name: str, caplog: pytest.LogCaptureFixture
):
    consumer = PointingConsumer(KAFKA_SOURCE, ONE_ANTENNA)
    await consumer.start(time.time())

    dtypes = {
        "antenna_name": "U10",
        "ts": "datetime64[ns]",
        "az": np.double,
        "el": np.double,
    }
    array = (antenna_name, 0, 0, 0)

    await _write_kafka_pointings(
        producer, COMMANDED_TOPIC, np.array([array], dtype=list(dtypes.items()))
    )
    await asyncio.sleep(0.1)
    await consumer.stop()

    assert 0 == consumer.num_samples(0, PointingType.COMMANDED)
    assert antenna_name in caplog.text


@pytest.mark.asyncio
async def test_known_antenna_is_received(producer: AIOKafkaProducer):
    consumer = PointingConsumer(KAFKA_SOURCE, ONE_ANTENNA)
    await consumer.start(time.time())

    dtypes = {
        "antenna_name": "U10",
        "ts": "datetime64[ns]",
        "az": np.double,
        "el": np.double,
    }
    array = (ANTENNA_NAME, 0, 0, 0)

    await _write_kafka_pointings(
        producer, COMMANDED_TOPIC, np.array([array], dtype=list(dtypes.items()))
    )
    await asyncio.sleep(0.1)
    await consumer.stop()

    assert 1 == consumer.num_samples(0, PointingType.COMMANDED)


async def _write_kafka_pointings(producer: AIOKafkaProducer, topic: str, pointings: np.ndarray):
    bio = BytesIO()
    np.save(bio, pointings)
    await producer.send_and_wait(
        topic,
        bio.getbuffer(),
    )


@pytest.mark.asyncio
async def test_corrupt_pointings_are_dropped(
    producer: AIOKafkaProducer,
    caplog: pytest.LogCaptureFixture,
):
    consumer = PointingConsumer(KAFKA_SOURCE, ONE_ANTENNA)
    await consumer.start(time.time())
    await producer.send_and_wait(ACTUAL_TOPIC, b"corrupt NPY message")
    await asyncio.sleep(0.1)
    await consumer.stop()

    assert 0 == consumer.num_samples(0, PointingType.COMMANDED)
    assert "invalid NPY message" in caplog.text


@pytest.mark.asyncio
async def test_reception_will_be_stopped_once_final_ts_is_reached(producer: AIOKafkaProducer):
    consumer = PointingConsumer(KAFKA_SOURCE, ONE_ANTENNA)
    now = time.time()

    await _write_ts_pointings(producer, [now + offset for offset in range(-10, 0)])
    await consumer.start(now - 5)

    t = asyncio.create_task(consumer.stop(stop_after_ts=now + 5))
    await _write_ts_pointings(producer, [now + offset for offset in range(0, 10)])
    await t

    # First 5 will be skipped on start and last 4 will also be skipped.
    # 5th last will be included as Kafka timestamps only have MS precision and
    # so the lost precision will be truncated and be prior to our exact desired TS
    assert 11 == consumer.num_samples(0, PointingType.ACTUAL)


@pytest.mark.asyncio
async def test_reception_is_stopped_immediately_if_final_ts_has_already_been_reached(
    producer: AIOKafkaProducer,
):
    consumer = PointingConsumer(KAFKA_SOURCE, ONE_ANTENNA)
    now = time.time()

    timestamps = [now + offset for offset in range(-10, 10)]
    await _write_ts_pointings(producer, timestamps)
    await consumer.start(now - 5)

    async def check_expected_num_samples(num_samples: int):
        while consumer.num_samples(0, PointingType.ACTUAL) < num_samples:
            await asyncio.sleep(0)

    await asyncio.wait_for(check_expected_num_samples(15), 1)

    await asyncio.wait_for(consumer.stop(stop_after_ts=now + 5), 0.1)


async def _write_ts_pointings(producer: AIOKafkaProducer, timestamps: list[float]):
    for ts in timestamps:
        dt = datetime.fromtimestamp(ts).isoformat()
        arr = np.array([(0, dt, 0, 0)], dtype=STANDARD_POINTING_DTYPE)
        bio = BytesIO()
        np.save(bio, arr)
        await producer.send_and_wait(ACTUAL_TOPIC, bio.getbuffer(), timestamp_ms=int(ts * 1e3))


@pytest.mark.parametrize("size", [0, 1, 2, 3, 4])
def test_too_few_data_points_skips_generation(size: int):
    calculator = PointingCalculator(1, 5)

    for i in range(size):
        calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(i), 0, 0)

    with pytest.raises(StopIteration):
        next(calculator.calc_entries())


def test_missing_samples_in_one_antenna_doesnt_stop_other_antenna_generation():
    calculator = PointingCalculator(2, 5)

    for i in range(5):
        calculator.add_sample(1, PointingType.ACTUAL, _datetime_ms(i), 0, 0)
        calculator.add_sample(1, PointingType.COMMANDED, _datetime_ms(i), 0, 0)

    entries = list(calculator.calc_entries())
    assert len(entries) == 5
    assert all(e.antenna_id == 1 for e in entries)


def test_missing_source_offsets_skips_antenna():
    calculator = PointingCalculator(2, 5, calculate_source_offset=False)

    for i in range(5):
        calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(i), 0, 0)
        calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(i), 0, 0)
        calculator.add_sample(0, PointingType.SOURCE_OFFSET, _datetime_ms(i), 0.1, 0.1)
        calculator.add_sample(1, PointingType.ACTUAL, _datetime_ms(i), 0, 0)
        calculator.add_sample(1, PointingType.COMMANDED, _datetime_ms(i), 0, 0)

    for i in range(4):
        calculator.add_sample(1, PointingType.SOURCE_OFFSET, _datetime_ms(i), i, i)

    entries = list(calculator.calc_entries())
    assert len(entries) == 5
    assert all(e.antenna_id == 0 for e in entries)
    assert_allclose([e.source_offset for e in entries], 0.1)


def test_duplicate_samples_are_discarded_in_spline():
    calculator = PointingCalculator(1, 5)

    # Unsorted order so that we verify that sorting works as expected
    for i in [1, 0, 2, 4, 3]:
        calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(i), 0, 0)

        # Second should get discarded
        calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(i), 0, 1)
        calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(i), 2, 3)

    entries = list(calculator.calc_entries())
    assert len(entries) == 5
    assert_allclose([e.target[0] for e in entries], 0.0)
    assert_allclose([e.target[1] for e in entries], 1.0)


def test_identical_timestamps_give_original_value():
    calculator = PointingCalculator(1, 5)

    for i in range(5):
        calculator.add_sample(
            0,
            PointingType.ACTUAL,
            _datetime_ms(i),
            i / 10,
            1 + i / 10,
        )
        calculator.add_sample(
            0,
            PointingType.COMMANDED,
            _datetime_ms(i),
            2 + i / 10,
            3 + i / 10,
        )

    pointings = list(calculator.calc_entries())
    assert 5 == len(pointings)

    for idx, pointing in enumerate(pointings):
        assert 0 == pointing.antenna_id
        _assert_ts(idx, pointing.time)
        _assert_azel_equal((idx / 10, 1 + idx / 10), pointing.direction)
        _assert_azel_equal((2.0 + idx / 10, 3.0 + idx / 10), pointing.target)
        _assert_azel_equal((-2.0, -2.0), pointing.source_offset)
        _assert_azel_equal((0.0, 0.0), pointing.pointing_offset)


def test_interpolated_commanded_pointings_are_correct():
    calculator = PointingCalculator(1, 5)
    calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(1), 2.1, 2.2)
    calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(3), 2.3, 2.4)
    calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(5), 2.5, 2.6)
    calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(7), 2.7, 2.8)
    calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(9), 2.9, 3.0)
    calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(11), 3.1, 3.2)
    assert 6 == calculator.length(0, PointingType.ACTUAL)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(2), 0.2, 0.3)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(4), 0.4, 0.5)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(6), 0.6, 0.7)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(8), 0.8, 0.9)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(10), 1.0, 1.1)
    assert 5 == calculator.length(0, PointingType.COMMANDED)

    pointings = list(calculator.calc_entries())
    assert 6 == len(pointings)

    # extrapolation takes the boundary value
    _assert_ts(1, pointings[0].time)
    _assert_azel_equal((2.1, 2.2), pointings[0].direction)
    _assert_azel_equal((0.2, 0.3), pointings[0].target)

    _assert_ts(3, pointings[1].time)
    assert 0.2 <= pointings[1].target[0] <= 0.4
    assert 0.3 <= pointings[1].target[1] <= 0.5
    _assert_ts(5, pointings[2].time)
    assert 0.4 <= pointings[2].target[0] <= 0.6
    assert 0.5 <= pointings[2].target[1] <= 0.7
    _assert_ts(7, pointings[3].time)
    assert 0.6 <= pointings[3].target[0] <= 0.8
    assert 0.7 <= pointings[3].target[1] <= 0.9
    _assert_ts(9, pointings[4].time)
    assert 0.8 <= pointings[4].target[0] <= 1.0
    assert 0.9 <= pointings[4].target[1] <= 1.1

    # extrapolation takes the boundary value
    _assert_ts(11, pointings[5].time)
    _assert_azel_equal((3.1, 3.2), pointings[5].direction)
    _assert_azel_equal((1.0, 1.1), pointings[5].target)


def test_interpolated_source_offset_pointings_are_correct():
    calculator = PointingCalculator(1, 5, calculate_source_offset=False)
    for i in range(1, 13, 2):
        p = 2 + i / 10
        calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(i), p, p + 0.1)
    assert 6 == calculator.length(0, PointingType.ACTUAL)
    for i in range(2, 12, 2):
        p = i / 10
        calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(i), p, p + 0.1)
    assert 5 == calculator.length(0, PointingType.COMMANDED)

    calculator.add_sample(0, PointingType.SOURCE_OFFSET, _datetime_ms(2), 5.0, 5.5)
    calculator.add_sample(0, PointingType.SOURCE_OFFSET, _datetime_ms(4), 5.1, 5.6)
    calculator.add_sample(0, PointingType.SOURCE_OFFSET, _datetime_ms(6), 5.2, 5.7)
    calculator.add_sample(0, PointingType.SOURCE_OFFSET, _datetime_ms(8), 5.3, 5.8)
    calculator.add_sample(0, PointingType.SOURCE_OFFSET, _datetime_ms(10), 5.4, 5.9)
    assert 5 == calculator.length(0, PointingType.SOURCE_OFFSET)

    pointings = list(calculator.calc_entries())
    assert 6 == len(pointings)

    # extrapolation takes the boundary value
    _assert_ts(1, pointings[0].time)
    _assert_azel_equal((5.0, 5.5), pointings[0].source_offset)

    _assert_ts(3, pointings[1].time)
    assert 5.0 <= pointings[1].source_offset[0] <= 5.1
    assert 5.5 <= pointings[1].source_offset[1] <= 5.6
    _assert_ts(5, pointings[2].time)
    assert 5.1 <= pointings[2].source_offset[0] <= 5.2
    assert 5.6 <= pointings[2].source_offset[1] <= 5.7
    _assert_ts(7, pointings[3].time)
    assert 5.2 <= pointings[3].source_offset[0] <= 5.3
    assert 5.7 <= pointings[3].source_offset[1] <= 5.8
    _assert_ts(9, pointings[4].time)
    assert 5.3 <= pointings[4].source_offset[0] <= 5.4
    assert 5.8 <= pointings[4].source_offset[1] <= 5.9

    # extrapolation takes the boundary value
    _assert_ts(11, pointings[5].time)
    _assert_azel_equal((5.4, 5.9), pointings[5].source_offset)


def test_non_increasing_timestamps_are_handled_correctly():
    calculator = PointingCalculator(1, 5)
    calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(1), 2.1, 2.2)
    calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(5), 2.3, 2.4)
    assert 2 == calculator.length(0, PointingType.ACTUAL)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(1), 0.1, 1.1)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(3), 0.3, 1.3)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(2), 0.2, 1.2)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(5), 0.5, 1.5)
    calculator.add_sample(0, PointingType.COMMANDED, _datetime_ms(4), 0.4, 1.4)
    assert 5 == calculator.length(0, PointingType.COMMANDED)

    pointings = list(calculator.calc_entries())
    assert 2 == len(pointings)

    _assert_azel_equal((2.1, 2.2), pointings[0].direction)
    _assert_azel_equal((0.1, 1.1), pointings[0].target)
    _assert_azel_equal((2.3, 2.4), pointings[1].direction)
    _assert_azel_equal((0.5, 1.5), pointings[1].target)


def test_data_arrays_automatically_grow():
    calculator = PointingCalculator(1, 2)
    assert 2 == calculator.capacity

    for i in range(2):
        calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(i), 0, 0)
        assert 2 == calculator.capacity

    for i in range(2):
        calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(i + 2), 0, 0)
        assert 4 == calculator.capacity

    for i in range(4):
        calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(i + 4), 0, 0)
        assert 8 == calculator.capacity

    calculator.add_sample(0, PointingType.ACTUAL, _datetime_ms(16), 0, 0)
    assert 16 == calculator.capacity


def _datetime_ms(millisec: int):
    return np.datetime64("2023-03-21T15:00:00", "ns") + np.timedelta64(millisec, "ms")


def _assert_ts(ms_time: int, unix_ts: float):
    expected_datetime = datetime(2023, 3, 21, 15, 0, 0, int(ms_time * 1e3))
    assert expected_datetime == datetime.utcfromtimestamp(unix_ts)


def _assert_azel_equal(expected: tuple[float, float], actual: tuple[float, float]):
    # Since the commanded/target pointings go through the interpolation
    # spline, we are subject to floating point precision issues
    # So just make sure we're close enough
    __tracebackhide__ = True
    assert_allclose(actual, expected)
