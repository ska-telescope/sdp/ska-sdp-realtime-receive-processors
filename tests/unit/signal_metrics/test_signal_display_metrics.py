"""Tests for the Plasma to Metrics class"""

# pylint: disable=abstract-class-instantiated,protected-access

import gzip
import logging
import os
from dataclasses import dataclass
from datetime import date
from pathlib import Path
from time import sleep, time
from unittest.mock import AsyncMock, MagicMock, call, patch

import msgpack
import msgpack_numpy
import pytest
from ska_sdp_config.entity.common import KafkaUrl
from ska_sdp_dataqueues import Encoding
from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricDataTypes

from realtime.receive.processors.sdp.metrics.signal_display_metrics import SignalDisplayMetrics
from realtime.receive.processors.sdp.metrics.utils.config import ALL_METRICS
from realtime.receive.processors.sdp.metrics.utils.flow import SignalDisplayFlow
from realtime.receive.processors.sdp.metrics.utils.utilities import create_id, metric_to_str
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline
from tests.test_utils import untar

msgpack_numpy.patch()
PLASMA_SOCKET = "/tmp/plasma"
logger = logging.getLogger(__name__)

TEST_DATA = {
    "mid": {
        "calls": {
            "stats": 53,
            MetricDataTypes.SPECTRUM: 45,
            MetricDataTypes.PHASE: 45,
            MetricDataTypes.AMPLITUDE: 45,
            MetricDataTypes.LAG_PLOT: 45,
            MetricDataTypes.BAND_AVERAGED_X_CORR: 45,
            MetricDataTypes.UV_COVERAGE: 45,
        }
    },
    "low": {
        "calls": {
            "stats": 209,
            MetricDataTypes.SPECTRUM: 201,
            MetricDataTypes.PHASE: 201,
            MetricDataTypes.AMPLITUDE: 201,
            MetricDataTypes.LAG_PLOT: 201,
            MetricDataTypes.BAND_AVERAGED_X_CORR: 201,
            MetricDataTypes.UV_COVERAGE: 201,
        }
    },
}


def _get_data(telescope, metric_type):
    file_name = f"{telescope}.{metric_to_str(metric_type)}.gz"
    path = os.path.join("tests", "unit", "signal_metrics", "data", "tests", file_name)
    with gzip.open(path, mode="r") as infile:
        return msgpack.unpack(infile)


def _clean_data(metric_type: str, input_data: dict) -> dict:
    if metric_type == "stats":
        del input_data["data"]["time_since_last_payload"]
        del input_data["data"]["time"]
    else:
        del input_data["data"]["timestamp"]

    return input_data


def _validate_sent_data(telescope, metric_type, input_data):
    # convert to Python objects
    data = [_clean_data(metric_type, d) for d in input_data if "data" in d]
    correct_data = _get_data(telescope, metric_type)

    # with open(f"{telescope}.{metric_to_str(metric_type)}.msgpack", "wb") as f:
    #     f.write(msgpack.packb(data))

    check_length = len(data) == len(correct_data)
    assert check_length, "Processed data doesn't match length check"
    check_sameness = data == correct_data
    if not check_sameness:
        for index, item in enumerate(data):
            check_item = item == correct_data[index]
            assert check_item, f"Item {index} doesn't match"
    assert check_sameness, "Processed data doesn't match"


# pylint: disable=protected-access,unnecessary-dunder-call
SRC_FUNCTION = "SignalDisplayMetrics"


@dataclass
class ProcessingBlock:
    """Fake config return for processing block get."""

    eb_id: str


@dataclass
class ExecutionBlock:
    """Fake config return for execution block get."""

    subarray_id: str


@dataclass
class FlowKey:
    """Fake config return for flow keys get."""

    pb_id: str
    kind: str
    name: str


@dataclass
class Sink:
    """Fake config return for flow sink."""

    topics: str
    host: str
    format: str


@dataclass
class Source:
    """Fake config return for flow source."""

    function: str
    parameters: dict


@dataclass
class Flow:
    """Fake config return for flow get."""

    key: FlowKey
    sources: list[Source]
    sink: Sink


@patch("realtime.receive.processors.sdp.metrics.signal_display_metrics.DataQueueProducer")
@patch("realtime.receive.processors.sdp.metrics.signal_display_metrics.ska_sdp_config")
def test_init_basic(mock_config, mock_producer, monkeypatch):
    """Test the basic creation"""
    monkeypatch.setenv("SDP_PB_ID", "pb-test-123")
    monkeypatch.setenv("SDP_SUBARRAY_ID", "01")
    processor = SignalDisplayMetrics()

    assert mock_config.mock_calls == [
        call.Config(),
        call.Config().txn(),
        call.Config().txn().__iter__(),
        call.Config().txn(),
        call.Config().txn().__iter__(),
        call.Config().watcher(),
        call.Config().watcher().__iter__(),
    ]
    assert mock_producer.mock_calls == []

    assert processor.enable_stats is True
    assert processor.received_payloads == 0
    assert processor.received_time_slices == 0
    assert processor._processing_block_id == "pb-test-123"
    assert processor._execution_block_id == "eb-unknown"
    assert processor._subarray_id == "01"
    assert processor._scan_id == 0
    assert processor._disable_kafka is False
    assert processor._ignore_config_db is False
    assert processor.config == mock_config.Config.return_value
    assert processor._producers == {}
    assert processor.time_since_last_payload <= time()


@patch("realtime.receive.processors.sdp.metrics.signal_display_metrics.DataQueueProducer")
@patch("realtime.receive.processors.sdp.metrics.signal_display_metrics.ska_sdp_config")
def test_init_disable_network(mock_config, mock_producer):
    """Test the offline creation"""
    processor = SignalDisplayMetrics(random_ids=True, disable_kafka=True, ignore_config_db=True)

    assert mock_config.mock_calls == []
    assert mock_producer.mock_calls == [
        call(server="localhost:9092", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]

    today = date.today().strftime("%Y%m%d")

    assert processor.enable_stats is True
    assert processor.received_payloads == 0
    assert processor.received_time_slices == 0
    assert processor._processing_block_id.startswith(f"pb-fake-{today}")
    assert processor._execution_block_id.startswith(f"eb-fake-{today}")
    assert processor._subarray_id == "01"
    assert processor._scan_id == 0
    assert processor._disable_kafka is True
    assert processor._ignore_config_db is True
    assert processor.config is None
    assert processor._producers == {KafkaUrl("kafka://localhost:9092"): mock_producer.return_value}
    assert processor.time_since_last_payload <= time()


@patch("realtime.receive.processors.sdp.metrics.signal_display_metrics.DataQueueProducer")
@patch("realtime.receive.processors.sdp.metrics.signal_display_metrics.ska_sdp_config")
def test_init_env(mock_config, mock_producer, monkeypatch):
    """Test the basic creation with config DB access (most common creation)"""

    transaction = MagicMock()
    transaction.processing_block.get.return_value = ProcessingBlock(eb_id="eb-test-fake")
    transaction.execution_block.get.return_value = ExecutionBlock(subarray_id="99")

    mock_config.Config.return_value.txn.return_value.__iter__.return_value = [transaction]

    monkeypatch.setenv("SDP_PB_ID", "pb-test-123")
    monkeypatch.setenv("SDP_SUBARRAY_ID", "01")
    processor = SignalDisplayMetrics()

    assert mock_config.mock_calls == [
        call.Config(),
        call.Config().txn(),
        call.Config().txn().__iter__(),
        call.Config().txn(),
        call.Config().txn().__iter__(),
        call.Config().watcher(),
        call.Config().watcher().__iter__(),
    ]
    assert mock_producer.mock_calls == []
    assert transaction.mock_calls == [
        call.processing_block.get("pb-test-123"),
        call.execution_block.get("eb-test-fake"),
        call.flow.list_keys(),
        call.flow.list_keys().__iter__(),
    ]

    assert processor.enable_stats is True
    assert processor.received_payloads == 0
    assert processor.received_time_slices == 0
    assert processor._processing_block_id == "pb-test-123"
    assert processor._execution_block_id == "eb-test-fake"
    assert processor._subarray_id == "99"
    assert processor._scan_id == 0
    assert processor._disable_kafka is False
    assert processor._ignore_config_db is False
    assert processor.config == mock_config.Config.return_value
    assert processor._producers == {}
    assert processor.time_since_last_payload <= time()


@patch("realtime.receive.processors.sdp.metrics.signal_display_metrics.ska_sdp_config")
def test_get_flows(mock_config, monkeypatch):
    """Test the basic creation with config DB access (most common creation)"""

    transaction = MagicMock()
    transaction.processing_block.get.return_value = ProcessingBlock(eb_id="eb-test-fake")
    transaction.execution_block.get.return_value = ExecutionBlock(subarray_id="99")

    flow_key = FlowKey(pb_id="pb-test-456", kind="data-queue", name="metrics-spectrum-01")

    transaction.flow.list_keys.return_value = [flow_key]
    transaction.flow.get.return_value = Flow(
        key=flow_key,
        sources=[
            Source(
                function=SRC_FUNCTION,
                parameters={
                    "metric_type": "spectrum",
                    "nchan_avg": 5,
                    "additional_windows": 5,
                    "rounding_sensitivity": 7,
                },
            )
        ],
        sink=Sink(
            topics="metrics-spectrum-01",
            host=KafkaUrl("kafka://host:9092"),
            format="msgpack_numpy",
        ),
    )

    mock_config.Config.return_value.txn.return_value.__iter__.return_value = [transaction]

    monkeypatch.setenv("SDP_PB_ID", "pb-test-456")
    monkeypatch.setenv("SDP_SUBARRAY_ID", "99")
    processor = SignalDisplayMetrics(metrics=["spectrum"])
    assert processor._processing_block_id == "pb-test-456"
    assert processor._execution_block_id == "eb-test-fake"
    assert processor._subarray_id == "99"

    processor._flow_watcher()
    sleep(1)

    assert processor.flows == {
        "spectrum": [
            SignalDisplayFlow(
                name="metrics-spectrum-01",
                topic="metrics-spectrum-01",
                host=KafkaUrl("kafka://host:9092"),
                data_format="msgpack_numpy",
                metric_type="spectrum",
                nchan_avg=5,
                additional_windows=5,
                rounding_sensitivity=7,
                flow=Flow(
                    key=FlowKey(
                        pb_id="pb-test-456", kind="data-queue", name="metrics-spectrum-01"
                    ),
                    sources=[
                        Source(
                            function=SRC_FUNCTION,
                            parameters={
                                "metric_type": "spectrum",
                                "nchan_avg": 5,
                                "additional_windows": 5,
                                "rounding_sensitivity": 7,
                            },
                        )
                    ],
                    sink=Sink(
                        topics="metrics-spectrum-01",
                        host=KafkaUrl("kafka://host:9092"),
                        format="msgpack_numpy",
                    ),
                ),
                state=None,
            )
        ]
    }
    assert transaction.mock_calls == [
        call.processing_block.get("pb-test-456"),
        call.execution_block.get("eb-test-fake"),
        call.flow.list_keys(),
        call.flow.get(FlowKey(pb_id="pb-test-456", kind="data-queue", name="metrics-spectrum-01")),
        call.flow.list_keys(),
        call.flow.get(FlowKey(pb_id="pb-test-456", kind="data-queue", name="metrics-spectrum-01")),
    ]


@patch("realtime.receive.processors.sdp.metrics.signal_display_metrics.ska_sdp_config")
def test_get_flows_spectral_range(mock_config, monkeypatch):
    """Test the basic creation with config DB access (most common creation)"""

    transaction = MagicMock()
    transaction.processing_block.get.return_value = ProcessingBlock(eb_id="eb-test-fake")
    transaction.execution_block.get.return_value = ExecutionBlock(subarray_id="99")

    flow_key = FlowKey(pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01")

    transaction.flow.list_keys.return_value = [flow_key]
    transaction.flow.get.return_value = Flow(
        key=flow_key,
        sources=[
            Source(
                function=SRC_FUNCTION,
                parameters={
                    "metric_type": "spectrum",
                    "nchan_avg": 1,
                    "additional_windows": 1,
                    "rounding_sensitivity": 7,
                },
            )
        ],
        sink=Sink(
            topics="metrics-spectrum-01",
            host=KafkaUrl("kafka://host:9092"),
            format="msgpack_numpy",
        ),
    )
    transaction.flow.state.return_value.get.return_value = {
        "windows": [{"start": 100, "end": 100, "channels_averaged": 1}]
    }

    mock_config.Config.return_value.txn.return_value.__iter__.return_value = [transaction]
    watcher_mock = MagicMock()
    watcher_mock.txn.return_value.__iter__.return_value = [transaction]
    mock_config.Config.return_value.watcher.return_value = [watcher_mock]

    monkeypatch.setenv("SDP_PB_ID", "pb-test-789")
    monkeypatch.setenv("SDP_SUBARRAY_ID", "99")
    processor = SignalDisplayMetrics(metrics=["spectrum"])
    assert processor._processing_block_id == "pb-test-789"
    assert processor._execution_block_id == "eb-test-fake"
    assert processor._subarray_id == "99"
    processor.config = None
    processor._flow_watcher()
    sleep(1)

    assert processor.flows == {
        "spectrum": [
            SignalDisplayFlow(
                name="metrics-spectrum-01",
                topic="metrics-spectrum-01",
                host=KafkaUrl("kafka://host:9092"),
                data_format="msgpack_numpy",
                metric_type="spectrum",
                nchan_avg=1,
                additional_windows=1,
                rounding_sensitivity=7,
                flow=Flow(
                    key=FlowKey(
                        pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01"
                    ),
                    sources=[
                        Source(
                            function=SRC_FUNCTION,
                            parameters={
                                "metric_type": "spectrum",
                                "nchan_avg": 1,
                                "additional_windows": 1,
                                "rounding_sensitivity": 7,
                            },
                        )
                    ],
                    sink=Sink(
                        topics="metrics-spectrum-01",
                        host=KafkaUrl("kafka://host:9092"),
                        format="msgpack_numpy",
                    ),
                ),
                state={"windows": [{"start": 100, "end": 100, "channels_averaged": 1}]},
            )
        ]
    }

    assert transaction.mock_calls == [
        call.processing_block.get("pb-test-789"),
        call.execution_block.get("eb-test-fake"),
        call.flow.list_keys(),
        call.flow.get(FlowKey(pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01")),
        call.flow.state(
            Flow(
                key=FlowKey(pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01"),
                sources=[
                    Source(
                        function=SRC_FUNCTION,
                        parameters={
                            "metric_type": "spectrum",
                            "nchan_avg": 1,
                            "additional_windows": 1,
                            "rounding_sensitivity": 7,
                        },
                    )
                ],
                sink=Sink(
                    topics="metrics-spectrum-01",
                    host=KafkaUrl("kafka://host:9092"),
                    format="msgpack_numpy",
                ),
            )
        ),
        call.flow.state().get(),
        call.flow.list_keys(),
        call.flow.get(FlowKey(pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01")),
        call.flow.state(
            Flow(
                key=FlowKey(pb_id="pb-test-789", kind="data-queue", name="metrics-spectrum-01"),
                sources=[
                    Source(
                        function="SignalDisplayMetrics",
                        parameters={
                            "metric_type": "spectrum",
                            "nchan_avg": 1,
                            "additional_windows": 1,
                            "rounding_sensitivity": 7,
                        },
                    )
                ],
                sink=Sink(
                    topics="metrics-spectrum-01",
                    host=KafkaUrl("kafka://host:9092"),
                    format="msgpack_numpy",
                ),
            )
        ),
        call.flow.state().get(),
    ]


# Disabling `mid` temporarily: "mid",
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "metric_type",
    ALL_METRICS + ["stats"],
)
@pytest.mark.parametrize("telescope", ["low"])
@patch(
    "realtime.receive.processors.sdp.metrics.signal_display_metrics.DataQueueProducer",
    autospec=True,
)
async def test_complete(mock_producer, telescope: str, metric_type: str):
    """Run a simple test to check if things work"""

    producer_instance = AsyncMock()
    producer_instance._producer = None
    mock_producer.return_value = producer_instance

    path = f"tests/unit/signal_metrics/data/{telescope}.ms"
    logger.info("Untarring %s", telescope)
    if not Path(path).exists():
        input_ms_path = untar(f"{path}.tar.gz")
    else:
        input_ms_path = Path(path)
    metrics_processor = SignalDisplayMetrics(
        random_ids=False, ignore_config_db=True, disable_kafka=False, metrics=[metric_type]
    )
    metrics_processor._data_queue = producer_instance

    logger.info("Run processor...")
    # run plasma writer and processor til done
    await arun_emulated_sdp_pipeline(
        input_ms_path,
        PLASMA_SOCKET,
        20000000,
        metrics_processor,
        num_scans=1,
    )

    assert mock_producer.call_args_list == [
        call(server="localhost:9092", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]

    # Move asserts out of assert for performance when things get bad:

    made_calls = producer_instance.mock_calls
    made_calls_length = len(made_calls)
    assert made_calls_length == TEST_DATA[telescope]["calls"][metric_type]

    # convert all data to json:
    data = [c.kwargs for c in made_calls]
    _validate_sent_data(telescope, metric_type, data)


@patch(
    "realtime.receive.processors.sdp.metrics.signal_display_metrics.DataQueueProducer",
    autospec=True,
)
def test_if_all_metrics_works_correctly(mock_producer):
    """Check if the correct values are set for all stats requested"""
    metrics_processor = SignalDisplayMetrics(
        random_ids=False, ignore_config_db=True, disable_kafka=False, metrics=["all"]
    )
    sleep(1)
    assert metrics_processor.metrics == ALL_METRICS
    assert metrics_processor.enable_stats
    assert mock_producer.call_args_list == [
        call(server="localhost:9092", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]


@patch(
    "realtime.receive.processors.sdp.metrics.signal_display_metrics.DataQueueProducer",
    autospec=True,
)
def test_no_metrics_requested(mock_producer):
    """Check if no metrics are requested"""
    metrics_processor = SignalDisplayMetrics(
        random_ids=False, ignore_config_db=True, disable_kafka=False, metrics=None
    )
    sleep(1)
    assert metrics_processor.metrics == ["stats"]
    assert metrics_processor.enable_stats
    assert mock_producer.call_args_list == [
        call(server="localhost:9092", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]


@patch(
    "realtime.receive.processors.sdp.metrics.signal_display_metrics.DataQueueProducer",
    autospec=True,
)
def test_only_stats_requested(mock_producer):
    """Check if no metrics are requested"""
    metrics_processor = SignalDisplayMetrics(
        random_ids=False, ignore_config_db=True, disable_kafka=False, metrics=["stats"]
    )
    sleep(1)
    assert metrics_processor.metrics == ["stats"]
    assert metrics_processor.enable_stats
    assert mock_producer.call_args_list == [
        call(server="localhost:9092", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]


@patch(
    "realtime.receive.processors.sdp.metrics.signal_display_metrics.DataQueueProducer",
    autospec=True,
)
def test_no_stats_requested(mock_producer):
    """Check if single metric requested that the stats is disabled"""
    metrics_processor = SignalDisplayMetrics(
        random_ids=False,
        ignore_config_db=True,
        disable_kafka=False,
        metrics=[MetricDataTypes.SPECTRUM],
    )
    sleep(1)
    assert metrics_processor.metrics == [MetricDataTypes.SPECTRUM]
    assert metrics_processor.enable_stats is False
    assert mock_producer.call_args_list == [
        call(server="localhost:9092", message_max_bytes=524288000, encoding=Encoding.MSGPACK_NUMPY)
    ]


@patch("realtime.receive.processors.sdp.metrics.utils.utilities.date", autospec=True)
def test_create_id(mock_date):
    """Test the ID creation method"""
    mock_date.today.return_value = date(2024, 4, 1)

    assert create_id("pytest").startswith("pytest-test-20240401-")


@patch("realtime.receive.processors.sdp.metrics.utils.utilities.date", autospec=True)
def test_create_id_2(mock_date):
    """Test the ID creation method"""
    mock_date.today.return_value = date(2024, 4, 1)

    assert create_id("pytest", generator="next").startswith("pytest-next-20240401-")
