import asyncio
from io import BytesIO

import numpy as np
import pytest
from pytest_benchmark.fixture import BenchmarkFixture

from realtime.receive.processors.sdp.pointing_helpers import PointingCalculator, PointingType
from tests.common_kafka import (
    KafkaBatchMessage,
    asend_in_batches,
    auto_delete_producer,
    earliest_consumer,
)
from tests.common_pointings import STANDARD_POINTING_DTYPE, generate_pseudo_numpy_pointing_data

TOPIC = "benchmark_topic"

# From https://confluence.skatelescope.org/display/SWSI/Array+Assemblies
AA05_DISHES = 4
AA2_DISHES = 68
AASTAR_DISHES = 144


@pytest.mark.benchmark(group="np.load")
@pytest.mark.parametrize("arr_size", [1, 100, 200, 400, 800, 1600])
def test_numpy_load_throughput(benchmark: BenchmarkFixture, arr_size: int):
    """To help understand how the performance changes with different sizes"""

    bio = BytesIO()
    data = np.empty(
        arr_size,
        dtype=STANDARD_POINTING_DTYPE,
    )
    np.save(bio, data)

    def load_bytes(data: memoryview):
        np.load(BytesIO(data))

    benchmark.pedantic(
        load_bytes,
        args=(bio.getbuffer(),),
        warmup_rounds=1000,
        rounds=4000,
    )


@pytest.mark.benchmark(group="aiokafka")
@pytest.mark.parametrize("message_size", [256, 512, 1024, 2048, 4096, 8192])
@pytest.mark.parametrize("message_count", [1, 20, 50, 100, 200, 400, 800])
def test_aiokafka_throughput(
    benchmark: BenchmarkFixture,
    message_size: int,
    message_count: int,
):
    """
    To help understand how fast aiokafka will read different numbers and
    sizes of messages
    """
    asyncio.run(_write_messages(message_count, message_size))

    async def aread_messages():
        async with earliest_consumer(TOPIC) as consumer:
            for _ in range(message_count):
                await consumer.getone()

    def read_messages():
        asyncio.run(aread_messages())

    benchmark(read_messages)


async def _write_messages(num_messages: int, message_size: int):
    message = bytearray(message_size)

    def generate_messages():
        for _ in range(num_messages):
            yield KafkaBatchMessage(message)

    async with auto_delete_producer(TOPIC) as producer:
        await asend_in_batches(producer, {TOPIC: generate_messages()})


@pytest.mark.benchmark(group="pointing_calculation")
@pytest.mark.parametrize("n_antennas", [AA05_DISHES, AA2_DISHES, AASTAR_DISHES])
# 5min is the expected pointing calibration scan time
@pytest.mark.parametrize("ob_time_min", [5])
def test_pointing_calculation_time(benchmark: BenchmarkFixture, n_antennas: int, ob_time_min: int):
    """
    To help understand the cost of calculating pointings all at once with
    different numbers of samples
    """
    calculator = PointingCalculator(n_antennas, calculate_source_offset=False)
    for pointing_type in PointingType:
        for pointing_datum in generate_pseudo_numpy_pointing_data(n_antennas, ob_time_min):
            calculator.add_sample(
                pointing_datum["antenna_id"],
                pointing_type,
                pointing_datum["ts"],
                pointing_datum["az"],
                pointing_datum["el"],
            )

    def calculate_entries():
        return list(calculator.calc_entries())

    benchmark(calculate_entries)
