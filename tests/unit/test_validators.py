import numpy as np
import pytest

from realtime.receive.processors.utils.numpy_validators import (
    ValidationError,
    validate_structured_ndarray,
)

EXPECTED_DTYPE = [
    [("antenna_name", np.str_), ("antenna_id", np.integer)],
    ("ts", np.datetime64),
    ("az", np.floating),
    ("el", np.floating),
]


def test_validate_structured_ndarray():
    raw_sample = np.array(
        [("LOW001", 0, 0, 0)],
        dtype=[
            ("antenna_name", np.str_),
            ("ts", "datetime64[ns]"),
            ("az", np.float32),
            ("el", np.float32),
        ],
    )
    sample = validate_structured_ndarray(
        raw_sample,
        EXPECTED_DTYPE,
    )
    assert raw_sample == sample


@pytest.mark.parametrize(
    "dtype,message",
    [
        (
            [
                ("antenna_name", np.int_),
                ("ts", "datetime64[ns]"),
                ("az", np.float32),
                ("el", np.float32),
            ],
            "is not a subdtype of <class 'numpy.str_'>",
        ),
        (
            [
                ("antenna_id", np.str_),
                ("ts", "datetime64[ns]"),
                ("az", np.float32),
                ("el", np.float32),
            ],
            "is not a subdtype of <class 'numpy.integer'>",
        ),
        (
            [
                ("ts", "datetime64[ns]"),
                ("az", np.float32),
                ("el", np.float32),
            ],
            "does not satisfy one of",
        ),
        (
            [
                ("antenna_id", np.int_),
                ("ts", "datetime64[ns]"),
                ("az", np.float32),
            ],
            "missing required member el of subdtype <class 'numpy.floating'>",
        ),
    ],
)
def test_validate_structured_ndarray_exceptions(dtype, message):
    with pytest.raises(ValidationError, match=message):
        validate_structured_ndarray(
            np.ones([], dtype=dtype),
            EXPECTED_DTYPE,
        )
