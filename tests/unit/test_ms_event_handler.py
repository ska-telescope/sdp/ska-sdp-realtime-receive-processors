import collections
import enum
import json
from datetime import datetime

import pytest
import pytest_asyncio
import ska_sdp_config
from aiokafka.consumer.consumer import AIOKafkaConsumer
from overrides import override
from ska_sdp_config.entity.flow import DataProduct, Flow

from realtime.receive.processors.sdp.ms_event_handler import (
    DataQueueNotifyMsClosedEventHandler,
    MsClosedEvent,
    MsClosedEventData,
    MsClosedEventHandler,
    SdpConfigMsClosedEventHandler,
)
from realtime.receive.processors.utils.data_queue_notifications import (
    KafkaNotifier,
    NotificationMessage,
)
from tests.common_kafka import KAFKA_HOST, auto_delete_producer


def test_aggregate_handler_discards_failing_handlers_on_start():
    first = _ConditionalThrowingMsClosedEventHandler("first")
    second = _ConditionalThrowingMsClosedEventHandler("second")
    closed_event = MsClosedEvent([first, second])

    first.next["start"] = _InvocationResult.RAISE

    closed_event.start()

    assert first.calls["start"] == [_InvocationResult.RAISE]
    assert second.calls["start"] == [_InvocationResult.SUCCEED]

    closed_event.invoke(MsClosedEventData("a.ms", None))
    assert first.calls["ms_closed"] == []
    assert second.calls["ms_closed"] == [(_InvocationResult.SUCCEED, "a.ms")]

    closed_event.stop()
    assert first.calls["stop"] == []
    assert second.calls["stop"] == [_InvocationResult.SUCCEED]


def test_aggregate_handler_invokes_started_handlers_even_if_exception_is_raised():
    first = _ConditionalThrowingMsClosedEventHandler("first")
    second = _ConditionalThrowingMsClosedEventHandler("second")
    close_event = MsClosedEvent([first, second])

    first.next["ms_closed"] = _InvocationResult.RAISE

    close_event.start()
    assert first.calls["start"] == [_InvocationResult.SUCCEED]
    assert second.calls["start"] == [_InvocationResult.SUCCEED]

    close_event.invoke(MsClosedEventData("a.ms", None))
    assert first.calls["ms_closed"] == [(_InvocationResult.RAISE, "a.ms")]
    assert second.calls["ms_closed"] == [(_InvocationResult.SUCCEED, "a.ms")]

    first.next["ms_closed"] = _InvocationResult.SUCCEED
    close_event.invoke(MsClosedEventData("b.ms", None))
    assert first.calls["ms_closed"] == [
        (_InvocationResult.RAISE, "a.ms"),
        (_InvocationResult.SUCCEED, "b.ms"),
    ]
    assert second.calls["ms_closed"] == [
        (_InvocationResult.SUCCEED, "a.ms"),
        (_InvocationResult.SUCCEED, "b.ms"),
    ]

    close_event.stop()
    assert first.calls["stop"] == [_InvocationResult.SUCCEED]
    assert second.calls["stop"] == [_InvocationResult.SUCCEED]


def test_aggregate_handler_stops_started_handlers_even_if_exception_is_raised():
    first = _ConditionalThrowingMsClosedEventHandler("first")
    second = _ConditionalThrowingMsClosedEventHandler("second")
    closed_event = MsClosedEvent([first, second])

    first.next["stop"] = _InvocationResult.RAISE

    closed_event.start()
    assert first.calls["start"] == [_InvocationResult.SUCCEED]
    assert second.calls["start"] == [_InvocationResult.SUCCEED]

    closed_event.stop()
    assert first.calls["stop"] == [_InvocationResult.RAISE]
    assert second.calls["stop"] == [_InvocationResult.SUCCEED]


class _InvocationResult(enum.Enum):
    SUCCEED = enum.auto()
    RAISE = enum.auto()


class _ConditionalThrowingMsClosedEventHandler(MsClosedEventHandler):
    def __init__(self, name: str) -> None:
        self.name = name
        self.next = collections.defaultdict(lambda: _InvocationResult.SUCCEED)
        self.calls = collections.defaultdict(list)

    @override
    def start(self):
        self.calls["start"].append(self.next["start"])
        if self.next["start"] == _InvocationResult.RAISE:
            raise RuntimeError(f"{self.name} raising in start")

    @override
    def invoke(self, data: MsClosedEventData):
        self.calls["ms_closed"].append((self.next["ms_closed"], data.local_path))
        if self.next["ms_closed"] == _InvocationResult.RAISE:
            raise RuntimeError(f"{self.name} raising in ms_closed")

    @override
    def stop(self):
        self.calls["stop"].append(self.next["stop"])
        if self.next["stop"] == _InvocationResult.RAISE:
            raise RuntimeError(f"{self.name} raising in stop")


@pytest.fixture(name="sdp_config")
def _sdp_config_fixture():
    config = ska_sdp_config.Config()
    config.backend.delete("/", must_exist=False, recursive=True, prefix=True)
    yield config
    config.backend.delete("/", must_exist=False, recursive=True, prefix=True)
    config.close()


@pytest.mark.parametrize(
    "existing_value, expected_paths",
    [
        ({"paths": ["a.ms"]}, ["a.ms", "event.ms"]),
        ({"paths": []}, ["event.ms"]),
        ({}, ["event.ms"]),
        (None, ["event.ms"]),
    ],
)
def test_sdp_config_ms_event_handler_updates_paths_correct(
    sdp_config: ska_sdp_config.Config,
    existing_value: dict | None,
    expected_paths: list[str],
):
    flow_key = Flow.Key(
        pb_id="pb-00-00000000-0", kind="data-product", name="vis-receive-mswriter-processor"
    )

    for txn in sdp_config.txn():
        txn.flow.create(
            Flow(
                key=flow_key,
                sink=DataProduct(data_dir="./", paths=[]),
                data_model="Visibility",
                sources=[],
            )
        )
        if existing_value:
            txn.flow.state(flow_key).create(existing_value)
        else:
            txn.flow.state(flow_key).create({"paths": []})

    handler = SdpConfigMsClosedEventHandler(flow_key.pb_id)
    handler.invoke(MsClosedEventData("event.ms", None))

    for txn in sdp_config.txn():
        final_value = txn.flow.state(flow_key).get()
    assert expected_paths == final_value["paths"]


@pytest.mark.parametrize(
    "local_path, sdp_path, expected_paths",
    [
        ("local.ms", "sdp.ms", ["sdp.ms"]),
        ("local.ms", None, ["local.ms"]),
    ],
)
def test_sdp_config_ms_event_handler_local_path_fallback(
    sdp_config: ska_sdp_config.Config,
    local_path: str,
    sdp_path: str | None,
    expected_paths: list[str],
):
    flow_key = Flow.Key(
        pb_id="pb-00-00000000-0", kind="data-product", name="vis-receive-mswriter-processor"
    )
    for txn in sdp_config.txn():
        txn.flow.create(
            Flow(
                key=flow_key,
                sink=DataProduct(data_dir="./", paths=[]),
                data_model="Visibility",
                sources=[],
            )
        )
        txn.flow.state(flow_key).create({"paths": []})
    handler = SdpConfigMsClosedEventHandler(flow_key.pb_id)
    handler.invoke(MsClosedEventData(local_path, sdp_path))

    for txn in sdp_config.txn():
        final_value = txn.flow.state(flow_key).get()
    assert expected_paths == final_value["paths"]


MS_CLOSED_EVENT_TOPIC = "ms-closed-events"


@pytest_asyncio.fixture(name="consumer")
async def consumer_fixture():
    """Kafka consumer for MS closed events"""
    consumer = AIOKafkaConsumer(MS_CLOSED_EVENT_TOPIC, bootstrap_servers=KAFKA_HOST)
    await consumer.start()
    yield consumer
    await consumer.stop()


@pytest_asyncio.fixture(name="kafka_notifier")
async def kafka_notifier():
    async with auto_delete_producer(MS_CLOSED_EVENT_TOPIC):
        yield KafkaNotifier(KAFKA_HOST, MS_CLOSED_EVENT_TOPIC)


@pytest.mark.asyncio
async def test_kafka_ms_event_handler_sends_notification(
    kafka_notifier: KafkaNotifier, consumer: AIOKafkaConsumer
):
    expected_local_path = "test_path"
    expected_eb_id = "some_eb_id"

    handler = DataQueueNotifyMsClosedEventHandler(kafka_notifier, expected_eb_id)
    event_data = MsClosedEventData(local_path=expected_local_path, sdp_path="test_sdp_path")

    # Run the handler
    handler.start()
    handler.invoke(event_data)

    # Verify that send_notification was called with the expected message
    expected_msg = NotificationMessage(
        file=expected_local_path,
        time=datetime.now().isoformat(),
        eb_id=expected_eb_id,
    )
    ms_closed_event_raw = (await consumer.getone()).value
    received_msg = NotificationMessage.from_dict(json.loads(ms_closed_event_raw.decode("utf-8")))

    # Verify the contents of the called message
    assert received_msg.file == expected_msg.file
    assert received_msg.eb_id == expected_msg.eb_id
    # Allow some tolerance for the time difference

    err = abs(
        datetime.fromisoformat(received_msg.time) - datetime.fromisoformat(received_msg.time)
    ).seconds
    assert err < 1

    handler.stop()
