from io import BytesIO

import numpy as np
import pytest
import pytest_asyncio
from aiokafka import AIOKafkaConsumer, AIOKafkaProducer

from tests.common_kafka import KAFKA_HOST

TEST_TOPIC = "test-events"


@pytest_asyncio.fixture(name="producer")
async def producer_fixture():
    p = AIOKafkaProducer(bootstrap_servers=KAFKA_HOST)
    await p.start()
    yield p
    await p.stop()


@pytest_asyncio.fixture(name="consumer")
async def consumer_fixture():
    consumer = AIOKafkaConsumer(TEST_TOPIC, bootstrap_servers=KAFKA_HOST)
    await consumer.start()
    yield consumer
    await consumer.stop()


@pytest.mark.asyncio
async def test_kafka_data_roundtrip(producer: AIOKafkaProducer, consumer: AIOKafkaConsumer):
    offsets = np.array(
        [
            (1, "2023-03-21T15:00:00.0", 1.0, 0.1),
            (2, "2023-03-21T15:00:00.0", 2.0, 0.2),
            (3, "2023-03-21T15:00:00.0", 3.0, 0.3),
        ],
        dtype=[
            ("dish", int),
            ("timestamp", "datetime64[ns]"),
            ("az", np.double),
            ("el", np.double),
        ],
    )

    bio = BytesIO()
    np.save(bio, offsets)
    data = bio.getbuffer()
    await producer.send_and_wait(TEST_TOPIC, data)

    record_dict = await consumer.getmany(timeout_ms=1000)
    messages = [msg for msgs in record_dict.values() for msg in msgs]
    assert len(messages) == 1
    arr = np.load(BytesIO(messages[0].value))
    assert len(arr) == 3
    assert arr[0]["dish"] == 1
    assert arr[1]["timestamp"] == np.datetime64("2023-03-21T15:00:00")
    assert arr[2]["az"] == 3.0
    assert arr[0]["el"] == 0.1
