"""Various utilities to assist with testing pointing processing code"""

import time

import numpy as np

STANDARD_POINTING_DTYPE = np.dtype(
    [
        ("antenna_id", int),
        ("ts", "datetime64[ns]"),
        ("az", np.double),
        ("el", np.double),
    ]
)

EXPECTED_POINTING_UPDATE_FREQ = 10  # Hz

_AZIMUTH_START = 0.5
_AZIMUTH_END = 1.0
_ELEVATION_START = 0.2
_ELEVATION_END = 0.4


def generate_pseudo_numpy_pointing_data(
    num_antennas: int,
    time_min: int,
    first_time=time.time(),
) -> np.ndarray:
    """
    Generate pointing data as a 1D numpy array that has the expected dtype
    that will be read off Kafka topics.
    Data is semi-realistic in that times are ascending (but
    antennas are interleaved), azimuth and elevation values are trending
    in one direction, however random noise has been added to each data
    point so that it isn't consistent between calls to this function.
    """
    num_samples = time_min * 60 * EXPECTED_POINTING_UPDATE_FREQ
    time_range = np.linspace(first_time, first_time + time_min * 60, num_samples)

    noisy_time_range = time_range + np.random.uniform(-0.05, 0.05, num_samples)
    dt_time_range = noisy_time_range * 1e9
    dt_time_range = dt_time_range.astype(int).astype("datetime64[ns]")

    def gen_antenna_data(antenna_id: int):
        data = np.empty(
            num_samples,
            dtype=STANDARD_POINTING_DTYPE,
        )
        data["antenna_id"] = antenna_id
        data["ts"] = dt_time_range
        data["az"] = np.linspace(_AZIMUTH_START, _AZIMUTH_END, num_samples) + _generate_noise(
            _AZIMUTH_START, _AZIMUTH_END, num_samples
        )
        data["el"] = np.linspace(_ELEVATION_START, _ELEVATION_END, num_samples) + _generate_noise(
            _ELEVATION_START, _ELEVATION_END, num_samples
        )

        return data

    all_antenna_data = np.concatenate(
        [gen_antenna_data(antenna_id) for antenna_id in range(num_antennas)]
    )

    # Order by time to get semi-realistic insertions into Kafka where the
    # antenna pointings are interleaved rather than all of antenna 0 then 1
    # etc
    sorted_times = np.argsort(all_antenna_data["ts"])
    return all_antenna_data[sorted_times]


def _generate_noise(start: float, end: float, num_samples: int):
    """
    Generates noise that when added to linspace'd values won't cause
    them to overlap
    """
    gap = (end - start) / num_samples
    abs_noise_range = gap / 2
    return np.random.uniform(-abs_noise_range, abs_noise_range, num_samples)
