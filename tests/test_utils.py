"""
test utilities for realtime.receive.processors
"""

import tarfile
from pathlib import Path
from typing import Union


def untar(archive_path: Union[str, Path]) -> Path:
    """Extracts a tar archive to the same directory as the archive.

    Returns:
        Path: string name of the extracted directory
    """
    archive_path = Path(archive_path)
    with tarfile.open(archive_path, "r:gz") as tar:
        tar.extractall(archive_path.parent)
        return archive_path.parent / tar.getnames()[0]
