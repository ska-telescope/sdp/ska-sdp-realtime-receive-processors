import asyncio
import logging
import random
import string
import tempfile
from io import BytesIO
from pathlib import Path

import numpy as np
import pytest
from casacore import tables
from pytest_benchmark.fixture import BenchmarkFixture

from realtime.receive.processors.sdp.mswriter_processor import MSWriterProcessor
from realtime.receive.processors.sdp.pointing_helpers import PointingKafkaSourceConfig
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline
from tests.common_kafka import (
    KAFKA_HOST,
    KafkaBatchMessage,
    asend_in_batches,
    auto_delete_producer,
)
from tests.common_pointings import generate_pseudo_numpy_pointing_data
from tests.test_utils import untar

COMMANDED_TOPIC = "test_commanded"
ACTUAL_TOPIC = "test_actual"
PLASMA_SOCKET = "/tmp/plasma"
logger = logging.getLogger(__name__)

# From https://confluence.skatelescope.org/display/SWSI/Array+Assemblies
AA05_DISHES = 4
AA2_DISHES = 68
AASTAR_DISHES = 144

INPUT_MS_PATH = untar("data/sim-vis.ms.tar.gz")


@pytest.mark.benchmark(group="mswriter_pointings")
def test_pointing_processing_baseline(benchmark: BenchmarkFixture):
    with tempfile.TemporaryDirectory() as temp_dir:
        ms_path = _benchmark_mswriter_processor(benchmark, temp_dir, None)

        pointing_table = tables.table(f"{temp_dir}/{ms_path}/POINTING")
        assert 0 == pointing_table.nrows()


@pytest.mark.benchmark(group="mswriter_pointings")
# Difficult to scale the number of antennas in the emulated pipeline right
# now, so the observation time can be increased to scale the amount of data
# if desired. 5min is the expected pointing calibration scan time.
@pytest.mark.parametrize("n_antennas", [AA05_DISHES])
@pytest.mark.parametrize("ob_time_min", [5])
@pytest.mark.parametrize("data_batch_size", [1, 10, 50, 100, 200, 400])
def test_pointing_processing(
    benchmark: BenchmarkFixture,
    n_antennas: int,
    ob_time_min: int,
    data_batch_size: int,
):
    expected_num_rows = asyncio.run(
        _write_kafka_pointings(
            n_antennas,
            ob_time_min,
            data_batch_size,
        )
    )

    with tempfile.TemporaryDirectory() as temp_dir:
        pointing_source = PointingKafkaSourceConfig(
            server=KAFKA_HOST,
            commanded_topic=COMMANDED_TOPIC,
            actual_topic=ACTUAL_TOPIC,
        )
        ms_path = _benchmark_mswriter_processor(benchmark, temp_dir, pointing_source)
        pointing_table = tables.table(f"{temp_dir}/{ms_path}/POINTING")
        assert expected_num_rows == pointing_table.nrows()


def _benchmark_mswriter_processor(
    benchmark: BenchmarkFixture,
    temp_dir: str,
    pointing_source: PointingKafkaSourceConfig | None,
):
    def run(processor: MSWriterProcessor):
        coro = arun_emulated_sdp_pipeline(
            INPUT_MS_PATH,
            PLASMA_SOCKET,
            20000000,
            processor,
            num_scans=1,
        )
        asyncio.run(coro)

    def do_benchmark(temp_dir: str):
        # write all outputs to temp directory
        rng = random.choices(string.ascii_lowercase + string.digits, k=8)
        template = Path(f"benchmark_{''.join(rng)}.ms")
        output_ms_template = temp_dir / template
        mswriter_processor = MSWriterProcessor(
            output_ms_template,
            timestamp_output=False,
            pointing_source_config=pointing_source,
        )
        run(mswriter_processor)
        return template

    template: Path = benchmark.pedantic(
        do_benchmark,
        args=(temp_dir,),
        warmup_rounds=1,
        rounds=3,
        iterations=1,
    )
    gen_path = f"{template.stem}.scan-1{template.suffix}"

    return gen_path


async def _write_kafka_pointings(
    num_antennas: int,
    time_min: int,
    data_batch_size: int,
) -> int:
    """
    Write pointings to Kafka, returning the number of actual pointings
    samples that were written.
    """
    num_samples: int | None = None

    def generate_kafka_batch_messages():
        all_antenna_data = generate_pseudo_numpy_pointing_data(num_antennas, time_min)
        data_batches = _chunk_array(all_antenna_data, data_batch_size)
        for data_batch in data_batches:
            bio = BytesIO()
            np.save(bio, data_batch)

            unix_ms = np.min(data_batch["ts"].astype("datetime64[ms]").astype("int")).item()

            yield KafkaBatchMessage(bio.getbuffer(), timestamp_ms=unix_ms)

        nonlocal num_samples
        if num_samples is None:
            num_samples = all_antenna_data.size
        else:
            assert num_samples == all_antenna_data.size

    async with auto_delete_producer(COMMANDED_TOPIC, ACTUAL_TOPIC) as producer:
        await asend_in_batches(
            producer,
            {
                # Call generate...() twice to get unique data for each topic
                COMMANDED_TOPIC: generate_kafka_batch_messages(),
                ACTUAL_TOPIC: generate_kafka_batch_messages(),
            },
        )

    assert num_samples is not None
    return num_samples


def _chunk_array(arr: np.ndarray, max_size: int):
    return np.array_split(arr, np.ceil(len(arr) / max_size))
