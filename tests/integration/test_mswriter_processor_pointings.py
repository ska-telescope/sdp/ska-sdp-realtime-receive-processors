"""
MSWriterProcessor pointing integration tests
"""

import logging
import tempfile
from datetime import datetime
from io import BytesIO
from pathlib import Path

import numpy as np
import pytest
import pytest_asyncio
from aiokafka import AIOKafkaProducer
from casacore import tables
from realtime.receive.core.ms_asserter import MSAsserter
from realtime.receive.core.msutils import MeasurementSet, vis_mjd_epoch
from realtime.receive.core.time_utils import mjd_to_unix, unix_to_mjd

from realtime.receive.processors.sdp.mswriter_processor import MSWriterProcessor
from realtime.receive.processors.sdp.pointing_helpers import PointingKafkaSourceConfig
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline
from tests.common_kafka import KAFKA_HOST, auto_delete_producer
from tests.common_pointings import STANDARD_POINTING_DTYPE
from tests.test_utils import untar

COMMANDED_TOPIC = "test_commanded"
ACTUAL_TOPIC = "test_actual"
SOURCE_OFFSET_TOPIC = "test_source_offset"
PLASMA_SOCKET = "/tmp/plasma"
logger = logging.getLogger(__name__)


@pytest_asyncio.fixture(name="producer")
async def kafka_producer():
    """Kafka producer"""
    async with auto_delete_producer(
        COMMANDED_TOPIC, ACTUAL_TOPIC, SOURCE_OFFSET_TOPIC
    ) as producer:
        yield producer


@pytest.mark.asyncio
# These seem to be treated differently for some reason
# The first will fail quickly, the second will take a few seconds which
# is enough to interrupt processing if it isn't a background task
@pytest.mark.parametrize("host", ["unknown-host", "unknown-host.local"])
async def test_invalid_kafka_host_is_handled(host: str):
    INPUT_MS_PATH = untar("data/1934_SB4094_b0_t0_ch0.ms.tar.gz")
    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("output_aa05low.ms")
        mswriter_processor = MSWriterProcessor(
            output_ms_template,
            pointing_source_config=PointingKafkaSourceConfig(f"{host}:9999", "cmd", "act"),
        )

        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            INPUT_MS_PATH,
            PLASMA_SOCKET,
            20000000,
            mswriter_processor,
            num_scans=1,
        )

        output_ms_path = output_ms_template.parent / (
            output_ms_template.stem + ".scan-1" + output_ms_template.suffix
        )
        asserter = MSAsserter()
        asserter.assert_ms_data_equal(str(INPUT_MS_PATH), str(output_ms_path))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "skip_publishing_to_kafka, source_offset_from_kafka, include_pointings_prior_to_first_vis",
    (
        (True, False, False),
        (False, True, True),
        (False, True, False),
        (False, False, True),
        (False, False, False),
    ),
)
async def test_pointings_from_kafka_are_written_to_ms(
    producer: AIOKafkaProducer,
    source_offset_from_kafka: bool,
    include_pointings_prior_to_first_vis: bool,
    skip_publishing_to_kafka: bool,
    caplog: pytest.LogCaptureFixture,
):
    INPUT_MS_PATH = untar("data/1934_SB4094_b0_t0_ch0.ms.tar.gz")

    async def _maybe_write_kafka_pointings(*args, **kwargs):
        if skip_publishing_to_kafka:
            return
        await _write_kafka_pointings(*args, **kwargs)

    with MeasurementSet.open(str(INPUT_MS_PATH)) as ms:
        vis_epoch = mjd_to_unix(vis_mjd_epoch(ms))

        if include_pointings_prior_to_first_vis:
            for i in range(ms.num_stations):
                await _maybe_write_kafka_pointings(
                    producer,
                    COMMANDED_TOPIC,
                    vis_epoch - 2,
                    [(i, "2023-03-21T15:00:01.5", -1, -1)],
                )
                await _maybe_write_kafka_pointings(
                    producer,
                    ACTUAL_TOPIC,
                    vis_epoch - 2,
                    [(i, "2023-03-21T15:00:01.0", -1, -1)],
                )
                await _maybe_write_kafka_pointings(
                    producer,
                    SOURCE_OFFSET_TOPIC,
                    vis_epoch - 2,
                    [(i, "2023-03-21T15:00:01.5", -1, -1)],
                )

        for i in range(ms.num_stations):
            await _maybe_write_kafka_pointings(
                producer,
                COMMANDED_TOPIC,
                vis_epoch + 2,
                [
                    (i, "2023-03-21T15:00:04.5", 3.5, 0.35),
                    (i, "2023-03-21T15:00:02.5", 2.0, 0.2),
                    (i, "2023-03-21T15:00:03.5", 3.0, 0.3),
                    (i, "2023-03-21T15:00:05.5", 4.0, 0.4),
                    (i, "2023-03-21T15:00:06.5", 5.0, 0.5),
                ],
            )
            await _maybe_write_kafka_pointings(
                producer,
                ACTUAL_TOPIC,
                vis_epoch + 2,
                [
                    (i, "2023-03-21T15:00:03.0", 1.5, 1.1),
                    (i, "2023-03-21T15:00:05.0", 2.5, 1.2),
                    (i, "2023-03-21T15:00:04.0", 3.5, 1.3),
                ],
            )
            await _maybe_write_kafka_pointings(
                producer,
                SOURCE_OFFSET_TOPIC,
                vis_epoch + 2,
                [
                    (i, "2023-03-21T15:00:02.5", 0.3, 0.2),
                    (i, "2023-03-21T15:00:03.5", 0.4, 0.25),
                    (i, "2023-03-21T15:00:05.5", 0.6, 0.35),
                    (i, "2023-03-21T15:00:06.5", 0.7, 0.4),
                    (i, "2023-03-21T15:00:04.5", 0.5, 0.3),
                ],
            )

    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("output_aa05low.ms")

        pointing_source = PointingKafkaSourceConfig(
            server=KAFKA_HOST,
            commanded_topic=COMMANDED_TOPIC,
            actual_topic=ACTUAL_TOPIC,
        )
        if source_offset_from_kafka:
            pointing_source.source_offset_topic = SOURCE_OFFSET_TOPIC

        mswriter_processor = MSWriterProcessor(
            output_ms_template,
            timestamp_output=False,
            pointing_source_config=pointing_source,
        )

        # run plasma writer and processor til done
        with caplog.at_level(logging.ERROR):
            await arun_emulated_sdp_pipeline(
                INPUT_MS_PATH,
                PLASMA_SOCKET,
                20000000,
                mswriter_processor,
                num_scans=1,
            )

        # No unexpected errors please
        assert all("Error while processing payload" not in msg for msg in caplog.messages)

        # ms template should result in an ms at the following location
        output_ms_path = output_ms_template.parent / (
            output_ms_template.stem + ".scan-1" + output_ms_template.suffix
        )
        asserter = MSAsserter()
        asserter.assert_ms_data_equal(str(INPUT_MS_PATH), str(output_ms_path))

        points = tables.table(f"{output_ms_path}/POINTING", readonly=True)
        if skip_publishing_to_kafka:
            assert points.nrows() == 0
            return

        assert 3 * 12 == points.nrows()

        # Verify the first entry for the antenna 0 is from the earliest actual
        # pointing sample
        ant0_points = points.query("ANTENNA_ID = 0")
        assert 3 == ant0_points.nrows()
        assert 0 == ant0_points.getcell("ANTENNA_ID", 0)
        expected_mjd_time = unix_to_mjd(
            datetime.fromisoformat("2023-03-21T15:00:03+00:00").timestamp()
        )
        assert expected_mjd_time == ant0_points.getcell("TIME", 0)
        np.testing.assert_array_equal([[1.5, 1.1]], ant0_points.getcell("DIRECTION", 0))
        target_cell = ant0_points.getcell("TARGET", 0)
        assert 2.0 <= target_cell[0][0] <= 3.0
        assert 0.2 <= target_cell[0][1] <= 0.3

        src_off_cell = ant0_points.getcell("SOURCE_OFFSET", 0)
        if source_offset_from_kafka:
            assert 0.3 <= src_off_cell[0][0] <= 0.4
            assert 0.2 <= src_off_cell[0][1] <= 0.25
        else:
            assert -1.5 <= src_off_cell[0][0] <= -0.5
            assert 0.8 <= src_off_cell[0][1] <= 0.9


async def _write_kafka_pointings(
    producer: AIOKafkaProducer, topic: str, unix_ts: float, pointings: list
):
    numpy_pointings = np.array(
        pointings,
        dtype=STANDARD_POINTING_DTYPE,
    )
    bio = BytesIO()
    np.save(bio, numpy_pointings)
    await producer.send_and_wait(
        topic,
        bio.getbuffer(),
        timestamp_ms=int(unix_ts * 1e3),
    )
