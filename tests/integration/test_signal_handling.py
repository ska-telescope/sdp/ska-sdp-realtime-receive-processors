import logging
import os
import signal
import subprocess as sp
import sys
import tempfile
import time

import psutil
import pytest

from realtime.receive.processors.utils.runner_utils import PlasmaStoreContext

PLASMA_SOCKET = "/tmp/plasma"
PROCESSOR_CLASS = "realtime.receive.processors.sdp.mswriter_processor.MSWriterProcessor"
logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "sig,exitcode_expected,emulate_data",
    [
        (signal.SIGTERM, 0, False),
        (signal.SIGINT, 0, False),
        (signal.SIGTERM, 0, True),
        (signal.SIGINT, 0, True),
        (signal.SIGALRM, -signal.SIGALRM.value, False),
        (signal.SIGKILL, -signal.SIGKILL.value, False),
    ],
)
def test_signal_termination(sig: signal.Signals, exitcode_expected: int, emulate_data: bool):
    with PlasmaStoreContext(PLASMA_SOCKET, 100000) as _, tempfile.TemporaryDirectory() as temp_dir:
        command = [
            sys.executable,
            "-m",
            "realtime.receive.processors.main",
            "--use-sdp-metadata",
            "false",
            "-r",
            temp_dir + "/ready.txt",
            PROCESSOR_CLASS,
            "--input=data/sim-vis.ms" if emulate_data else None,
            temp_dir + "/output.ms",
        ]
        process = sp.Popen(
            list(filter(None, command)),
            stdout=sp.PIPE,
            stderr=sp.PIPE,
        )

        try:
            # wait for process to be ready before interrupting
            _wait_for_file(temp_dir + "/ready.txt", timeout=5)
            assert process.returncode is None
        except (TimeoutError, IOError):
            stdout, stderr = process.communicate()
            exitcode_actual = process.returncode
            assert False, _handle_popen_output(
                f"Process is not ready, exitcode {exitcode_actual}\n",
                stdout,
                stderr,
            )

        plasma_store_pid = None
        if emulate_data:
            children_procs = psutil.Process(process.pid).children()
            assert len(children_procs) == 1
            plasma_store_pid = children_procs[0].pid

        # interrupt process and return within timeout
        process.send_signal(sig)
        try:
            stdout, stderr = process.communicate(timeout=5)
        except sp.TimeoutExpired as e:
            logger.exception(
                "Process timed out :(.\nstdout: %s\nstderr: %s",
                e.stdout.decode() if e.stdout else "",
                e.stderr.decode() if e.stderr else "",
            )
            raise

        logger.info(
            "Process exited cleanly.\nstdout: %s\nstderr: %s",
            stdout.decode(),
            stderr.decode(),
        )
        exitcode_actual = process.returncode
        assert exitcode_expected == exitcode_actual, _handle_popen_output(
            f"Process exited with {exitcode_actual}\n",
            stdout,
            stderr,
        )

        if plasma_store_pid is not None:
            assert not psutil.pid_exists(plasma_store_pid)


def _handle_popen_output(message, stdout, stderr):
    if stdout:
        print(stdout.decode().rstrip())
    if stderr:
        print(stderr.decode().rstrip(), file=sys.stderr)
    return message


def _wait_for_file(filepath: str, timeout: float):
    timer = 0
    pollrate = 0.2
    while not os.path.lexists(filepath):
        time.sleep(min(pollrate, timeout - timer))
        timer += pollrate
        if timer >= timeout:
            raise TimeoutError
    if not os.path.isfile(filepath):
        raise IOError(f"{filepath} is not a file")
