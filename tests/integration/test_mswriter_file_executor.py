import re
import tempfile
from pathlib import Path
from typing import Optional

import numpy as np
import pytest
from realtime.receive.core.msutils import MeasurementSet, Mode

from realtime.receive.processors.file_executor import CommandFileExecutor, FunctionFileExecutor
from realtime.receive.processors.sdp.ms_event_handler import FileExecutorMsClosedEventHander
from realtime.receive.processors.sdp.mswriter_processor import MSWriterProcessor
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline
from tests.test_utils import untar

PLASMA_SOCKET = "/tmp/plasma"
INPUT_MS_PATH = untar("data/1934_SB4094_b0_t0_ch0.ms.tar.gz")


def assert_ms_scan_number(ms_path: str):
    """
    Asserts that the generated ms is of form
    "name.scan-%i.ms" and the SCAN_NUMBER column
    matches the directory name.
    """
    path = Path(ms_path)
    assert path.suffix == ".ms"
    file_parts = path.name.split(".")
    assert len(file_parts) > 2
    scan_matches = re.search("scan-(.*)", file_parts[-2])
    assert scan_matches is not None
    scan_id = int(scan_matches.group(1))
    measurement_set = MeasurementSet(ms_path, mode=Mode.READONLY)
    np.testing.assert_allclose(measurement_set.t.getcol("SCAN_NUMBER"), scan_id)


def assert_fail(ms_path):
    assert False


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "file_executor,args,exception",
    [
        (CommandFileExecutor, ["ls", "%s"], None),
        (CommandFileExecutor, ["ls", "%s-does-not-exist"], None),
        (FunctionFileExecutor, assert_ms_scan_number, None),
        (FunctionFileExecutor, assert_fail, AssertionError),
    ],
)
async def test_file_executors(
    file_executor: type,
    args: object,
    exception: Optional[type],
    caplog: pytest.LogCaptureFixture,
):
    num_scans = 2
    num_timestamps = 2
    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("output.ms")
        mswriter_processor = MSWriterProcessor(
            output_ms_template,
            use_plasmastman=False,
            timestamp_output=False,
            event_handlers=[FileExecutorMsClosedEventHander(file_executor(args))],
        )

        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            INPUT_MS_PATH,
            PLASMA_SOCKET,
            10000000,
            mswriter_processor,
            num_timestamps=num_timestamps,
            num_scans=num_scans,
        )
        if exception is not None:
            assert exception.__name__ in caplog.text
