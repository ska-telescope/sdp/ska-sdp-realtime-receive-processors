"""
MSWriterProcessor integration tests
"""

import ctypes.util
import logging
import tempfile
from pathlib import Path

import astropy.units as u
import pytest
from overrides import override
from realtime.receive.core import ChannelRange
from realtime.receive.core.ms_asserter import MSAsserter

from realtime.receive.processors.file_executor import FunctionFileExecutor
from realtime.receive.processors.sdp.accumulating_processor import (
    FreqAccumulatingProcessor,
    TimeAccumulatingProcessor,
)
from realtime.receive.processors.sdp.ms_event_handler import (
    FileExecutorMsClosedEventHander,
    MsClosedEventData,
    MsClosedEventHandler,
)
from realtime.receive.processors.sdp.mswriter_processor import MSWriterProcessor
from realtime.receive.processors.utils.processor_utils import load_base_processor_class
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline
from tests.test_utils import untar

try:
    HAS_LIBPLASMASTMAN = ctypes.util.find_library("plasmastman")
except RuntimeError:
    HAS_LIBPLASMASTMAN = False

PLASMA_SOCKET = "/tmp/plasma"
PROCESSOR_CLASS = "realtime.receive.processors.sdp.mswriter_processor.MSWriterProcessor"
logger = logging.getLogger(__name__)


def test_load_processor_class():
    assert load_base_processor_class(PROCESSOR_CLASS) == MSWriterProcessor


@pytest.mark.asyncio
@pytest.mark.parametrize("stman", ["tiledstman", "plasmastman"])
async def test_aa05low_single_scan(stman: str):
    """Tests mswriter processor against AA 0.5 Low data and checks
    that the output measurement set is deterministically generated.
    """
    num_scans = 1
    if stman == "plasmastman" and not HAS_LIBPLASMASTMAN:
        pytest.skip("libplasmastman not installed")

    INPUT_MS_PATH = untar("data/1934_SB4094_b0_t0_ch0.ms.tar.gz")
    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("output_aa05low.ms")
        mswriter_processor = MSWriterProcessor(
            output_ms_template,
            use_plasmastman=(stman == "plasmastman"),
            timestamp_output=False,
        )

        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            INPUT_MS_PATH,
            PLASMA_SOCKET,
            20000000,
            mswriter_processor,
            num_scans=num_scans,
        )

        # ms template should result in an ms at the following location
        output_ms_path = output_ms_template.parent / (
            output_ms_template.stem + ".scan-1" + output_ms_template.suffix
        )
        asserter = MSAsserter()
        asserter.assert_ms_data_equal(str(INPUT_MS_PATH), str(output_ms_path))


@pytest.mark.asyncio
@pytest.mark.parametrize("stman", ["tiledstman", "plasmastman"])
async def test_aa05low_single_scan_split_reception(stman: str):
    """Tests mswriter processor against AA 0.5 Low data and checks
    that the output measurement set is deterministically generated.
    Also test the case where a payload is split by frequency into
    multiple Visibility instances that the processor has to splice
    back together
    """
    num_scans = 1
    if stman == "plasmastman" and not HAS_LIBPLASMASTMAN:
        pytest.skip("libplasmastman not installed")

    INPUT_MS_PATH = untar("data/1934_SB4094_b0_t0_ch0.ms.tar.gz")
    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("output_aa05low.ms")
        mswriter_processor = MSWriterProcessor(
            output_ms_template,
            use_plasmastman=(stman == "plasmastman"),
            timestamp_output=False,
        )
        time_accumulating_processor = TimeAccumulatingProcessor(time_interval=1.0 * u.s)
        freq_accumulating_processor = FreqAccumulatingProcessor(
            time_limit=10.0 * u.s, num_subbands=2
        )
        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            INPUT_MS_PATH,
            PLASMA_SOCKET,
            20000000,
            [freq_accumulating_processor, time_accumulating_processor, mswriter_processor],
            num_scans=num_scans,
            num_timestamps=None,
            max_channels_per_payload=27,
        )

        # ms template should result in an ms at the following location
        output_ms_path = output_ms_template.parent / (
            output_ms_template.stem + ".scan-1" + output_ms_template.suffix
        )

        asserter = MSAsserter()
        asserter.assert_ms_data_equal(str(INPUT_MS_PATH), str(output_ms_path))


@pytest.mark.asyncio
@pytest.mark.parametrize("stman", ["tiledstman", "plasmastman"])
@pytest.mark.parametrize("num_scans", [1, 2])
async def test_aa05low_multiple(num_scans: int, stman: str):
    """Tests mswriter processor against AA 0.5 Low data with multiple scans."""

    num_timestamps = None  # lowering speeds up tests

    if stman == "plasmastman" and not HAS_LIBPLASMASTMAN:
        pytest.skip("libplasmastman not installed")

    INPUT_MS_PATH = untar("data/1934_SB4094_b0_t0_ch0.ms.tar.gz")
    output_ms_paths = []

    def append_ms(ms_path: str):
        output_ms_paths.append(ms_path)

    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("output_aa05low.ms")
        mswriter_processor = MSWriterProcessor(
            output_ms_template,
            use_plasmastman=(stman == "plasmastman"),
            timestamp_output=True,
            event_handlers=[FileExecutorMsClosedEventHander(FunctionFileExecutor(append_ms))],
        )

        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            INPUT_MS_PATH,
            PLASMA_SOCKET,
            50000000,
            mswriter_processor,
            num_scans=num_scans,
            num_timestamps=num_timestamps,
        )

        for output_ms_path in output_ms_paths:
            asserter = MSAsserter()
            asserter.assert_ms_data_equal(str(INPUT_MS_PATH), output_ms_path)


@pytest.mark.asyncio
async def test_non_zero_start_and_non_contiguous():
    num_scans = 1

    INPUT_MS_PATH = untar("data/sim-vis.ms.tar.gz")
    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("output_aa05low.ms")
        mswriter_processor = MSWriterProcessor(output_ms_template)

        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            INPUT_MS_PATH,
            PLASMA_SOCKET,
            20000000,
            mswriter_processor,
            num_scans=num_scans,
            channels=ChannelRange(5, 4, 2),
        )

        # ms template should result in an ms at the following location
        output_ms_path = output_ms_template.parent / (
            output_ms_template.stem + ".scan-1" + output_ms_template.suffix
        )
        asserter = MSAsserter()
        asserter.assert_ms_data_equal(str(INPUT_MS_PATH), str(output_ms_path))


@pytest.mark.asyncio
async def test_ms_events_get_fired():
    INPUT_MS_PATH = untar("data/sim-vis.ms.tar.gz")

    with tempfile.TemporaryDirectory() as temp_dir:
        output_ms_template = temp_dir / Path("output_aa05low.ms")

        event_handler = _CountingMsClosedEventHandler()
        mswriter_processor = MSWriterProcessor(output_ms_template, event_handlers=[event_handler])

        assert event_handler.start_count == 1
        assert event_handler.stop_count == 0
        assert len(event_handler.closed_events) == 0

        await arun_emulated_sdp_pipeline(
            INPUT_MS_PATH,
            PLASMA_SOCKET,
            20000000,
            mswriter_processor,
        )

        assert event_handler.start_count == 1
        assert event_handler.stop_count == 1

        assert len(event_handler.closed_events) == 1
        evt = event_handler.closed_events[0]
        assert evt.local_path.startswith(str(output_ms_template.parent))
        assert output_ms_template.stem in evt.local_path
        assert evt.local_path.endswith(output_ms_template.suffix)
        assert evt.sdp_path is None


class _CountingMsClosedEventHandler(MsClosedEventHandler):
    def __init__(self) -> None:
        self.start_count = 0
        self.stop_count = 0
        self.closed_events: list[MsClosedEventData] = []

    @override
    def start(self):
        self.start_count += 1

    @override
    def invoke(self, data: MsClosedEventData):
        self.closed_events.append(data)

    @override
    def stop(self):
        self.stop_count += 1
