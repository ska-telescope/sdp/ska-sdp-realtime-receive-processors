"""
Runner integration tests
"""

import asyncio
import math
import time
from pathlib import Path
from typing import List, Optional

import pytest
from ska_sdp_datamodels.visibility.vis_model import Visibility

from realtime.receive.processors.runner import Runner
from realtime.receive.processors.sdp.base_processor import BaseProcessor
from realtime.receive.processors.sdp.counter_processor import CounterProcessor
from realtime.receive.processors.utils.runner_utils import (
    PlasmaStoreContext,
    arun_emulated_sdp_pipeline,
    arun_plasma_writer,
    arun_plasma_writer_and_runner,
    create_plasma_consumer,
)
from tests.test_utils import untar

INPUT_MS_PATH = untar("data/1934_SB4094_b0_t0_ch0.ms.tar.gz")
TOTAL_TIMESTEPS = 56
TOTAL_CHANNELS = 54
PLASMA_SOCKET = "/tmp/plasma"


@pytest.mark.asyncio
@pytest.mark.parametrize("max_channels", [None, 60, 27, 15])
async def test_dataset_queuing(
    max_channels: Optional[int],
):
    """
    Tests the creation of the datasets by the queuing processor.
    """
    expected_chunks = math.ceil(TOTAL_CHANNELS / max_channels) if max_channels else 1
    total_datasets = TOTAL_TIMESTEPS * expected_chunks
    first_ds_channels = math.ceil(TOTAL_CHANNELS / expected_chunks)
    last_ds_channels = math.floor(TOTAL_CHANNELS / expected_chunks)

    datasets = await _generate_datasets_from_ms(
        INPUT_MS_PATH, max_channels_per_payload=max_channels
    )

    assert len(datasets) == total_datasets

    for index, ds_channels in [(0, first_ds_channels), (-1, last_ds_channels)]:
        dataset = datasets[index]
        assert dataset["datetime"].shape == (1,)
        assert dataset["vis"].shape == (1, 78, ds_channels, 4)
        assert dataset["channel_bandwidth"].shape == (ds_channels,)
        assert dataset["integration_time"].shape == (1,)
        assert dataset["weight"].shape == (1, 78, ds_channels, 4)
        assert dataset["flags"].shape == (1, 78, ds_channels, 4)
        assert dataset["uvw"].shape == (1, 78, 3)
        assert len(dataset.coords) == 7
        assert len(dataset.attrs) == 9
        assert dataset.configuration.location is not None


async def _generate_datasets_from_ms(ms: Path, **kwargs) -> List[Visibility]:
    datasets: List[Visibility] = []

    class MockProcessor(BaseProcessor):
        """Simple mock processor for testing"""

        @staticmethod
        def create(argv):
            pass

        async def process(self, dataset):
            assert dataset is not None
            datasets.append(dataset)

        async def close(self):
            pass

    await arun_emulated_sdp_pipeline(
        ms,
        PLASMA_SOCKET,
        20000000,
        MockProcessor(),
        **kwargs,
    )
    return datasets


@pytest.mark.parametrize("num_processed_scans, num_sent_scans", [(2, 3)])
@pytest.mark.asyncio
async def test_max_scans(num_processed_scans: int, num_sent_scans: int):
    """Tests the stop behaviour when a configured number of scans have been received."""

    class MockProcessor(BaseProcessor):
        """Simple mock processor for testing"""

        ended = []

        @staticmethod
        def create(argv):
            pass

        async def process(self, dataset):
            pass

        async def end_scan(self, scan_id: int) -> None:
            self.ended.append(scan_id)

        async def close(self):
            pass

    processor = MockProcessor()

    assert num_processed_scans < num_sent_scans
    with PlasmaStoreContext(PLASMA_SOCKET, 20000000):
        runner = Runner(
            PLASMA_SOCKET,
            [processor],
            polling_rate=0.1,
            use_sdp_metadata=False,
            max_scans=num_processed_scans,
        )
        async with create_plasma_consumer(str(INPUT_MS_PATH), PLASMA_SOCKET) as consumer:
            processor_task = asyncio.create_task(runner.run())
            consumer_task = asyncio.create_task(
                arun_plasma_writer_and_runner(
                    runner,
                    consumer,
                    str(INPUT_MS_PATH),
                    num_scans=num_sent_scans,
                )
            )

            await processor_task
            consumer_task.cancel()

    assert processor.ended == list(range(1, num_processed_scans + 1))


@pytest.mark.asyncio
async def test_requests_burst():
    """Tests that request bursts are processed as fast as possible"""

    POLLING_RATE = 1
    processor = CounterProcessor()

    with PlasmaStoreContext(PLASMA_SOCKET, 20000000):
        # Bring runner up first, otherwise the plasma consumer will see no
        # processors and won't publish anything to Plasma
        runner = Runner(
            PLASMA_SOCKET,
            [processor],
            polling_rate=POLLING_RATE,
            use_sdp_metadata=False,
            max_scans=1,
        )

        # Create and publish data through plasma_writer consumer, making sure it
        # keeps all RPCs around for the runner to process them. Note that the
        # arun_plasma_writer coro issues one process_payload RPC per time step,
        # plus the start/end_scan RPCs.
        plasma_writer = create_plasma_consumer(
            INPUT_MS_PATH, PLASMA_SOCKET, payloads_in_flight=TOTAL_TIMESTEPS + 2
        )
        await arun_plasma_writer(
            runner,
            plasma_writer,
            str(INPUT_MS_PATH),
            wait_for_each_response=False,
        )

        # Now let the processor run through all pending requests
        start = time.monotonic()
        await runner.run()
        end = time.monotonic()

    # This is a very simple assertion, but is the only truth we can check
    # with high confidence -- and it fails with our previous Runner.run main
    # loop implementation
    assert (end - start) < POLLING_RATE * 3
