"""Various utilities to assist with testing code using Kafka"""

import asyncio
import contextlib
from dataclasses import dataclass
from typing import Iterable

from aiokafka import AIOKafkaConsumer, AIOKafkaProducer
from aiokafka.admin import AIOKafkaAdminClient
from aiokafka.producer.message_accumulator import BatchBuilder

KAFKA_HOST = "localhost:9092"


@contextlib.asynccontextmanager
async def auto_delete_producer(*topics: str):
    """Deletes the provided topics, then yields a producer"""
    admin_client = AIOKafkaAdminClient(bootstrap_servers=KAFKA_HOST)
    await admin_client.start()
    existing_topics = set(await admin_client.list_topics()).intersection(topics)
    await admin_client.delete_topics(existing_topics)
    await admin_client.close()

    producer = AIOKafkaProducer(bootstrap_servers=KAFKA_HOST)
    await producer.start()
    yield producer
    await producer.stop()


@contextlib.asynccontextmanager
async def earliest_consumer(*topics: str):
    """
    Yields a consumer that will consume all messages from the start of a topic.
    By default consumers will only read messages that are produced after the
    consumer has been started.
    """
    consumer = AIOKafkaConsumer(
        *topics,
        bootstrap_servers=KAFKA_HOST,
        auto_offset_reset="earliest",
    )

    await consumer.start()
    yield consumer
    await consumer.stop()


@dataclass
class KafkaBatchMessage:
    """Represents a single message that can be written to a Kafka batch"""

    value: bytes | bytearray | memoryview
    """The bytes of the message to be written to kafka"""
    timestamp_ms: int | None = None
    """The timestamp of the message, as Unix time in milliseconds"""


async def asend_in_batches(
    producer: AIOKafkaProducer,
    topic_messages: dict[str, Iterable[KafkaBatchMessage]],
):
    """
    Write the provided messages to each topic using the Kafka batch API
    which minimises the amount of time to send large numbers of messages
    (as less requests are made to the Kafka server)
    """

    def generate_batches(data: Iterable[KafkaBatchMessage]):
        kafka_batch = producer.create_batch()

        for datum in data:
            # Docs say unix secs as float but it accepts unix millisecs as int!
            metadata = kafka_batch.append(
                key=None, value=datum.value, timestamp=datum.timestamp_ms
            )

            if metadata is None:
                yield kafka_batch
                kafka_batch = producer.create_batch()
                metadata = kafka_batch.append(
                    key=None, value=datum.value, timestamp=datum.timestamp_ms
                )
                assert metadata, "Unable to add message to new batch!"

        yield kafka_batch

    async def aqueue_batches_for_send(topic: str, batches: Iterable[BatchBuilder]):
        """
        Returns a list of futures that will each complete once the respective
        batch has been delivered to Kafka
        """
        partitions = await producer.partitions_for(topic)
        partition = next(iter(partitions))

        return [await producer.send_batch(batch, topic, partition=partition) for batch in batches]

    delivery_futures = []
    for topic, message in topic_messages.items():
        delivery_futures += await aqueue_batches_for_send(
            topic,
            generate_batches(message),
        )

    await asyncio.gather(*delivery_futures)
