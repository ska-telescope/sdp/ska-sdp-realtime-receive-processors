"""
MSWriterProcessor integration tests
"""

import logging
import tempfile
from pathlib import Path

import numpy as np
import pytest
from realtime.receive.core.ms_asserter import AssertProps, MSAsserter

from realtime.receive.processors.file_executor import FunctionFileExecutor
from realtime.receive.processors.sdp.average_processor import AverageTimeProcessor
from realtime.receive.processors.sdp.ms_event_handler import FileExecutorMsClosedEventHander
from realtime.receive.processors.sdp.mswriter_processor import MSWriterProcessor
from realtime.receive.processors.sdp.rfi_mask_processor import RfiMaskProcessor
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline
from tests.test_utils import untar

PLASMA_SOCKET = "/tmp/plasma"
logger = logging.getLogger(__name__)


@pytest.fixture(scope="session", name="sim_vis_ms")
def sim_vis_ms_fixture() -> Path:
    """Returns the path to the unpacked sim-vis.ms file"""
    return untar("data/sim-vis.ms.tar.gz")


@pytest.fixture(scope="session", name="expected_output_ms")
def expected_output_ms_fixture() -> Path:
    """Returns the path to the unpacked sim-vis-flagged.ms file"""
    return untar("data/sim-vis-flagged.ms.tar.gz")


@pytest.mark.asyncio
@pytest.mark.xfail(reason="mswriter does not yet write flags")
async def test_rfi_mask_pipeline(sim_vis_ms, expected_output_ms):
    """Tests RFI masking with mswriter processor."""

    output_ms_paths = []

    def append_ms(ms_path: str):
        output_ms_paths.append(ms_path)

    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("sim-vis-output.ms")

        processors = [
            RfiMaskProcessor(rfi_mask=np.array([[1.498e8, 1.499e8]])),
            MSWriterProcessor(
                output_ms_template,
                timestamp_output=True,
                event_handlers=[FileExecutorMsClosedEventHander(FunctionFileExecutor(append_ms))],
            ),
        ]

        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            sim_vis_ms,
            PLASMA_SOCKET,
            50000000,
            processors,
            num_scans=1,
        )

        asserter = MSAsserter()
        for output_ms_path in output_ms_paths:
            asserter.assert_ms_equal(
                str(expected_output_ms),
                output_ms_path,
                columns=[
                    AssertProps("DATA", True, True),
                    AssertProps("FLAG", True, True),
                    AssertProps("ANTENNA1", True, True),
                    AssertProps("ANTENNA2", True, True),
                ],
                column_desc_keys=[
                    "ndim",
                    "valueType",
                    "_c_order",
                    "maxlen",
                ],
            )


@pytest.mark.asyncio
async def test_average_time_pipeline(sim_vis_ms, expected_output_ms):
    """Tests RFI masking with mswriter processor."""

    output_ms_paths = []

    def append_ms(ms_path: str):
        output_ms_paths.append(ms_path)

    with tempfile.TemporaryDirectory() as temp_dir:
        # write all outputs to temp directory
        output_ms_template = temp_dir / Path("sim-vis-output.ms")

        processors = [
            AverageTimeProcessor(timestep=1, flag_threshold=0.5),
            MSWriterProcessor(
                output_ms_template,
                timestamp_output=True,
                event_handlers=[FileExecutorMsClosedEventHander(FunctionFileExecutor(append_ms))],
            ),
        ]

        # run plasma writer and processor til done
        await arun_emulated_sdp_pipeline(
            sim_vis_ms,
            PLASMA_SOCKET,
            50000000,
            processors,
            num_scans=1,
        )

        asserter = MSAsserter()
        for output_ms_path in output_ms_paths:
            asserter.assert_ms_equal(
                str(expected_output_ms),
                output_ms_path,
                columns=[
                    AssertProps("DATA", True, True),
                    # AssertProps("FLAG", False, False),
                    AssertProps("ANTENNA1", True, True),
                    AssertProps("ANTENNA2", True, True),
                ],
                column_desc_keys=[
                    "ndim",
                    "valueType",
                    "_c_order",
                    "maxlen",
                ],
            )
