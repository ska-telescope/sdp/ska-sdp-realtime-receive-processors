Change Log
###########

Development
-----------

Added
^^^^^

 * Added processor classes previously defined in
   `receive-integration <https://gitlab.com/ska-telescope/sdp/ska-sdp-realtime-receive-integration>`_
   to this package.

3.0.0
-----

Added
^^^^^

 * Support for publishing messages to a Kafka queue whenever a Measurement Set is closed.

Fixed
^^^^^

* Use correct DataProduct database path

Changed
^^^^^^^

* Update data product flow state instead of creating
* Migrated the Signal Display Metric Generator into this repo. Reference
  to the old `code <https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-metric-generator>`_
  and old `documentation <https://developer.skao.int/projects/ska-sdp-qa-metric-generator/en/latest/>`_
  for previous version information.
* **BREAKING** Bumped dependency on ``ska-sdp-config`` and
  ``ska-sdp-dataproduct-metadata`` to ``^1.0.0``,
  and on ``ska-sdp-realtime-receive-core`` to ``^7.0.0``
  to support SDP v1.
  Accordingly, bumped dependency on ``ska-sdp-realtime-receive-modules`` to ``^6.0.0``.

2.3.1
-----

 * Added support for SKART

2.3.0
-----

Changed
^^^^^^^

* Updated dependency on ska-sdp-dataproduct-metadata to "0.6.0"
* Updated dependency on ska-sdp-realtime-receive-core to "6.5.0"
* Updated dependency on ska-sdp-realtime-receive-modules to "5.2.0"

2.2.6
-----

Added
^^^^^
 * Added a finish and close method to flush the invocations in the queueing processors

2.2.4
-----

Changed
^^^^^^^

* Updated dependency on aiokafka to ``0.11.0``.
* Updated dependency on ska-sdp-datamodels to ``0.3.0``.

2.2.3 [YANKED]
--------------

2.2.2 [YANKED]
--------------

2.2.1 [YANKED]
--------------

2.2.0
-----

Added
^^^^^

* Simple change to the runner to keep track of the number of END_SCANS that are
  received and to only propagate the END_SCAN to the processors when the
  number of END_SCANS is equal to the number of scans that
  the user has requested to process.

2.1.0
-----

Added
^^^^^

* Added support for one or more user processors.
* Added a processor to accumulate frequency split Visibilities into a single Visibility.
* Added ability for ``mswriter_processor`` to write flags.

Changed
^^^^^^^

* Changed ``BaseProcessor.process`` signature to support a return value to pass into a subsequent processor.

Deprecated
^^^^^^^^^^

* Deprecated ``NullProcessor`` in favor of ``CounterProcessor``.

2.0.2
-----

Added
^^^^^

* Allow Kafka pointing payloads to have an ``antenna_name`` field.
  If present, it's preferred over the ``antenna_id`` one,
  and is validated against the antenna list that makes up the subarray.

* The ``DatasetMsWriterHelper.sw and DatasetMsWriterHelper.sw_slice`` properties
  have been added to the ``MSWriterProcessor`` to allow the user to access the
  spectral window and spectral window slice being written to the Measurement Set.
  Previously it was assumed that the spectral window was the same as the scan
  and being written in full. This has been updated to allow for the possibility
  receiving datasets from more than one receiver

Changed
^^^^^^^

* The ``mswriter_processor`` now writes the data to the Measurement Set in
  frequency slices as they are received. This permits the use of a single
  processor and plasma store with multiple instances of the receiver.

Fixed
^^^^^

* The ``Visibility.configuration.location`` attribute is now set.
  Its value is currently the same as the first antenna's position,
  but in future versions the array centre will be used instead.

2.0.1
-----

* Fix error in ``mswriter`` processor when trying to write pointing data
  but there is no pointing data to write.

2.0.0
-----

Added
^^^^^

* When ingesting pointings from Kafka, instead of waiting blindly for an
  entire second when the Measurement Set is requested to be closed, stop
  as soon as a kafka message is received with a timestamp after the
  timestamp of the final visibility (or the original timeout is reached).

Changed
^^^^^^^

* ``BaseProcessor.process()`` return value is now ignored when deciding whether
  to continue processing payloads
* Updated dependency on ska-realtime-receive-modules to ``^5.0.0``.

Removed
^^^^^^^

* **BREAKING** ``BaseProcessor.timeout()``, as well as ``payload_timeout`` on both the
  CLI and ``Runner``  have been removed as the new ``end_scan()`` method
  explicitly handles when processing should finish.

2.0.0-alpha.2
-------------

Fixed
^^^^^

* ``MSWriterProcessor`` now supports writing to measurement sets where the
  corresponding spectral window has a non-zero starting channel_id.

2.0.0-alpha.1
-------------

Added
^^^^^

* User processors can now react to a scan starting of ending in SDP
  by implementing the ``start_scan`` and ``end_scan`` coroutine methods,
  both of which take the scan ID as argument.
  When running against an input Measurement Set,
  a single scan is simulated.
* New ``--max-scans N`` command line option available to all processors,
  allowing a processor to exit automatically after ``N`` scans worth of data
  have been streamed through.
* If ``SDP_PB_ID`` is available as an environment variable,
  the ``mswriter`` processor will now append the path of each
  measurement set to SdpConfig in a manner that adheres to ADR-81.

Changed
^^^^^^^

* The ``mswriter`` processor now closes the Measurement Set it is writing
  when its ``end_scan`` method is invoked,
  thus flushing data to disk earlier than it used to.
* Updated dependency on ska-sdp-dal-schemas to ``^0.6.1``.
* Updated dependency on ska-realtime-receive-modules to ``^4.3.0-alpha.1``.

Removed
^^^^^^^

 * **BREAKING** ``MSWriterProcessor``\ 's
   ``max_payloads`` and ``max_ms`` arguments
   and corresponding command-line options
   have been removed.
   Use the more generic ``--max-scans`` command line option
   that applies equally to all processors instead.
* **BREAKING**: ``NullProcessor``\ 's
  ``max_datasets_to_process``
  and corresponding command-line option
  has also been removed,
  use ``-max-scans`` instead.

1.3.0
-----

Added
^^^^^

* Add ``--source-offset-topic`` option to ``mswriter`` processor
  to populate a true SOURCE_OFFSET in the
  measurement set rather than calculating it from ``actual - commanded``.
  Not providing this option is now deprecated.

Changed
^^^^^^^

* Updated dependency on ska-realtime-receive-core to ``^6.1.0``.

1.2.0
-----

Added
^^^^^

* The ``mswriter`` processor now reads the Kafka server host
  from the ``SDP_KAFKA_HOST`` environment variable
  if ``--kafka-server`` isn't provided.

1.1.1
-----

Changed
^^^^^^^

* Updated dependency on ska-sdp-dal-schemas to ``~0.6.0``.
* Updated dependency on ska-realtime-receive-modules to ``^4.2.1``.

1.1.0
-----

Changed
^^^^^^^

* Updated dependency on pyarrow to ``==11.0.0``
  to get binary wheels when installing under Python 3.11.
* Updated dependency on ska_sdp_realtime_receive_modules to ``^4.2.0``.

1.0.1
-----

Fixed
^^^^^

* The ``MetaData`` class from ``ska-sdp-dataproduct-metadata`` package
  was being incorrectly used after updating the update to ``~0.2.0``.


1.0.0
-----

Added
^^^^^

* Add the option to send a subset of channels when running with
  ``--input`` (using ``--input-channel-range``)
  or when using the ``arun_emulated_sdp_pipeline()`` function
  (using the ``channels`` parameter).

Changed
^^^^^^^

* **BREAKING** Use ``Visibility`` class from ska-sdp-datamodels
  as the main data model
  to handle visibilities and their metadata
  to user-written processors.
  The main differences are:

  * ``attr.plasma_refs`` has moved to ``attr.meta.plasma_refs``.
  * ``attr.configuration.xyz`` now has dimensions ``(#antenna, 3)``.

* **BREAKING**
  Processors now listen to the new ``process_visibility`` RPC call
  rather than the old ``read_payload`` one.
  This means that this package is now incompatible
  with receiver versions ``< 4``.
* **BREAKING** Bump dependency on receive-core to ``^6.0.0``
* Processors are now provided correct channel frequencies and bandwidths
* ``MSWriterProcessor`` now writes correct spectral window
  information to the measurement set
* Prevent ``MSWriterProcessor`` from crashing when an invalid
  Kafka host is provided
* Prevent duplicate commanded pointing timestamps from crashing the
  ``MSWriterProcessor`` by discarding later samples.

Fixed
^^^^^

* Prevent ``MSWriterProcessor`` aborting reception of visibilities
  when a Kafka connection takes a long time or times out.


1.0.0-alpha.3
-------------

Fixed
^^^^^

* Prevent ``MSWriterProcessor`` aborting reception of visibilities
  when a Kafka connection takes a long time or times out.


1.0.0-alpha.2
-------------

Added
^^^^^

* Add the option to send a subset of channels when running with
  ``--input`` (using ``--input-channel-range``)
  or when using the ``arun_emulated_sdp_pipeline()`` function
  (using the ``channels`` parameter).

Changed
^^^^^^^

* Prevent ``MSWriterProcessor`` from crashing when an invalid
  Kafka host is provided
* Prevent duplicate commanded pointing timestamps from crashing the
  ``MSWriterProcessor`` by discarding later samples.

Fixed
^^^^^

* Correctly determine frequency and bandwidth information by reading
  the channel IDs from plasma and cross-checking it with the scan's
  spectral window.


1.0.0-alpha.1
-------------

Changed
^^^^^^^

* **BREAKING** Use ``Visibility`` class from ska-sdp-datamodels
  as the main data model
  to handle visibilities and their metadata
  to user-written processors.
  The main differences are:

  * ``attr.plasma_refs`` has moved to ``attr.meta.plasma_refs``.
  * ``attr.configuration.xyz`` now has dimensions ``(#antenna, 3)``.

* **BREAKING**
  Processors now listen to the new ``process_visibility`` RPC call
  rather than the old ``read_payload`` one.
  This means that this package is now incompatible
  with receiver versions ``< 4``.

* Bump dependency on receive-core to ``^5.1.3``
  to properly store spectral window total bandwidth.


0.5.1
-----

Changed
^^^^^^^

* Bump dependency on receive-core to ``^5.1.0``
  to properly store baselines read from Plasma
  into output Measurement Sets.

0.5.0
-----

Added
^^^^^
* Allow pointings table in the MS writer processor to be populated from Kafka.

Changed
^^^^^^^

* Updated dependency on receive-core to ``^5.0.1``
  for imaging-compliant Measurement Set writing,
  and improved Measurement Set access.
* Output value written by a processor is a 1-element array
  with ``0`` if there was no problem reading the inputs
  and ``1`` if any problem was found
  (previously it was always an empty array).
* The plasma timeout used internally during SDP pipeline emulation
  has been reduced from ``10`` to ``2`` seconds
  in order to be more strict with user-written processors
  that might block the asyncio loop thread for too long.

Fixed
^^^^^

* Always write an output value into the plasma store
  after reading the input values.
  This was not happening in the case of errors
  while converting the inputs into a Visibility object.
* Bursts of RPC calls coming from Plasma
  are forwarded as fast as possible to the user-written processor.
* When running a processor against an input Measurement Set,
  start the internal ``plasma_store`` subprocess in a new session
  to avoid signals sent to the parent process
  from being sent to the ``plasma_store`` as well.
  This caused race conditions during the shutdown
  of the SDP pipeline emulation logic.
* Handle plasma timeouts during SDP pipeline emulation correctly,
  as they most probably indicate a processor
  that has decided to finish its execution earlier.

0.4.0
-----

Added
^^^^^
* Processors now have access to a ``Storage`` class
  that allows them to define the files they will create.
  In production deployments this class will be backed
  by the SDP metadata library,
  which ensures files are created in the correct location,
  and they are recorded in a metadata file.

Changed
^^^^^^^
* Switched project build from setuputils to Poetry
* Ensured processors successfully disconnect from the Plasma Store
  when they finish.
* Updated dependency on receive-modules to 3.6.0.
* Updated dependency on cbf-emulator to 4.2.0.
* Changed ``MSWriterProcessor`` to use new ``Storage`` class.

0.3.0
-----

* Allow cancelling tasks in CLI and tests
* Update Dockerfile to use `ubuntu:22.04` and do a proper multi-stage build

0.2.0
-----

* Update dependencies on receive-core, receive-modules, cbf-emulator and ska-sdp-dal-schemas
  to latest available versions.
* Update MSWriterProcessor to use new BaseProcessor & remove deprecated PlasmaPayloadProcessor

0.1.1
-----

* First tag, partially operational but still just a development release.
* Added docker image build and publish to CAR.
