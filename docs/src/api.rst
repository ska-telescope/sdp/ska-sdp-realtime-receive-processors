API documentation
=================

CLI interface
-------------

.. sphinx_argparse_cli::
  :module: realtime.receive.processors.main
  :func: _build_parser
  :prog: plasma_processor
  :title:
  :description: Unknown arguments will be passed to your processor create() function


Processor Interface
-------------------

.. autoclass:: realtime.receive.processors.sdp.base_processor.BaseProcessor
   :members:


Storage
-------

.. automodule:: realtime.receive.processors.storage
   :members:


In-built Processors
-------------------

.. Potential future in-built processors include no-op and logging processors
.. autoclass:: realtime.receive.processors.sdp.mswriter_processor.MSWriterProcessor



Others
------

.. autoclass:: realtime.receive.processors.file_executor.FileExecutor
.. autoclass:: realtime.receive.processors.file_executor.FunctionFileExecutor
.. autoclass:: realtime.receive.processors.file_executor.CommandFileExecutor
