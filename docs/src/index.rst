.. ska-sdp-realtime-receive-processors documentation master file, created by
   sphinx-quickstart on Fri Jun 24 14:12:58 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SDP Receive Processors
======================

This package contains the shared code for the SDP receiver processors.

*Processors* are programs that connect to a Plasma store
and receive payloads from the receiver pipeline.
This package aims to ease the development of such programs,
dealing with all the complexity of talking to plasma,
unpacking payloads,
allowing developers to concentrate on their business logic.


Context
-------

At a high level the realtime receive processors lie at the end of the realtime receive pipeline,
each working with data written to the plasma store by the *Receiver* (in the
:doc:`ska-sdp-realtime-receive-modules:index` repository). The receiver has a single job,
and that is to unpack visibilities received over the network and write them to the Plasma store so
the processors can do their work. *Processors* can perform a variety of functions, for example one
might write data to a measurement set for later analysis (implemented in this repo:
:class:`~realtime.receive.processors.sdp.mswriter_processor.MSWriterProcessor`) or performing
some calculations to perform realtime calibration.

.. This diagram can be edited by opening the file directly in https://app.diagrams.net
.. figure:: _static/img/realtime-receive.drawio.svg


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   api
   processors/signal_display_metrics


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
