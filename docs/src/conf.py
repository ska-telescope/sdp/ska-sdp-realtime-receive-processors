# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

from sphinx_pyproject import SphinxConfig

sys.path.insert(0, os.path.abspath("../../src/"))

# -- Project information -----------------------------------------------------

pyproject = globals()
config = SphinxConfig("../../pyproject.toml", globalns=pyproject)

project = pyproject["name"]
copyright = "2022, Rodrigo Tobar, Steve Ord, Callan Gray"
author = pyproject["author"]

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.intersphinx",
    "sphinx_argparse_cli",
]

autodoc_mock_imports = []

html_theme = "sphinx_rtd_theme"
html_context = {
    "favicon": "img/favicon.ico",
    "logo": "img/logo.jpg",
    "theme_logo_only": True,
}
htmlhelp_basename = "developerskatelescopeorgdoc"

intersphinx_mapping = {
    "ska-sdp-realtime-receive-core": (
        "https://developer.skatelescope.org/projects/ska-sdp-realtime-receive-core/en/latest",
        None,
    ),
    "ska-sdp-realtime-receive-modules": (
        "https://developer.skatelescope.org/projects/ska-sdp-realtime-receive-modules/en/latest",
        None,
    ),
    "ska-sdp-datamodels": (
        "https://developer.skao.int/projects/ska-sdp-datamodels/en/latest",
        None,
    ),
    "ska-sdp-qa-display": (
        "https://developer.skao.int/projects/ska-sdp-qa-display/en/latest",
        None,
    ),
    "ska-sdp-dataqueues": (
        "https://developer.skao.int/projects/ska-sdp-dataqueues/en/latest",
        None,
    ),
}

autosectionlabel_prefix_document = True
