"""Storage organisation classes"""

import os

from ska_sdp_dataproduct_metadata.metadata import File as SdpFile
from ska_sdp_dataproduct_metadata.metadata import MetaData


class File:
    """
    A representation of a file to be written by a processor.

    Users shouldn't create this directly, instead they should use the
    Storage.declare_new_file factory method.

    :param metadata_file: An SDP metadata File class pointing to the path of
     the file.
    :param path: The path to the file when not using the SPD MetaData class.
    """

    def __init__(
        self,
        sdp_file: SdpFile | None = None,
        path: str | None = None,
    ):
        assert bool(sdp_file) != bool(path)
        self._sdp_file: SdpFile | None = sdp_file
        self._path: str | None = path

    def update_status(self, status):
        """
        Updates the status of this file to `status`.

        :param status: The new status of the file.
        """
        if self._sdp_file:
            self._sdp_file.update_status(status)

    @property
    def local_path(self) -> str:
        """The path to this file on the local filesystem"""
        if self._sdp_file:
            return self._sdp_file.full_path
        return self._path

    @property
    def sdp_path(self) -> str | None:
        """
        The path to this file as a globally accessible SDP path. If not
        running in the context of SDP, returns None.
        """
        if not self._sdp_file:
            return None

        # Ideally would be a property exposed on SdpFile like full_path
        # We're basing this off the code here:
        # https://gitlab.com/ska-telescope/sdp/ska-sdp-dataproduct-metadata/-/blob/0.2.1/src/ska_sdp_dataproduct_metadata/metadata.py?ref_type=tags#L100-111
        # pylint: disable-next=protected-access
        sdp_path = self._sdp_file.full_path.removeprefix(self._sdp_file._metadata._root)
        return os.path.normpath(f"/{sdp_path}")


# pylint: disable=too-few-public-methods
class Storage:
    """
    Manages filesystem storage of processors.

    Used by processors to declare a new file that should
    be written to disk. allows processors to use the MetaData machinery or not
    based on a runtime toggle.

    :param use_sdp_metadata: Whether to use the SDP MetaData machinery or not.
    """

    def __init__(self, use_sdp_metadata: bool = True):
        self._metadata: MetaData | None = None
        if use_sdp_metadata:
            self._metadata = MetaData()
            self._metadata.load_processing_block(
                mount_path=f"{os.getenv('SDP_DATA_PVC_MOUNT_PATH', '')}"
            )

    def declare_new_file(self, path: str, description: str) -> File:
        """
        Declares that a new file will be created.

        :param path: The path where the file will be created.
        :param description: A description of the new file.
        :return: A File object representing the new file.
        """
        if self._metadata:
            return File(sdp_file=self._metadata.new_file(path, description))
        return File(path=path)
