"""
command_executor for realtime.receive.processors.
"""

import logging
import queue
import subprocess as sp
from abc import ABCMeta, abstractmethod
from concurrent.futures import Future, ThreadPoolExecutor
from typing import Callable, List, Union

from overrides import override

logger = logging.getLogger(__name__)


class FileExecutor(metaclass=ABCMeta):
    """
    Executes jobs in the background for files that get scheduled
    to this instance.
    """

    @abstractmethod
    def schedule(self, filename) -> Future:
        """Schedules execution for the given filename"""
        raise NotImplementedError

    @abstractmethod
    def start(self):
        """Begin background execution of the scheduled filenames"""
        raise NotImplementedError

    @abstractmethod
    def stop(self):
        """Awaits any background processing commands."""
        raise NotImplementedError


class FunctionFileExecutor(FileExecutor):
    """
    A file executor that schedules the execution of a function
    on the output filename in a background thread.

    Functions are executed on a single background thread, one after another.
    """

    def __init__(self, fun: Callable[[str], None]):
        self._fn = fun
        self._to_process = queue.Queue()
        self._pool = ThreadPoolExecutor(max_workers=1)
        self._future: Union[Future, None] = None

    @override
    def start(self):
        self._future = self._pool.submit(self._run_functions)
        return self

    def _run_functions(self):
        while True:
            # An enqueued None signals the end of the loop
            ms_filename = self._to_process.get(block=True)
            if not ms_filename:
                break
            self._fn(ms_filename)

    def schedule(self, filename):
        assert self._future, f"{self} not entered"
        self._to_process.put_nowait(filename)

    @override
    def stop(self):
        self._to_process.put_nowait(None)
        # result must be called to propagate thread exceptions
        # to main thread
        if self._future:
            self._future.result(timeout=1)


class CommandFileExecutor(FunctionFileExecutor):
    """
    A file executor that schedules the execution of a bash command
    on the output filename in a background thread.

    Commands are executed on a single background thread, one after another.
    """

    def __init__(self, command_template: List[str]):
        """
        Args:
            command_template (str): command where literal %s gets substituted
            for the generated measurement set path
        """
        self._command_template = command_template
        super().__init__(self._run_command)

    def _run_command(self, ms_filename: str):
        cmd = [self._replace_fname(arg, ms_filename) for arg in self._command_template]
        cmdline = sp.list2cmdline(cmd)
        logger.info("Launching: %s", cmdline)
        # use bash to support operators e.g. &&, ||, >
        result = sp.run(
            ["bash", "-c", cmdline],
            stdout=sp.PIPE,
            stderr=sp.PIPE,
            check=False,
        )

        msg = f'Command "{cmdline}" exited with status {result.returncode}.\n'
        if result.stdout:
            msg += f"[stdout]\n{result.stdout.decode().rstrip()}"
        if result.stderr:
            msg += f"[stderr]\n{result.stderr.decode().rstrip()}"
        if result.returncode == 0:
            logger.info(msg)
        else:
            logger.error(msg)

    @staticmethod
    def _replace_fname(arg: str, filename: str):
        if "%s" in arg:
            return arg % filename
        return arg
