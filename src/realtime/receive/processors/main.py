"""
main for realtime.receive.processors.
"""

import argparse
import asyncio
import functools
import logging
import signal

import ska_ser_logging
from realtime.receive.core import ChannelRange
from realtime.receive.core.common import strtobool

from realtime.receive.processors.runner import Runner
from realtime.receive.processors.sdp import BaseProcessor
from realtime.receive.processors.utils.processor_utils import load_base_processor_class
from realtime.receive.processors.utils.runner_utils import arun_emulated_sdp_pipeline

logger = logging.getLogger(__name__)


def load_processors(
    processor_classes: list[str], processor_args: list[str]
) -> list[BaseProcessor]:
    """Loader for processors from a config and command line arguments.

    Args:
        processor_classes (list[str]): list of class import paths.
        processor_args (list[str]): combined list of arguments for all processors.

    Returns:
        list[BaseProcessor]: list of loaded processors.
    """
    logger.info("Creating processors: %s", " -> ".join(processor_classes))
    return [
        load_base_processor_class(processor_class).create(processor_args)
        for processor_class in processor_classes
    ]


def main():
    """
    Main function.
    """

    parser = _build_parser()

    args, user_processor_args = parser.parse_known_args()

    logging_level = logging.DEBUG if args.verbose else logging.INFO
    ska_ser_logging.configure_logging(level=logging_level)

    async def amain():
        user_processors = load_processors(
            args.user_processor_class.split(","), user_processor_args
        )
        if args.input:
            emulated_sdp_pipeline_task = asyncio.create_task(
                arun_emulated_sdp_pipeline(
                    input_ms_filepath=args.input,
                    plasma_socket=args.plasma_socket,
                    mem_size=1000000000,
                    user_processors=user_processors,
                    num_receivers=1,
                    readiness_file=args.readiness_file,
                    polling_rate=1,
                    channels=args.input_channel_range,
                )
            )

            def shutdown(signame: signal.Signals):
                logger.warning(
                    "%s Signal received, stopping SDP pipeline emulation..",
                    signame.name,
                )
                emulated_sdp_pipeline_task.cancel()

            loop = asyncio.get_running_loop()
            for signame in (signal.SIGINT, signal.SIGTERM):
                loop.add_signal_handler(signame, functools.partial(shutdown, signame))
            try:
                await emulated_sdp_pipeline_task
            except asyncio.CancelledError:
                pass
        else:
            runner = Runner(
                plasma_socket=args.plasma_socket,
                user_processors=user_processors,
                num_receivers=args.num_receivers,
                readiness_file=args.readiness_file,
                polling_rate=1,
                use_sdp_metadata=args.use_sdp_metadata,
            )

            async def shutdown(signame: signal.Signals):
                logger.warning("%s Signal received, stopping runner..", signame.name)
                await runner.close()

            loop = asyncio.get_running_loop()
            for signame in (signal.SIGINT, signal.SIGTERM):
                loop.add_signal_handler(
                    signame,
                    lambda signame=signame: asyncio.create_task(shutdown(signame)),
                )
            await runner.run()

    asyncio.run(amain())


def _build_parser():
    parser = argparse.ArgumentParser(description="Runs an user-provided receive processor")
    parser.add_argument(
        "user_processor_class",
        help=(
            "One or more <module_path>.<class_name> processor paths implementing BaseProcessor. "
            "A chain of processors can be specified using a comma-separated list."
        ),
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="If set, more verbose output will be produced",
        action="store_true",
    )
    parser.add_argument(
        "-s",
        "--plasma_socket",
        help="The socket where Plasma is listening for connections",
        default="/tmp/plasma",
    )
    parser.add_argument(
        "-r",
        "--readiness-file",
        help=(
            "An empty file that will be created after the processor has "
            "finished setting up, signalling it's ready to receive data"
        ),
    )
    parser.add_argument(
        "--max-scans",
        type=int,
        default=None,
        help="The number of scans to process data for before automatically exiting.",
    )
    parser.add_argument(
        "--input",
        type=str,
        default=None,
        help="Emulate data reception using a measurement set file",
    )
    parser.add_argument(
        "--input-channel-range",
        type=ChannelRange.from_str,
        default=None,
        help=(
            "Map the channel data in the measurement set to the "
            "specified channel ids using start:count[:stride] notation."
        ),
    )
    parser.add_argument(
        "--use-sdp-metadata",
        dest="use_sdp_metadata",
        type=strtobool,
        default=True,
        help="Use SDP metadata support, ignored when used with --input",
    )
    parser.add_argument(
        "--num-receivers",
        type=int,
        default=1,
        help="Number of receivers used in the pipeline",
    )

    return parser


if __name__ == "__main__":
    main()
