"""
block_visibility utils for realtime.receive.processors.
"""

import logging
from typing import Iterable, Sequence

import awkward as ak
import numpy as np
import pandas
import ska_sdp_dal as rpc
import xarray
from astropy.coordinates import EarthLocation, SkyCoord
from astropy.units import Quantity
from realtime.receive.core.channel_range import ChannelRange
from realtime.receive.core.common import from_dict
from realtime.receive.core.scan import SpectralWindow
from ska_sdp_datamodels.configuration import Configuration
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility import Visibility

logger = logging.getLogger(__name__)


def combine_visibilities(
    visibilities: Sequence[Visibility], update_refs: bool = True
) -> Visibility | None:
    """Combines a list of Visibility objects into a single Visibility object."""

    if len(visibilities) == 0:
        return None

    # Key containing the array and sort_key for the first element
    sort_key = "frequency"
    first_element_key = 0

    # Sort the list using sorted() and a lambda function
    sorted_vis = sorted(
        visibilities,
        key=lambda item: item.isel({sort_key: first_element_key}),
    )

    time_idx_ids = np.array([], dtype=int)
    exposure_ids = np.array([], dtype=float)
    channel_ids = np.array([], dtype=int)
    for vis in sorted_vis:
        vis_refs = vis.meta["plasma_refs"]
        time_idx_ids = np.concatenate((time_idx_ids, np.array(vis_refs["time_index_ref"])))
        exposure_ids = np.concatenate((exposure_ids, np.array(vis_refs["exposures_ref"])))
        channel_ids = np.concatenate((channel_ids, np.array(vis_refs["channel_ids_ref"])))

    combined = xarray.combine_by_coords(
        sorted_vis,
        data_vars="different",
        coords="different",
        fill_value={"flags": True},
        join="outer",
        combine_attrs="override",
    )

    if update_refs:
        combined.meta["plasma_refs"]["time_index_ref"] = np.unique(time_idx_ids)
        combined.meta["plasma_refs"]["exposures_ref"] = exposure_ids
        combined.meta["plasma_refs"]["channel_ids_ref"] = np.unique(channel_ids)

    assert isinstance(combined, xarray.Dataset)
    return Visibility(data_vars=combined.data_vars, coords=combined.coords, attrs=combined.attrs)


# pylint: disable-next=too-many-locals
def _create_antennas_configuration(
    antennas_table_ref: rpc.TableRef,
) -> Configuration:
    """
    Convert a pyarrow.Table received via plasma to an antenna Configuration.

    :param antennas_table: contains antenna definitions from plasma
    :return: newly formatted antenna definitions
    """
    antennas_table = antennas_table_ref.get()
    xyz = np.stack(antennas_table["xyz"].to_numpy())
    # YAN-1680: default the array location to the first antenna's.
    # This isn't necessarily correct, but adding this missing information is high priority.
    # Doing it correclty is costly; this shortcut can be found in ska-sdp-datamodels itself.
    location = EarthLocation(
        x=Quantity(xyz[0][0], "m"),
        y=Quantity(xyz[0][1], "m"),
        z=Quantity(xyz[0][2], "m"),
    )
    return Configuration.constructor(
        "Antenna Configuration",
        names=antennas_table["name"],
        xyz=xyz,
        location=location,
        mount=antennas_table["mount"],
        diameter=antennas_table["dish_diameter"],
    )


# TODO (yan-1125) reduce argument and variable count
# pylint: disable-next=unused-argument,too-many-arguments,too-many-locals
def create_block_visibility(  # pylint: disable=too-many-statements
    scan_id_ref: rpc.TensorRef,
    time_index_ref: rpc.TensorRef,
    times_ref: rpc.TensorRef,
    intervals_ref: rpc.TensorRef,
    exposures_ref: rpc.TensorRef,
    channel_ids_ref: rpc.TensorRef,
    flags_ref: rpc.TensorRef,
    weights_ref: rpc.TensorRef,
    beam_ref: rpc.TableRef,
    baselines_ref: rpc.TableRef,
    spectral_window_ref: rpc.TableRef,
    channels_ref: rpc.TableRef,
    antennas_ref: rpc.TableRef,
    field_ref: rpc.TableRef,
    polarisations_ref: rpc.TableRef,
    uvw_ref: rpc.TensorRef,
    vis_ref: rpc.TensorRef,
) -> Visibility:
    """
    Converts ska-sdp-dal objects to a Visibility dataset described by
    ska-sdp-datamodels.

    The Attributes of the Visibility dataset have a field called "meta", under
    which we insert a "plasma_refs" dictionary. This holds the value of various
    plasma references passed here and that don't have a clear place to live in
    the Visibility Dataset.
    """

    # Baseline order
    baselines_table = baselines_ref.get()
    baselines: np.ndarray = np.array([baselines_table["antenna1"], baselines_table["antenna2"]])
    baselines = pandas.MultiIndex.from_arrays(baselines, names=("antenna1", "antenna2"))

    # Channels, Spectral Window & Bandwidth
    channel_ids = channel_ids_ref.get()
    channels = ChannelRange.from_ndarray(channel_ids)

    spectral_window = _find_spectral_window_for_channel_range(
        spectral_window_ref.get_awkward(), channels
    )
    frequency = spectral_window.frequencies_for_range(channels)
    channel_bandwidth = np.repeat(spectral_window.channel_bandwidth, channels.count)

    # Polarisation frame, there's no way to build it directly from a list of
    # names
    stoke_types = set(polarisations_ref.get_awkward().to_list()[0]["corr_type"])
    for frame_name, frame_def in PolarisationFrame.polarisation_frames.items():
        if set(frame_def.keys()) == stoke_types:
            polarisation_frame = PolarisationFrame(frame_name)
            break
    else:
        raise ValueError(f"Unsupported set of stoke types: {stoke_types}")

    # Phase centre
    field = field_ref.get_awkward()
    phasecentre: SkyCoord = SkyCoord(
        field["phase_dir"]["ra"],
        field["phase_dir"]["dec"],
        frame="icrs",
        unit="rad",
    )

    # These are the additional parameters that are not used to build the
    # Visibility.
    plasma_refs = {
        "scan_id_ref": scan_id_ref.get(),
        "time_index_ref": time_index_ref.get(),
        "exposures_ref": exposures_ref.get(),
        "channel_ids_ref": channel_ids_ref.get(),
        "beam_ref": beam_ref.get(),
        "channels_ref": channels_ref.get(),
        "field_ref": field_ref.get(),
        "spectral_window_ref": spectral_window_ref.get(),
        "polarisations_ref": polarisations_ref.get(),
    }
    meta = {"plasma_refs": plasma_refs}

    return Visibility.constructor(
        frequency=frequency,
        channel_bandwidth=channel_bandwidth,
        phasecentre=phasecentre,
        configuration=_create_antennas_configuration(antennas_ref),
        uvw=uvw_ref.get(),
        time=times_ref.get(),
        vis=vis_ref.get(),
        weight=weights_ref.get(),
        integration_time=intervals_ref.get(),
        flags=flags_ref.get(),
        baselines=baselines,
        polarisation_frame=polarisation_frame,
        meta=meta,
    )


def _find_spectral_window_for_channel_range(
    spectral_windows: Iterable[ak.Record], channel_range: ChannelRange
) -> SpectralWindow:
    for sw_record in spectral_windows:
        spectral_window = from_dict(SpectralWindow, ak.to_list(sw_record))

        if spectral_window.as_channel_range().contains_range(channel_range):
            return spectral_window

    raise ValueError(
        "Unable to find matching spectral window for provided channels "
        f"(looking for {repr(channel_range)} in {spectral_windows})"
    )
