"""
Helper functions for working with BaseProcessors
"""

import importlib
from typing import Type, Union

from realtime.receive.processors.sdp.base_processor import BaseProcessor


def load_base_processor_class(user_processor_class: Union[type, str]) -> Type[BaseProcessor]:
    """
    Dynamically loads a BaseProcessor type from an import
    path string.
    """
    if isinstance(user_processor_class, type):
        cls = user_processor_class
    else:
        module, _, classname = user_processor_class.rpartition(".")
        cls = getattr(importlib.import_module(module), classname)

    if issubclass(cls, BaseProcessor):
        return cls

    raise ValueError("user_process_class not derived from BaseProcessor")
