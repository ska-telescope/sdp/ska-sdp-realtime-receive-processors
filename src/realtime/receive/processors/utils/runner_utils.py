"""
runner utilities for realtime.receive.processors.
"""

import asyncio
import logging
import math
import subprocess
from contextlib import AbstractContextManager
from pathlib import Path
from typing import Iterable, Optional, TypeVar, Union

import numpy as np
import ska_sdp_dal
from overrides import overrides
from realtime.receive.core import ChannelRange, icd, msutils, time_utils
from realtime.receive.core.icd import Payload
from realtime.receive.core.msutils import MeasurementSet
from realtime.receive.core.scan import Scan
from realtime.receive.core.uvw_engine import UVWEngine
from realtime.receive.modules.consumers import plasma_writer
from realtime.receive.modules.payload_data import PayloadData
from realtime.receive.modules.tm import MeasurementSetTM
from realtime.receive.modules.utils import sdp_visibility

from realtime.receive.processors.runner import Runner
from realtime.receive.processors.sdp.base_processor import BaseProcessor

logger = logging.getLogger(__name__)


def _create_payload(
    scan_id: int,
    channel_id: int,
    channel_count: int,
    polarisation_id: int,
    visibilities: np.ndarray,
    timestamp: float,
) -> Payload:
    """Creates a payload instance used by consumers"""
    n_freq, n_baselines = visibilities.shape[0:2]
    payload = Payload()
    payload.scan_id = scan_id
    payload.channel_id = channel_id
    payload.channel_count = channel_count
    payload.polarisation_id = polarisation_id
    payload.visibilities = visibilities
    payload.timestamp = timestamp
    payload.correlated_data_fraction = np.ones([n_freq, n_baselines])
    return payload


def create_plasma_consumer(
    input_ms_filepath: str, plasma_socket: str, **consumer_config
) -> plasma_writer.PlasmaWriterConsumer:
    """Create a plasma consumer to send Measurement Set data through plasma"""
    with MeasurementSet.open(str(input_ms_filepath)) as measurement_set:
        telescope_manager = MeasurementSetTM(measurement_set)
        uvw_engine = UVWEngine(telescope_manager.get_antennas(), swap_baselines=True)

    config = plasma_writer.PlasmaWriterConfig(
        plasma_path=plasma_socket,
        wait_for_all_responses_timeout=2,
        **consumer_config,
    )
    return plasma_writer.PlasmaWriterConsumer(config, telescope_manager, uvw_engine)


async def arun_plasma_writer(*args, **kwargs) -> None:
    """
    Asynchronously passes measurement set data through the plasma
    writer and awaits processor responses.
    """
    try:
        await _arun_plasma_writer(*args, **kwargs)
    except ska_sdp_dal.connection.TimeoutException:
        logger.warning(
            "Timed out while waiting for response, processor must have quitted in the meanwhile"
        )


# pylint: disable-next=too-many-locals
async def _arun_plasma_writer(
    runner: Runner,
    consumer: plasma_writer.PlasmaWriterConsumer,
    input_ms_filepath: str,
    num_timestamps: Optional[int] = None,
    num_scans: int = 1,
    max_channels_per_payload: Optional[int] = None,
    wait_for_each_response: bool = True,
    channels: ChannelRange | None = None,
) -> None:
    with MeasurementSet.open(input_ms_filepath) as measurement_set:
        scan_type = measurement_set.calculate_scan_type()
        if channels:
            if measurement_set.num_channels != channels.count:
                raise ValueError(
                    f"Specified channels must have the same count ({channels.count}) "
                    f"as what is contained in the measurement set ({measurement_set.num_channels})"
                )

    async def _perform_rpc_call(call, *args, **kwargs):
        await call(*args, **kwargs)
        if wait_for_each_response:
            await consumer.wait_for_oldest_reponse(2)

    # TODO (yan-1125) a limitation of vis_reader is that it expects all MS
    # data to be for the same scan_number which is not necessarily the case.
    # scan_number changes from the source MS should be preserved thus
    # vis_reader should be updated to filter by scan and/or return the
    # vis scan number.
    for scan_id in range(1, num_scans + 1):
        await _perform_rpc_call(consumer.start_scan, scan_id)
        payload_seq_no = 0

        scan = Scan(scan_id, scan_type)

        spectral_windows = (
            spectral_window
            for b in scan.scan_type.beams
            for spectral_window in b.channels.spectral_windows
        )
        if channels:
            for spectral_window in spectral_windows:
                spectral_window.start = channels.start_id
                spectral_window.stride = channels.stride
        else:
            channels = next(spectral_windows).as_channel_range()

        async for vis, timestamp in msutils.vis_reader(
            input_ms_filepath,
            num_timestamps=num_timestamps if num_timestamps is not None else 0,
        ):
            # The rest of the code assumes visibilities are in ICD order
            vis = icd.ms_to_icd(vis)
            num_chunks = (
                1
                if max_channels_per_payload is None
                else math.ceil(len(vis) / max_channels_per_payload)
            )

            first_channel_id = channels.start_id
            for visibility_chunk in np.array_split(vis, num_chunks):
                # stop sending if runner has finished early
                if not runner.active:
                    return

                payload = _create_payload(
                    scan_id=scan_id,
                    channel_id=first_channel_id,
                    channel_count=len(visibility_chunk),
                    polarisation_id=0,
                    visibilities=visibility_chunk,
                    timestamp=time_utils.mjd_to_unix(timestamp),
                )

                visibility = sdp_visibility.create_visibility(
                    [PayloadData.from_payload(payload, payload_seq_no, scan)],
                    consumer.tm,
                )
                await _perform_rpc_call(consumer.consume, visibility)

                if num_chunks == 1:
                    payload_seq_no += 1

                first_channel_id += channels.stride * len(visibility_chunk)
            if num_chunks > 1:
                payload_seq_no += 1
        await _perform_rpc_call(consumer.end_scan, scan_id)


async def arun_plasma_writer_and_runner(runner, *args, **kwargs) -> None:
    """Runs arun_plasma_writer and ensures the runner is signalled to close."""
    async with runner:
        await arun_plasma_writer(runner, *args, **kwargs)


async def _cancel_tasks(tasks: Iterable[asyncio.Task]):
    """Cancels and awaits on all tasks, ignoring CancelledError exceptions"""
    for task in tasks:
        task.cancel()
    for task in tasks:
        try:
            await task
        except asyncio.CancelledError:
            pass


# pylint: disable=too-many-arguments
async def arun_plasma_writer_and_processor(
    input_ms_filepath: Union[Path, str],
    plasma_socket: Union[Path, str],
    user_processors: list[BaseProcessor],
    num_receivers: int,
    num_timestamps: Optional[int],
    num_scans: int,
    max_channels_per_payload: Optional[int],
    readiness_file: Optional[str],
    polling_rate: float,
    use_sdp_metadata: bool,
    channels: ChannelRange | None,
):
    """
    Asynchronously runs the plasma_writer consumer and a user
    processor until all data is processed.
    """
    runner = Runner(
        str(plasma_socket),
        user_processors,
        num_receivers=num_receivers,
        readiness_file=readiness_file,
        polling_rate=polling_rate,
        use_sdp_metadata=use_sdp_metadata,
        max_scans=num_scans,
    )

    consumer = create_plasma_consumer(input_ms_filepath, plasma_socket)

    async with consumer:
        tasks = [
            asyncio.create_task(coro)
            for coro in (
                runner.run(),
                arun_plasma_writer_and_runner(
                    runner,
                    consumer,
                    str(input_ms_filepath),
                    num_timestamps=num_timestamps,
                    num_scans=num_scans,
                    max_channels_per_payload=max_channels_per_payload,
                    channels=channels,
                ),
            )
        ]
        try:
            await asyncio.gather(*tasks)
        except asyncio.CancelledError:
            await _cancel_tasks(tasks)
            raise


# pylint: disable-next=too-many-arguments
async def arun_emulated_sdp_pipeline(
    input_ms_filepath: Union[Path, str],
    plasma_socket: Union[Path, str],
    mem_size: int,
    user_processors: BaseProcessor | list[BaseProcessor],
    num_receivers: int = 1,
    num_timestamps: Optional[int] = None,
    num_scans: int = 1,
    max_channels_per_payload: Optional[int] = None,
    readiness_file: Optional[str] = None,
    polling_rate: float = 0.1,
    use_sdp_metadata: bool = False,
    channels: ChannelRange | None = None,
):
    """
    Asynchronously runs an SDP processor pipeline including plasma store,
    plasma writer, user processor and supplies emulated data.
    """
    with PlasmaStoreContext(plasma_socket, mem_size) as _:
        await arun_plasma_writer_and_processor(
            input_ms_filepath,
            plasma_socket,
            listify(user_processors),
            num_receivers=num_receivers,
            num_timestamps=num_timestamps,
            num_scans=num_scans,
            max_channels_per_payload=max_channels_per_payload,
            readiness_file=readiness_file,
            polling_rate=polling_rate,
            use_sdp_metadata=use_sdp_metadata,
            channels=channels,
        )


class PlasmaStoreContext(AbstractContextManager):
    """
    Context wrapper for a plasma store process.
    """

    _proc: subprocess.Popen

    def __init__(self, plasma_socket: Union[str, Path], mem_size: int):
        self._cmd = [
            "plasma_store",
            "-m",
            str(mem_size),
            "-s",
            str(plasma_socket),
        ]

    @overrides
    def __enter__(self):
        logger.info("creating plasma_store")
        self._proc = subprocess.Popen(self._cmd, start_new_session=True)
        return self

    @overrides
    def __exit__(self, exc_type, exc_value, traceback):
        logger.info("terminating plasma_store")

        self._proc.terminate()
        self._proc.wait()


_T = TypeVar("_T")


def listify(value: _T | Iterable[_T]) -> list[_T]:
    """Converts scalar and list-like inputs to a list."""
    if isinstance(value, list):
        return value
    if hasattr(value, "__iter__"):
        return list(value)
    return [value]
