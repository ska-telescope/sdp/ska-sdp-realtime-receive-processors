"""
Kafka notification utitilies.
"""

import dataclasses
import json
import logging

from aiokafka import AIOKafkaProducer

logger = logging.getLogger(__name__)


@dataclasses.dataclass
class NotificationMessage:
    """Class that provides a formalisation of the notification message format"""

    file: str
    time: str
    eb_id: str

    def to_dict(self) -> dict:
        """Return the notification message as a dictionary"""
        return {
            "file": self.file,
            "time": self.time,
            "metadata": {"eb_id": self.eb_id},
        }

    @classmethod
    def from_dict(cls, value: dict) -> "NotificationMessage":
        """Creates a NotificationMessage from a dictionary object"""
        return NotificationMessage(value["file"], value["time"], eb_id=value["metadata"]["eb_id"])


class KafkaNotifier:
    """Notifier that uses a message queue for notifying services of ingest events."""

    def __init__(self, server: str, topic: str):
        self._topic = topic
        self._server = server
        self.aioproducer = AIOKafkaProducer(
            bootstrap_servers=self._server,
            compression_type="gzip",
        )
        logger.info(
            "Created a Kafka Notifier, host=%s, topic=%s",
            self._server,
            self._topic,
        )

    async def start(self):
        """Start the notifier."""
        # pylint: disable-next=attribute-defined-outside-init
        await self.aioproducer.start()

    async def send_notification(self, message: NotificationMessage):
        """Send a notification to the Kafka queue"""
        await self.aioproducer.send_and_wait(
            self._topic, json.dumps(message.to_dict()).encode("utf-8")
        )

    async def stop(self):
        """Stop the notifier."""
        logger.info(
            "Stopping Kafka Notifier, host=%s, topic=%s",
            self._server,
            self._topic,
        )
        await self.aioproducer.stop()
