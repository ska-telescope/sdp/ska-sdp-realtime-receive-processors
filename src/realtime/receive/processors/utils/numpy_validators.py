"""Validators for numpy types."""

import logging
from types import MappingProxyType

import numpy as np

logger = logging.getLogger(__name__)

_FieldDesc = tuple[str, type]
"""field_name, field_dtype."""


class ValidationError(Exception):
    """Argument validation failed."""


def __get_validation_message(
    fields: MappingProxyType[str, tuple[np.dtype, int]], desc: _FieldDesc
) -> str:
    name, dtype = desc
    if name not in fields:
        return f"missing required member {name} of subdtype {dtype}"

    if not np.issubdtype(fields[name][0], dtype):
        return f"field {name} is not a subdtype of {dtype} (got {fields[name]})"
    return ""


def has_subdtype_field(value: np.ndarray, field: _FieldDesc) -> bool:
    """Determines whether an array has the given field as a subdtype.

    Args:
        value (np.ndarray): numpy array.
        field (tuple[str, type]): numpy field tuple.

    Returns:
        bool: True if the value contains the subdtype field.
    """
    fields = value.dtype.fields
    name, dtype = field
    return field[0] in fields and np.issubdtype(fields[name][0], dtype)


def validate_structured_ndarray(
    value: np.ndarray, expected_dtype: list[_FieldDesc | list[_FieldDesc]]
):
    """Validates a structured array against named dtypes. Nested lists can be used to denote a
    type union.

    Args:
        value (np.ndarray): value to be validated.
        dtype (list[NamedType | list[NamedType]]): Dtype to validate against.

    Raises:
        ValidationError: Exception containing failed checks when validation fails.

    Returns:
        value (np.ndarray): Validated value.
    """

    messages = []
    fields: MappingProxyType = value.dtype.fields or {}  # type: ignore

    for named_type_union in expected_dtype:
        if isinstance(named_type_union, list):
            # named type union
            union_messages = []
            for named_type in named_type_union:
                union_messages.append(__get_validation_message(fields, named_type))
            if all(union_messages):
                messages.append(f"dtype does not satisfy one of {named_type_union}")
                messages += map(lambda s: 4 * " " + s, union_messages)
        else:
            # simple named type
            message = __get_validation_message(fields, named_type_union)
            if message:
                messages.append(message)

    if messages:
        raise ValidationError(
            f"array of dtype {value.dtype} failed validation\n" + "\n".join(messages)
        )

    return value
