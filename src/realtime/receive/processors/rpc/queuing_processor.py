"""
dataset_queuing_processor for realtime.receive.processors.rpc.
"""

import enum
import logging

import numpy as np
import ska_sdp_dal as rpc
from sdp_dal_schemas import PROCEDURES

from realtime.receive.processors.utils.block_visibility import create_block_visibility

logger = logging.getLogger(__name__)


class Procedure(enum.Enum):
    """Enumeration of procedures that can be called on a processor"""

    START_SCAN = enum.auto()
    PROCESS_VISIBILITY = enum.auto()
    END_SCAN = enum.auto()


class InvocationResult(enum.IntEnum):
    """Enumeration of RPC invocation results"""

    SUCCESS = 0
    FAILURE = 1


Invocations = list[tuple[Procedure, tuple]]


class QueuingProcessor(rpc.Processor):
    """
    A processor that adapts incoming RPC calls into a queue of objects fit for
    consumption by SDP Processors.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(
            [PROCEDURES[name] for name in ("process_visibility", "start_scan", "end_scan")],
            *args,
            **kwargs
        )
        self.bytes_received = 0
        self._invocations: Invocations = []

    def stop(self):
        """Disconnect this processor from the underlying Plasma Store."""
        logger.warning("Disconnecting from Plasma Store")
        self._conn.client.disconnect()

    def _scan_transition(self, scan_id, procedure: Procedure, output: rpc.TensorRef):
        status = InvocationResult.SUCCESS
        try:
            invocation = procedure, (scan_id.get()[0],)
            self._invocations.append(invocation)
        except Exception:  # pylint: disable=broad-except
            logger.exception("Unexpected error while reading scan ID for %r", procedure)
            status = InvocationResult.FAILURE
        output.put(np.array([status], dtype="i1"))

    def start_scan(self, scan_id: rpc.TensorRef, output: rpc.TensorRef):
        """Enqueues a call indicating the start of a scan."""
        logger.info("Starting scan %s", scan_id.get()[0])
        self._scan_transition(scan_id, Procedure.START_SCAN, output)

    def end_scan(self, scan_id: rpc.TensorRef, output: rpc.TensorRef):
        """Enqueues a call indicating the end of a scan."""
        logger.info("Ending scan %s", scan_id.get()[0])
        self._scan_transition(scan_id, Procedure.END_SCAN, output)

    # RPC call signature pylint: disable=too-many-locals,too-many-arguments
    def process_visibility(
        self,
        scan_id: rpc.TensorRef,
        time_index: rpc.TensorRef,
        times: rpc.TensorRef,
        intervals: rpc.TensorRef,
        exposures: rpc.TensorRef,
        channel_ids: rpc.TensorRef,
        flag: rpc.TensorRef,
        weight: rpc.TensorRef,
        beam: rpc.TableRef,
        baselines: rpc.TableRef,
        spectral_window: rpc.TableRef,
        channels: rpc.TableRef,
        antennas: rpc.TableRef,
        field: rpc.TableRef,
        polarizations: rpc.TableRef,
        uvw: rpc.TensorRef,
        vis: rpc.TensorRef,
        output: rpc.TensorRef,
    ):
        """Reads rpc parameters and enques a block visibility dataset."""
        status = InvocationResult.SUCCESS
        try:
            visibility = create_block_visibility(
                scan_id_ref=scan_id,
                time_index_ref=time_index,
                times_ref=times,
                intervals_ref=intervals,
                exposures_ref=exposures,
                channel_ids_ref=channel_ids,
                flags_ref=flag,
                weights_ref=weight,
                beam_ref=beam,
                baselines_ref=baselines,
                spectral_window_ref=spectral_window,
                channels_ref=channels,
                antennas_ref=antennas,
                field_ref=field,
                polarisations_ref=polarizations,
                uvw_ref=uvw,
                vis_ref=vis,
            )
            invocation = Procedure.PROCESS_VISIBILITY, (visibility,)
            self._invocations.append(invocation)
        except Exception:  # pylint: disable=broad-except
            logger.exception("Unexpected error while converting inputs into Visibility")
            status = InvocationResult.FAILURE
        finally:
            output.put(np.array([status], dtype="i1"))

    def pop_invocations(self) -> Invocations:
        """Detaches all received invocations and returns them to the caller"""
        invocations = self._invocations
        self._invocations = []
        return invocations

    def remaining_invocations(self) -> int:
        """Returns the number of remaining invocations"""
        return len(self._invocations)
