"""
runner for realtime.receive.processors.
"""

import asyncio
import concurrent.futures
import contextlib
import logging
import time
from datetime import timedelta
from typing import Optional

from realtime.receive.processors.rpc.queuing_processor import Procedure, QueuingProcessor
from realtime.receive.processors.sdp.base_processor import BaseProcessor

from .storage import Storage

logger = logging.getLogger(__name__)


class Runner:
    """
    Runs a PayloadQueueingProcessor asynchronously, passing its payloads to a
    user-provided processing object.
    """

    def __init__(
        self,
        plasma_socket: str,
        user_processors: list[BaseProcessor],
        num_receivers: int = 1,
        polling_rate: float = 1,
        readiness_file: Optional[str] = None,
        use_sdp_metadata=True,
        max_scans: int | None = None,
    ):
        self.queuing_processor = QueuingProcessor(plasma_socket)
        self.user_processors = user_processors
        self.active = True
        self.process_pollrate = polling_rate
        self._readiness_file = readiness_file
        self._max_scans = max_scans
        self._scans_ended = 0
        self._num_receivers = num_receivers
        self._end_of_scan_received = 0
        self._start_scans_received = 0

        for user_processor in self.user_processors:
            user_processor.storage = Storage(use_sdp_metadata=use_sdp_metadata)
            user_processor.set_plasma_socket(plasma_socket)

    def _mark_as_ready(self):
        if self._readiness_file:
            with open(self._readiness_file, "wb"):
                pass

    async def run(self):
        """
        Runs the main loop, reacting to payloads being sent via RPC calls,
        and passing them to the user-provided processors.
        """
        try:
            await self._run()
        finally:
            self.queuing_processor.stop()

    async def _run(self):
        logger.info("Started processor runner, waiting for incoming calls")
        executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        loop = asyncio.get_event_loop()

        async with contextlib.AsyncExitStack() as stack:
            for user_processor in self.user_processors:
                await stack.enter_async_context(user_processor)

            self._mark_as_ready()
            while self.active:
                # Run the plasma RPC processor and translate to Visibility model
                await loop.run_in_executor(
                    executor,
                    self.queuing_processor.process,
                    self.process_pollrate,
                )

                # check if a payload was yielded, passing it to the user processor
                invocations = self.queuing_processor.pop_invocations()
                if invocations:
                    for invocation in invocations:
                        procedure, inputs = invocation
                        try:
                            if await self._invoke_procedure(
                                self.user_processors, procedure, *inputs
                            ):
                                self.active = False
                                break
                        except Exception:  # pylint: disable=broad-except
                            # We don't want to allow third party code to
                            # crash us (here and below)!
                            logger.exception("Error while processing payload")

    async def _invoke_procedure(
        self, user_processors: list[BaseProcessor], procedure: Procedure, *args
    ) -> bool:
        """
        Runs the given invocation on the user-provided processor, returns whether the runner
        should stop processing further requests.
        """
        match procedure:
            case Procedure.PROCESS_VISIBILITY:
                # pass the result of each processor to the next
                # in the chain
                try:
                    iterator = iter(user_processors)
                    result = await next(iterator).process(*args)
                    while result is not None:
                        result = await next(iterator).process(result)
                except StopIteration:
                    pass

                return False
            case Procedure.START_SCAN:
                self._start_scans_received += 1
                if self._start_scans_received == self._num_receivers:
                    self._start_scans_received = 0
                    for user_processor in user_processors:
                        await user_processor.start_scan(*args)
                    return False
            case Procedure.END_SCAN:
                self._end_of_scan_received += 1
                if self._end_of_scan_received == self._num_receivers:
                    for index, user_processor in enumerate(user_processors):
                        try:
                            iterator = iter(user_processors[index:])
                            result = await next(iterator).end_scan(*args)
                            while result is not None:
                                result = await next(iterator).process(result)
                        except StopIteration:
                            pass
                    self._scans_ended += 1
                    self._end_of_scan_received = 0
                return self._max_scans is not None and self._scans_ended >= self._max_scans
            case _:
                raise NotImplementedError(procedure)

    async def finish_and_close(self, timeout=10):
        """
        Finish the current invocations
        """
        start_time = time.time()
        interval = timedelta(seconds=timeout)
        stop_time = start_time + interval

        logger.warning("Finishing invocations")
        while self.queuing_processor.remaining_invocations() > 0 or time.time() > stop_time:
            await asyncio.sleep(0.1)

        self.active = False
        logger.warning("Closing runner")

    async def close(self):
        """Signal this runner to stop its activity and return"""
        self.active = False
        logger.warning("Closing runner")

    async def __aenter__(self):
        """Return self when entered as an asynchronous context manager"""
        return self

    async def __aexit__(self, _exc, _typ, _tb):
        """Closes this object when exited as an asynchronous context manager"""
        await self.close()
