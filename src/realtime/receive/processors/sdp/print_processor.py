"""Example PrintProcessor implementation"""

from __future__ import annotations

import logging
from typing import Iterable

from ska_sdp_datamodels.visibility.vis_model import Visibility

from realtime.receive.processors.sdp.base_processor import BaseProcessor

logger = logging.getLogger(__name__)


class PrintProcessor(BaseProcessor):
    """A processor that prints datasets, useful for tests"""

    @staticmethod
    def create(_argv: Iterable[str]) -> PrintProcessor:
        return PrintProcessor()

    async def process(self, dataset: Visibility) -> Visibility:
        logger.info("Received dataset: %r", dataset)
        return dataset

    async def close(self):
        pass
