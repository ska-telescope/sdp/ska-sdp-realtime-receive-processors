"""Class to convert data from Plasma to the Signal Metrics API.

This class is meant to be run with the Plasma receivers, and as such requires
that the plasma store is operational, as well as data is written to it in the
correct way.
"""

import argparse
import concurrent.futures
import logging
import os
import threading
from dataclasses import asdict
from time import time
from typing import Any

import numpy
import ska_sdp_config
from overrides import override
from ska_sdp_config.entity.common import KafkaUrl
from ska_sdp_datamodels.visibility import Visibility
from ska_sdp_dataqueues import DataQueueProducer, Encoding
from ska_sdp_dataqueues.schemas.signal_display_metrics import VisReceiveStatistics

from realtime.receive.processors.sdp.base_processor import BaseProcessor
from realtime.receive.processors.sdp.metrics.metrics import metrics_to_dict, process_metrics
from realtime.receive.processors.sdp.metrics.utils.config import get_metrics_list
from realtime.receive.processors.sdp.metrics.utils.flow import SignalDisplayFlow
from realtime.receive.processors.sdp.metrics.utils.utilities import Timer, create_id, metric_to_str
from realtime.receive.processors.sdp.metrics.utils.visibility import VisibilityHelper

logger = logging.getLogger(__name__)

KAFKA_MAX_BYTES = 500 * 1024 * 1024

# pylint: disable=protected-access,too-many-instance-attributes


class SignalDisplayMetrics(BaseProcessor):
    """Processer to send data to the signal displays."""

    def __init__(
        self,
        random_ids=False,
        ignore_config_db=False,
        disable_kafka=False,
        metrics: list[str] = None,
    ):
        if metrics is None:
            metrics = ["stats"]

        super().__init__()
        logger.warning("Starting...")

        self.enable_stats = "all" in metrics or "stats" in metrics
        self.received_payloads = 0
        self.received_time_slices = 0
        self.process_pollrate = 1.0
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.flows = []

        self._processing_block_id = os.environ.get("SDP_PB_ID", "pb-unknown")
        self._execution_block_id = os.environ.get("SDP_EB_ID", "eb-unknown")
        self._subarray_id = os.environ.get("SDP_SUBARRAY_ID", "01")
        self._scan_id = 0
        self._disable_kafka = disable_kafka
        self._ignore_config_db = ignore_config_db
        self.config = None
        self.first_scan = True
        self._producers: dict[str, DataQueueProducer] = {}
        self._flow_stats: SignalDisplayFlow = None

        self.time_since_last_payload = time()

        if random_ids:
            self._processing_block_id = create_id("pb", "fake")
            self._execution_block_id = create_id("eb", "fake")
            self._ignore_config_db = True
        elif not ignore_config_db and self._processing_block_id != "pb-unknown":
            self.config = ska_sdp_config.Config()
            for txn in self.config.txn():
                processing_block = txn.processing_block.get(self._processing_block_id)
                self._execution_block_id = processing_block.eb_id
                execution_block = txn.execution_block.get(self._execution_block_id)
                self._subarray_id = execution_block.subarray_id

        logger.info("Processing Block: %s", self._processing_block_id)
        logger.info("Execution Block: %s", self._execution_block_id)
        logger.info("Subarray: %s", self._subarray_id)

        self.metrics = get_metrics_list(metrics)
        if self.enable_stats and "stats" not in self.metrics:
            self.metrics.append("stats")

        self._flow_watcher()

    @staticmethod
    @override
    def create(argv) -> "SignalDisplayMetrics":  # pragma: no cover
        """Create instance of SignalDisplayMetrics."""
        parser = argparse.ArgumentParser(
            description="",
            prog="SignalDisplayMetrics",
        )
        parser.add_argument(
            "--use-random-ids",
            action="store_true",
            help="Whether to use random IDs or not",
        )
        parser.add_argument(
            "--ignore-config-db",
            action="store_true",
            help="Whether to ignore the config DB for the IDs",
        )
        parser.add_argument(
            "--disable-kafka",
            action="store_true",
            help="Do not send data to Kafka",
        )
        parser.add_argument(
            "--metrics",
            type=str,
            help="Comma seperated list of metrics",
            default="stats",
        )
        args = parser.parse_args(argv)

        return SignalDisplayMetrics(
            random_ids=args.use_random_ids,
            ignore_config_db=args.ignore_config_db,
            disable_kafka=args.disable_kafka,
            metrics=args.metrics.split(","),
        )

    @override
    async def process(self, dataset: Visibility) -> None:
        """
        Processes the given visibilities dataset.

        :param dataset: A dataset read from Plasma.
        :returns: True to stop processing, false to continue reading payloads.
        """

        logger.info("Received dataset")

        # Log error, but ignore it
        try:
            if self.first_scan:
                await self._send_updates("start")
                self.first_scan = False

            self.received_payloads += 1
            count_new_slices = len(dataset.coords["time"].values)
            self.received_time_slices += count_new_slices
            logger.info("Received time slices: %d/%d", count_new_slices, self.received_time_slices)
            with Timer(logger, "Process Dataset"):
                local_dataset = VisibilityHelper(
                    dataset,
                    self._processing_block_id,
                    self._subarray_id,
                    self._execution_block_id,
                )

                send = process_metrics(self.flows, local_dataset)

                if not self._disable_kafka:
                    with Timer(logger, "Send all data"):
                        for flow, outputs in send:
                            producer = await self._get_and_connect_producer(flow)
                            for partition, value in enumerate(outputs):
                                await producer.send(
                                    data=metrics_to_dict(value),
                                    topic=flow.topic,
                                    encoding=flow.data_format,
                                    partition=partition,
                                )
            if self.enable_stats:
                with Timer(logger, "Send Stats"):
                    await self._send_updates("receiving")

            self.time_since_last_payload = time()
        except Exception as exc:  # pylint: disable=broad-exception-caught; #pragma: no cover
            logger.exception(exc)

    async def start_scan(self, scan_id: int) -> None:
        """
        Called when a new scan has started. The default implementation ignores
        this event, but subclasses might want to react to this.

        :param scan_id: the ID of the scan that has started.
        """
        if isinstance(scan_id, numpy.int64):
            scan_id = scan_id.item()
        self._scan_id = scan_id
        await self._send_updates("new")
        self._finish_writing()

    async def end_scan(self, scan_id: int) -> Any:
        """
        Called when a scan has ended. The default implementation ignores this
        event, but subclasses might want to react to this.

        :param scan_id: the ID of the scan that has ended.
        """
        if isinstance(scan_id, numpy.int64):
            scan_id = scan_id.item()
        self._scan_id = scan_id
        await self._send_updates("end_scan")
        self._finish_writing()

    def _finish_writing(self):  # pragma: no cover
        self.received_payloads = 0
        self.received_time_slices = 0
        self.time_since_last_payload = time()

    def _flow_watcher(self):
        self.flows = self._get_flows()
        if self._ignore_config_db:
            return
        threading.Thread(target=self._flow_thread).start()

    def _flow_thread(self):
        for watcher in self.config.watcher():
            for txn in watcher.txn():
                for _, flows in self.flows.items():
                    for flow in flows:
                        state = txn.flow.state(flow.flow).get()
                        flow.state = state

    def _get_flows(self):
        if self._ignore_config_db:
            default_host = os.environ.get("SDP_KAFKA_HOST", "localhost")
            flows = {
                metric: [
                    SignalDisplayFlow(
                        name=f"flow-{metric}",
                        topic=f"metrics-{metric_to_str(metric)}-{self._subarray_id}",
                        host=KafkaUrl(f"kafka://{default_host}:9092"),
                        data_format="msgpack_numpy",
                        metric_type=metric,
                        nchan_avg=1,
                        additional_windows=0,
                        rounding_sensitivity=5,
                        flow=None,
                        state=None,
                    )
                ]
                for metric in self.metrics
            }
            self._flow_get_unique_servers(flows)
            return flows

        if self.config is None:
            self.config = ska_sdp_config.Config()
        flows = {}
        # pylint: disable-next=too-many-nested-blocks
        for txn in self.config.txn():
            for flow in txn.flow.list_keys():
                if flow.pb_id == self._processing_block_id and flow.kind == "data-queue":
                    config_flow = txn.flow.get(flow)

                    for source in config_flow.sources:
                        signal_display_flow = self._flow_check_flow_source(config_flow, source)
                        if signal_display_flow is None:
                            continue

                        if signal_display_flow.metric_type in flows:
                            flows[signal_display_flow.metric_type].append(signal_display_flow)
                        else:
                            flows[signal_display_flow.metric_type] = [signal_display_flow]

        self._flow_get_unique_servers(flows)
        return flows

    def _flow_check_flow_source(self, config_flow, source) -> SignalDisplayFlow | None:
        if source.function not in [
            "SignalDisplayMetrics",
            "SignalDisplayMetrics.Stats",
        ]:
            return None

        metric_type = source.parameters["metric_type"]
        if metric_type in self.metrics or metric_type == "stats":
            return SignalDisplayFlow(
                name=config_flow.key.name,
                topic=config_flow.sink.topics,
                host=config_flow.sink.host,
                data_format=config_flow.sink.format,
                metric_type=metric_type,
                nchan_avg=source.parameters["nchan_avg"],
                additional_windows=source.parameters.get("additional_windows", 0),
                rounding_sensitivity=source.parameters.get("rounding_sensitivity", 5),
                flow=config_flow,
                state=None,
            )
        return None

    def _flow_get_unique_servers(self, flows: dict[str, list[SignalDisplayFlow]]):
        for _, flow_entries in flows.items():
            for flow in flow_entries:
                if flow.host not in self._producers:
                    logger.warning("Kafka host: %s -> %s", flow.host, flow.host.bootstrap_address)
                    self._producers[flow.host] = DataQueueProducer(
                        server=flow.host.bootstrap_address,
                        message_max_bytes=KAFKA_MAX_BYTES,
                        encoding=Encoding.MSGPACK_NUMPY,
                    )
                if flow.metric_type == "stats":
                    self._flow_stats = flow

    @override
    async def close(self):
        """Signal this processor to stop its activity and return"""
        self._scan_id = 0
        await self._send_updates("stopped")
        if self._disable_kafka:
            logger.info("Skipping flush on disabled kafka")  # pragma: no cover
        else:
            for producer in self._producers.values():
                await producer._producer_stop()
        if self.config:
            self.config.close()

    async def _send_updates(self, state):
        if self._disable_kafka or not self.enable_stats or self._flow_stats is None:
            logger.warning("Not sending stats")
            return

        try:
            data = VisReceiveStatistics(
                time=time(),
                type="visibility_receive",
                state=state,
                processing_block_id=self._processing_block_id,
                execution_block_id=self._execution_block_id,
                subarray_id=self._subarray_id,
                scan_id=self._scan_id,
                payloads_received=self.received_payloads,
                time_slices=self.received_time_slices,
                time_since_last_payload=time() - self.time_since_last_payload,
            )
            producer = await self._get_and_connect_producer(self._flow_stats)

            await producer.send(
                data=asdict(data),
                encoding=self._flow_stats.data_format,
                topic=self._flow_stats.topic,
                partition=0,
            )
        except Exception as error:  # pylint: disable=broad-exception-caught; #pragma: no cover
            logger.exception(error)

    # helper utilities:
    async def _get_and_connect_producer(self, flow: SignalDisplayFlow) -> DataQueueProducer:
        producer = self._producers[flow.host]
        if producer._producer is None:
            await producer._producer_start()
        return producer


def main():  # pragma: no cover
    """A do nothing main."""
    print("This file should not be run on it's own, an example to run this file would be:")
    print(
        "plasma-processor "
        "realtime.receive.processors.sdp.metrics.signal_display_metrics.SignalDisplayMetrics "
        "--plasma_socket /plasma/socket "
        "--readiness-file /tmp/processor_ready"
        "--metrics all"
    )


if __name__ == "__main__":  # pragma: no cover
    main()
