"""Utility functions for the metrics."""

import numpy as np


def average_channels(data: np.ndarray, nchan_avg: int) -> np.ndarray:
    """Average given data over the amount of channels"""
    num_rows, num_columns, num_polarisations = data.shape
    padded_size = (
        num_columns + (nchan_avg - num_columns % nchan_avg) % nchan_avg
    )  # Size after padding

    padded_data = np.pad(
        data.astype(complex),
        ((0, 0), (0, padded_size - num_columns), (0, 0)),
        mode="constant",
        constant_values=np.NaN,
    )

    # pylint: disable-next=too-many-function-args
    reshaped_data = padded_data.reshape(num_rows, -1, nchan_avg, num_polarisations)
    data = np.nanmean(reshaped_data, axis=2)

    return data
