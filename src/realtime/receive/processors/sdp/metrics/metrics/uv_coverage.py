"""The calculation for the uv coverage graphs"""

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
    UVCoveragePayload,
)

from realtime.receive.processors.sdp.metrics.utils.flow import SignalDisplayFlow
from realtime.receive.processors.sdp.metrics.utils.visibility import VisibilityHelper

from .utils import average_channels


def uv_coverage(
    flows: list[SignalDisplayFlow], dataset: VisibilityHelper
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """Processes the weight and uvw values."""
    return [
        (
            flow,
            [
                _process_uv_coverage_data(weights.values, uvws.values, dataset, window, flow)
                for window in flow.windows()
            ],
        )
        for weights, uvws in zip(dataset.dataset["weight"], dataset.dataset["uvw"])
        for flow in flows
    ]


# pylint: disable-next=too-many-locals
def _process_uv_coverage_data(
    weights: np.ndarray,
    uvws: np.ndarray,
    dataset: VisibilityHelper,
    window: tuple[int, int, int],
    flow: SignalDisplayFlow,
) -> MetricPayload:
    """
    Produces uv/weight distribution data.
    Takes the UVW values and divides by mid frequency to convert values to number of wavelengths.
    """
    _, _, nchan_avg = window
    baselines, _, _ = dataset.baselines
    polarisations = dataset.polarisations

    payload = MetricPayload(
        data_type=MetricDataTypes.UV_COVERAGE,
        processing_block_id=dataset.processing_block_id,
        spectral_window=SpectralWindow(
            freq_min=dataset.spectral_window["freq_min"],
            freq_max=dataset.spectral_window["freq_max"],
            count=dataset.spectral_window["count"],
            channels_id=0,  # object request int, getting a string
            spectral_window_id=dataset.spectral_window["spectral_window_id"],
            start=dataset.spectral_window["start"],
            stride=dataset.spectral_window["stride"],
        ),
    )
    if nchan_avg > 1:
        weights = average_channels(weights, nchan_avg)

    min_freq = payload.spectral_window.freq_min
    max_freq = payload.spectral_window.freq_max
    mid_frequency = (min_freq + max_freq) / 2
    wavelength = 299792458 / mid_frequency
    mid_frequency_index = int(payload.spectral_window.count / nchan_avg / 2)

    u_values = np.round(uvws[:, 0] / wavelength, flow.rounding_sensitivity)
    v_values = np.round(uvws[:, 1] / wavelength, flow.rounding_sensitivity)
    w_values = np.round(uvws[:, 2] / wavelength, flow.rounding_sensitivity)

    payload.spectral_window.count = weights.shape[1]

    for baseline_index, baseline in enumerate(baselines):
        for polarisation_index, polarisation in enumerate(polarisations):
            weight_value = np.round(
                weights[baseline_index, mid_frequency_index, polarisation_index],
                flow.rounding_sensitivity,
            )

            payload.data.append(
                UVCoveragePayload(
                    baseline=baseline,
                    polarisation=polarisation,
                    weight=weight_value,
                    u=u_values[baseline_index],
                    v=v_values[baseline_index],
                    w=w_values[baseline_index],
                )
            )

    return payload
