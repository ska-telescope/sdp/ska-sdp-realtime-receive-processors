"""The calculations for the Lag Plots."""

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    DataPayload,
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
)

from realtime.receive.processors.sdp.metrics.utils.config import SPECTRUM_MAX, SPECTRUM_MIN
from realtime.receive.processors.sdp.metrics.utils.flow import SignalDisplayFlow
from realtime.receive.processors.sdp.metrics.utils.visibility import VisibilityHelper

from .utils import average_channels

# All metrics are setup the same way, and most are filtered in a similar way
# Disable this check for simplicity
# pylint: disable=duplicate-code


def lag_plots(
    flows: list[SignalDisplayFlow], dataset: VisibilityHelper
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """The Lag Plot calculation."""
    return [
        (
            flow,
            [_process_lag_plots_data(data.values, dataset, window) for window in flow.windows()],
        )
        for data in dataset.data
        for flow in flows
    ]


# pylint: disable-next=too-many-locals
def _process_lag_plots_data(
    data: np.ndarray, dataset: VisibilityHelper, window: tuple[int, int, int]
):
    """Generate plot of cross-correlation power vs lag."""
    spectrum_start, spectrum_end, nchan_avg = window
    baselines, _, _ = dataset.baselines
    polarisations = dataset.polarisations

    payload = MetricPayload(
        data_type=MetricDataTypes.LAG_PLOT,
        processing_block_id=dataset.processing_block_id,
        spectral_window=SpectralWindow(
            freq_min=dataset.spectral_window["freq_min"],
            freq_max=dataset.spectral_window["freq_max"],
            count=dataset.spectral_window["count"],
            channels_id=0,  # object request int, getting a string
            spectral_window_id=dataset.spectral_window["spectral_window_id"],
            start=dataset.spectral_window["start"],
            stride=dataset.spectral_window["stride"],
        ),
    )

    if nchan_avg > 1:
        data = average_channels(data, nchan_avg)

    start_index, end_index = dataset.spectrum_indexes(spectrum_start, spectrum_end, data.shape[1])

    data = data[:, start_index:end_index, :]

    # Filter out autocorrelations
    valid_baseline_mask = np.array([a1 != a2 for a1, a2 in (b.split("_") for b in baselines)])
    data = data[valid_baseline_mask]  # Only use cross-correlation data
    valid_baselines = [b for i, b in enumerate(baselines) if valid_baseline_mask[i]]

    fft_data = np.abs(np.fft.fftshift(np.fft.ifft(data, axis=1), axes=1))
    fft_data /= np.max(fft_data, axis=1, keepdims=True)
    fft_data *= 360

    payload.spectral_window.count = fft_data.shape[1]
    if not (spectrum_start == SPECTRUM_MIN and spectrum_end == SPECTRUM_MAX):
        payload.spectral_window.freq_min = spectrum_start
        payload.spectral_window.freq_max = spectrum_end

    for baseline_idx, baseline in enumerate(valid_baselines):
        for pol_idx, polarisation in enumerate(polarisations):
            payload.data.append(
                DataPayload(
                    baseline=baseline,
                    polarisation=polarisation,
                    data=fft_data[baseline_idx, :, pol_idx].tolist(),
                )
            )

    return payload
