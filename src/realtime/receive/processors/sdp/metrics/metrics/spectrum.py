"""The calculation for the Spectrum plots"""

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
    SpectrumPayload,
)

from realtime.receive.processors.sdp.metrics.utils.config import SPECTRUM_MAX, SPECTRUM_MIN
from realtime.receive.processors.sdp.metrics.utils.flow import SignalDisplayFlow
from realtime.receive.processors.sdp.metrics.utils.visibility import VisibilityHelper

from .utils import average_channels

# All metrics are setup the same way, and most are filtered in a similar way
# Disable this check for simplicity
# pylint: disable=duplicate-code


def spectrum(
    flows: list[SignalDisplayFlow], dataset: VisibilityHelper
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """The calculation for the Spectrum Plots"""
    return [
        (
            flow,
            [
                _process_spectrum_data(data.values, dataset, window, flow)
                for window in flow.windows()
            ],
        )
        for data in dataset.data
        for flow in flows
    ]


# pylint: disable-next=too-many-locals
def _process_spectrum_data(
    data, dataset: VisibilityHelper, window: tuple[int, int, int], flow: SignalDisplayFlow
):
    spectrum_start, spectrum_end, nchan_avg = window
    _, antenna1, antenna2 = dataset.baselines

    auto = np.array(antenna1) == np.array(antenna2)
    data = data[auto, :, :]  # Filter data for autocorrelations

    payload = MetricPayload(
        data_type=MetricDataTypes.SPECTRUM,
        processing_block_id=dataset.processing_block_id,
        spectral_window=SpectralWindow(
            freq_min=dataset.spectral_window["freq_min"],
            freq_max=dataset.spectral_window["freq_max"],
            count=dataset.spectral_window["count"],
            channels_id=0,  # object request int, getting a string
            spectral_window_id=dataset.spectral_window["spectral_window_id"],
            start=dataset.spectral_window["start"],
            stride=dataset.spectral_window["stride"],
        ),
    )

    if nchan_avg > 1:
        data = average_channels(data, nchan_avg)

    start_index, end_index = dataset.spectrum_indexes(spectrum_start, spectrum_end, data.shape[1])

    payload.spectral_window.count = data.shape[1]
    if not (spectrum_start == SPECTRUM_MIN and spectrum_end == SPECTRUM_MAX):
        payload.spectral_window.freq_min = spectrum_start
        payload.spectral_window.freq_max = spectrum_end

    spectrum_mean = np.mean(data[:, start_index:end_index, :], axis=0)

    magnitude = np.abs(spectrum_mean)
    angle = np.angle(spectrum_mean)

    payload.spectral_window.count = magnitude.shape[0]

    payload.data = [
        SpectrumPayload(
            polarisation=pol,
            power=np.round(magnitude[:, idx], flow.rounding_sensitivity).tolist(),
            angle=np.round(angle[:, idx], flow.rounding_sensitivity).tolist(),
        )
        for idx, pol in enumerate(dataset.polarisations)
    ]

    return payload
