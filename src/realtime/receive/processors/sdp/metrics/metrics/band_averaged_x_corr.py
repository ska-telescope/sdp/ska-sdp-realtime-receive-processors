"""The calculations for the Band Averaged Cross Correlation Power."""

# pylint: disable=duplicate-code,too-many-locals,too-many-function-args

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    DataPayload,
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
)

from realtime.receive.processors.sdp.metrics.utils.flow import SignalDisplayFlow
from realtime.receive.processors.sdp.metrics.utils.visibility import VisibilityHelper


def band_averaged_x_corr(
    flows: list[SignalDisplayFlow], dataset: VisibilityHelper
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """The Lag Plot calculation."""
    return [
        (flow, [_process_band_averaged_x_corr_data(data.values, dataset)])
        for data in dataset.data
        for flow in flows
    ]


def _process_band_averaged_x_corr_data(
    data: np.ndarray,
    dataset: VisibilityHelper,
):
    """Generate plot of cross-corellation power vs lag.

    :param data: visibility data array
    :param baselines: baselines
    :param polarisation: polarisations
    :param processing_block: The data block to be processed
    """
    baselines, _, _ = dataset.baselines
    polarisations = dataset.polarisations

    payload = MetricPayload(
        data_type=MetricDataTypes.BAND_AVERAGED_X_CORR,
        processing_block_id=dataset.processing_block_id,
        spectral_window=SpectralWindow(
            freq_min=dataset.spectral_window["freq_min"],
            freq_max=dataset.spectral_window["freq_max"],
            count=dataset.spectral_window["count"],
            channels_id=0,  # object request int, getting a string
            spectral_window_id=dataset.spectral_window["spectral_window_id"],
            start=dataset.spectral_window["start"],
            stride=dataset.spectral_window["stride"],
        ),
    )

    for baseline_index, baseline in enumerate(baselines):
        for polarisation_index, polarisation in enumerate(polarisations):
            visibilities = data[baseline_index, :, polarisation_index]
            x_corr_power = np.abs(np.square(np.mean(visibilities)))
            payload.data.append(
                DataPayload(
                    baseline=baseline,
                    polarisation=polarisation,
                    data=x_corr_power.tolist(),
                )
            )

    return payload
