"""Metric calculation functions"""

import logging

from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricDataTypes, MetricPayload

from realtime.receive.processors.sdp.metrics.metrics.amplitude import amplitude
from realtime.receive.processors.sdp.metrics.metrics.band_averaged_x_corr import (
    band_averaged_x_corr,
)
from realtime.receive.processors.sdp.metrics.metrics.lag_plots import lag_plots
from realtime.receive.processors.sdp.metrics.metrics.phase import phase
from realtime.receive.processors.sdp.metrics.metrics.spectrum import spectrum
from realtime.receive.processors.sdp.metrics.metrics.uv_coverage import uv_coverage
from realtime.receive.processors.sdp.metrics.utils.flow import SignalDisplayFlow
from realtime.receive.processors.sdp.metrics.utils.utilities import Timer
from realtime.receive.processors.sdp.metrics.utils.visibility import VisibilityHelper

logger = logging.getLogger(__name__)


def process_metrics(
    metric_flows: dict[MetricDataTypes, list[SignalDisplayFlow]],
    local_dataset: VisibilityHelper,
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """Process list of metrics and dataset."""
    send = []
    for metric, flows in metric_flows.items():
        try:
            with Timer(logger, f"Metric: {metric}"):
                match metric:
                    case MetricDataTypes.SPECTRUM:
                        send.extend(spectrum(flows, local_dataset))
                    case MetricDataTypes.PHASE:
                        send.extend(phase(flows, local_dataset))
                    case MetricDataTypes.AMPLITUDE:
                        send.extend(amplitude(flows, local_dataset))
                    case MetricDataTypes.LAG_PLOT:
                        send.extend(lag_plots(flows, local_dataset))
                    case MetricDataTypes.BAND_AVERAGED_X_CORR:
                        send.extend(band_averaged_x_corr(flows, local_dataset))
                    case MetricDataTypes.UV_COVERAGE:
                        send.extend(uv_coverage(flows, local_dataset))
        except Exception:  # pylint: disable=broad-exception-caught
            logger.exception("Metric %s created an exception", metric)
    return send


def metrics_to_dict(payload: MetricPayload) -> dict:
    """Convert the MetricPayload to dict"""
    output = {
        "data_type": payload.data_type,
        "processing_block_id": payload.processing_block_id,
        "spectral_window": {
            "freq_min": payload.spectral_window.freq_min,
            "freq_max": payload.spectral_window.freq_max,
            "count": payload.spectral_window.count,
            "channels_id": payload.spectral_window.channels_id,
            "spectral_window_id": payload.spectral_window.spectral_window_id,
            "start": payload.spectral_window.start,
            "stride": payload.spectral_window.stride,
        },
        "timestamp": payload.timestamp,
        "data": [],
    }

    if payload.data_type == MetricDataTypes.SPECTRUM:
        output["data"] = [
            {
                "polarisation": data.polarisation,
                "power": data.power,
                "angle": data.angle,
            }
            for data in payload.data
        ]
    elif payload.data_type == MetricDataTypes.UV_COVERAGE:
        output["data"] = [
            {
                "baseline": data.baseline,
                "polarisation": data.polarisation,
                "weight": data.weight,
                "u": data.u,
                "v": data.v,
                "w": data.w,
            }
            for data in payload.data
        ]
    elif payload.data_type in (
        MetricDataTypes.PHASE,
        MetricDataTypes.AMPLITUDE,
    ):
        output["data"] = [
            {
                "baseline": data.baseline,
                "polarisation": data.polarisation,
                "data": data.data,
                "component": data.component,
            }
            for data in payload.data
        ]
    else:
        output["data"] = [
            {
                "baseline": data.baseline,
                "polarisation": data.polarisation,
                "data": data.data,
            }
            for data in payload.data
        ]

    return output
