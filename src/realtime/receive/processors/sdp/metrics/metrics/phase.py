"""The calculations required for the Phase graphs."""

import numpy as np
from ska_sdp_dataqueues.schemas.signal_display_metrics import (
    DataAndComponentPayload,
    MetricDataTypes,
    MetricPayload,
    SpectralWindow,
)

from realtime.receive.processors.sdp.metrics.utils.config import SPECTRUM_MAX, SPECTRUM_MIN
from realtime.receive.processors.sdp.metrics.utils.flow import SignalDisplayFlow
from realtime.receive.processors.sdp.metrics.utils.visibility import VisibilityHelper

from .utils import average_channels

# All metrics are setup the same way, and most are filtered in a similar way
# Disable this check for simplicity
# pylint: disable=duplicate-code


# pylint: disable-next=too-many-locals
def phase(
    flows: list[SignalDisplayFlow], dataset: VisibilityHelper
) -> list[tuple[SignalDisplayFlow, MetricPayload]]:
    """Phase graph calculation"""
    return [
        (
            flow,
            [_process_phase_data(data.values, dataset, window, flow) for window in flow.windows()],
        )
        for data in dataset.data
        for flow in flows
    ]


def _process_phase_data(
    data: np.ndarray,
    dataset: VisibilityHelper,
    window: tuple[int, int, int],
    flow: SignalDisplayFlow,
):
    spectrum_start, spectrum_end, nchan_avg = window
    baselines, _, _ = dataset.baselines
    polarisations = dataset.polarisations

    payload = MetricPayload(
        data_type=MetricDataTypes.PHASE,
        processing_block_id=dataset.processing_block_id,
        spectral_window=SpectralWindow(
            freq_min=dataset.spectral_window["freq_min"],
            freq_max=dataset.spectral_window["freq_max"],
            count=dataset.spectral_window["count"],
            channels_id=0,  # object request int, getting a string
            spectral_window_id=dataset.spectral_window["spectral_window_id"],
            start=dataset.spectral_window["start"],
            stride=dataset.spectral_window["stride"],
        ),
    )

    if nchan_avg > 1:
        data = average_channels(data, nchan_avg)

    start_index, end_index = dataset.spectrum_indexes(spectrum_start, spectrum_end, data.shape[1])

    angles = np.round(np.angle(data[:, start_index:end_index, :]), flow.rounding_sensitivity)
    imag_components = np.round(
        np.imag(data[:, start_index:end_index, :]), flow.rounding_sensitivity
    )

    payload.spectral_window.count = angles.shape[1]
    if not (spectrum_start == SPECTRUM_MIN and spectrum_end == SPECTRUM_MAX):
        payload.spectral_window.freq_min = spectrum_start
        payload.spectral_window.freq_max = spectrum_end

    payload.data = [
        DataAndComponentPayload(
            baseline=baseline,
            polarisation=polarisation,
            data=angles[baseline_index, :, polarisation_index].tolist(),
            component=imag_components[baseline_index, :, polarisation_index].tolist(),
        )
        for baseline_index, baseline in enumerate(baselines)
        for polarisation_index, polarisation in enumerate(polarisations)
    ]

    return payload
