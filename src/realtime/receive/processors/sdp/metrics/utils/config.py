"""Configuration to be used within the application"""

import enum
import logging

from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricDataTypes

logger = logging.getLogger(__name__)

AVG_CHAN_SPECTRUM = 1
AVG_CHAN_PHASE_AND_AMPLITUDE = 1

# This range should always select all data as this range is outside our
# available spectrum
SPECTRUM_MIN = 0
SPECTRUM_MAX = 1e15

CORRTYPE_TO_POL = {
    5: "RR",
    6: "RL",
    7: "LR",
    8: "LL",
    9: "XX",
    10: "XY",
    11: "YX",
    12: "YY",
}


class StokesType(enum.IntEnum):
    """
    A type of stoke for correlations.

    The values correspond to the casacore::Stokes::StokesTypes enumeration for
    consistency and ease of use when writing them into a Measurement Set.
    """

    RR = 5
    RL = 6
    LR = 7
    LL = 8
    XX = 9
    XY = 10
    YX = 11
    YY = 12


ALL_METRICS = [
    MetricDataTypes.SPECTRUM,
    MetricDataTypes.PHASE,
    MetricDataTypes.AMPLITUDE,
    MetricDataTypes.LAG_PLOT,
    MetricDataTypes.BAND_AVERAGED_X_CORR,
    MetricDataTypes.UV_COVERAGE,
]


def get_metrics_list(metrics: list[str] = None):
    """Get the list of metrics from given list."""
    if metrics is None or "all" in metrics:
        return ALL_METRICS

    return_metrics = []
    for metric in metrics:
        if metric in ALL_METRICS:
            return_metrics.append(metric)
        else:
            logger.warning('Invalid metric "%s"', metric)
    return return_metrics
