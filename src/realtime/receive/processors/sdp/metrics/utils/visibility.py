"""Helper to get data from the Visibility object"""

import logging
from functools import cached_property, lru_cache

import numpy as np

from realtime.receive.processors.sdp.metrics.utils.config import StokesType

logger = logging.getLogger(__name__)


class VisibilityHelper:
    """Helper class to use a Visibility object."""

    def __init__(self, dataset, processing_block_id, subarray_id, execution_block_id):
        self.dataset = dataset
        self.processing_block_id = processing_block_id
        self.subarray = subarray_id
        self.execution_block_id = execution_block_id

    @cached_property
    def _plasma_attrs(self) -> dict:
        return self.dataset.attrs["meta"]["plasma_refs"]

    @cached_property
    def timestamp(self) -> float:
        """Get creation timestamp."""
        return self.dataset.coords["time"].item(0)

    @cached_property
    def time_index(self) -> int:
        """Get index within receive process."""
        return self._plasma_attrs["time_index_ref"].item(0)

    @cached_property
    def antennas(self) -> list:
        """Get the list of antennas and info."""
        antennas_config = self.dataset.attrs["configuration"]
        return [
            {
                "name": antennas_config.data_vars["names"].values[id],
                "diameter": antennas_config.data_vars["diameter"].values[id],
                "location": antennas_config.data_vars["xyz"].values[id].tolist(),
            }
            for id in antennas_config.coords["id"].values
        ]

    @cached_property
    def baselines(self) -> tuple[list[str], list[int], list[int]]:
        """Get the Baseline setup."""
        baselines = self.dataset.coords["baselines"]
        antenna1 = baselines["antenna1"].to_numpy()
        antenna2 = baselines["antenna2"].to_numpy()
        antennas = self.antennas
        return (
            [
                f"{antennas[a1]['name']}_{antennas[a2]['name']}"
                for a1, a2 in zip(antenna1, antenna2)
            ],
            antenna1,
            antenna2,
        )

    @cached_property
    def spectral_window(self) -> dict:
        """Get the Spectral Window."""
        first_beam = self._plasma_attrs["beam_ref"].to_pylist()[0]
        spectral_window_table = self._plasma_attrs["spectral_window_ref"].to_pylist()

        for channel in self._plasma_attrs["channels_ref"].to_pylist():
            if channel["channels_id"] == first_beam["channels_id"]:
                for spectral_window in spectral_window_table:
                    if spectral_window["channels_id"] == channel["channels_id"]:
                        return spectral_window
        return {}  # pragma: no cover

    @cached_property
    def polarisations(self) -> list[str]:
        """Get list of polarisations."""
        beam_ref = self._plasma_attrs["beam_ref"].to_pylist()
        for polarization in self._plasma_attrs["polarisations_ref"].to_pylist():
            if beam_ref[0]["polarizations_id"] == polarization["polarizations_id"]:
                return [StokesType[s].name for s in polarization["corr_type"]]
        return []  # pragma: no cover

    @cached_property
    def data(self):
        """Get the Visibility data."""
        return self.dataset.data_vars["vis"]

    @lru_cache
    def spectrum_indexes(
        self, spectrum_start: int, spectrum_end: int, count: int
    ) -> tuple[int, int]:
        """Locate the indexes for the spectrum given."""
        frequencies = np.linspace(
            self.spectral_window["freq_min"],
            self.spectral_window["freq_max"],
            count,
        )

        start_index = np.searchsorted(frequencies, spectrum_start, side="left")
        end_index = np.searchsorted(frequencies, spectrum_end, side="right")

        return (start_index, end_index)
