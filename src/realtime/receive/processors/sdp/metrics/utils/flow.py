"""Internal description of a flow."""

from dataclasses import dataclass

from ska_sdp_config.entity.flow import Flow

from realtime.receive.processors.sdp.metrics.utils.config import SPECTRUM_MAX, SPECTRUM_MIN

# pylint: disable=too-many-instance-attributes


@dataclass
class SignalDisplayFlow:
    """Flow configuration"""

    name: str
    topic: str
    host: str
    data_format: str
    metric_type: str
    nchan_avg: int
    additional_windows: int
    rounding_sensitivity: int
    flow: Flow
    state: dict | None

    def windows(self):
        """Get the windows from the default, and state."""
        windows = [(SPECTRUM_MIN, SPECTRUM_MAX, self.nchan_avg)]
        if self.state is not None and "windows" in self.state:
            for window in self.state["windows"]:
                windows.append((window["start"], window["end"], window["channels_averaged"]))
        return windows[: (self.additional_windows + 1)]
