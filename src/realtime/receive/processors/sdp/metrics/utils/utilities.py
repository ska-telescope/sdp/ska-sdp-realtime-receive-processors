"""Module used to convert numpy objects to suitable JSON Objects."""

import random
import time
from datetime import date

from ska_sdp_dataqueues.schemas.signal_display_metrics import MetricDataTypes


def create_id(prefix, generator="test"):
    """
    Create an ID with the given prefix.

    The ID will contain today's date and a random 5-digit number.

    :param prefix: the prefix
    :param generator: the name of the generator to use

    """
    today = date.today().strftime("%Y%m%d")
    number = random.randint(0, 99999)
    return f"{prefix}-{generator}-{today}-{number:05d}"


class Timer:
    """Simple utility class to output a time around a piece of code."""

    def __init__(self, logger, name=""):
        self.name = name
        self.start = 0
        self.logger = logger

    def __enter__(self):
        self.start = time.time()

    def __exit__(self, type_, value, traceback):
        self.logger.info("[%s]: Time taken : %0.6f s", self.name, time.time() - self.start)
        return False


def metric_to_str(metric: MetricDataTypes | str) -> str:
    """Get the string value of the metric Enum."""
    return "stats" if metric == "stats" else metric.value
