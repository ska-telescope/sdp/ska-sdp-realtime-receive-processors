"""
mswriter for realtime.receive.processors.
"""

from __future__ import annotations

import argparse
import asyncio
import concurrent.futures
import functools
import logging
import os
import warnings
from asyncio import Task
from datetime import datetime
from functools import cached_property
from pathlib import Path
from typing import Generator, Optional, Sequence, Union

import numpy as np
import pyarrow as pa
import ska_sdp_config
from aiokafka.errors import KafkaError
from astropy import units
from overrides import override
from realtime.receive.core import msutils, time_utils
from realtime.receive.core.antenna import Antenna
from realtime.receive.core.baselines import Baselines
from realtime.receive.core.scan import (
    Angle,
    Beam,
    Channels,
    Field,
    PhaseDirection,
    Polarisations,
    Scan,
    ScanType,
    SpectralWindow,
    StokesType,
)
from ska_sdp_config.entity import Flow
from ska_sdp_datamodels.configuration import Configuration
from ska_sdp_datamodels.visibility import Visibility

from realtime.receive.processors.file_executor import CommandFileExecutor
from realtime.receive.processors.sdp.base_processor import BaseProcessor
from realtime.receive.processors.sdp.ms_event_handler import (
    DataQueueNotifyMsClosedEventHandler,
    FileExecutorMsClosedEventHander,
    MsClosedEvent,
    MsClosedEventData,
    MsClosedEventHandler,
    SdpConfigMsClosedEventHandler,
)
from realtime.receive.processors.sdp.pointing_helpers import (
    PointingConsumer,
    PointingKafkaSourceConfig,
)
from realtime.receive.processors.storage import File
from realtime.receive.processors.utils.data_queue_notifications import KafkaNotifier

logger = logging.getLogger(__name__)


# pylint: disable=too-many-instance-attributes
class MSWriterProcessor(BaseProcessor):
    """A Processor that writes incoming payloads into a Measurement Set"""

    def __init__(
        self,
        output_ms_path: Union[str, Path],
        use_plasmastman: bool = False,
        timestamp_output: bool = False,
        pointing_source_config: Optional[PointingKafkaSourceConfig] = None,
        event_handlers: Optional[list[MsClosedEventHandler]] = None,
        sdp_config: ska_sdp_config.Config | None = None,
    ):
        super().__init__()

        self._output_ms_template = Path(output_ms_path)
        self.received_payloads = 0
        self.process_pollrate = 1.0
        self.mswriter: Optional[msutils.MSWriter] = None
        self._ms_file: File | None = None
        self.ms_written = 0
        self._use_plasmastman = use_plasmastman
        self._timestamp_output = timestamp_output
        self._current_scan_id: int | None = None
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)

        self._closed_event = MsClosedEvent(event_handlers or [])
        self._closed_event.start()

        self._pointing_runner = _PointingConsumerRunner(pointing_source_config)

        self._end_scans_received = 0  # This is incremented when the end_scan is called
        self._end_scans_expected = 0  # This is a proxy for the number of receivers

        self._sdp_config = sdp_config
        self.pb_id = os.environ.get("SDP_PB_ID")
        if self.pb_id and self._sdp_config:
            dp_key = Flow.Key(
                pb_id=self.pb_id, kind="data-product", name="vis-receive-mswriter-processor"
            )
            for txn in self._sdp_config.txn():
                txn.flow.take_ownership_if_not_alive(dp_key)
                if txn.flow.state(dp_key).exists():
                    txn.flow.state(dp_key).update({"status": "FLOWING", "paths": []})
                else:
                    logger.warning("flow state missing for {dp_key}, creating")
                    txn.flow.state(dp_key).create({"status": "FLOWING", "paths": []})

    @staticmethod
    def _create_parser() -> argparse.ArgumentParser:
        parser = argparse.ArgumentParser(
            description="A processor that writes Measurement Sets",
            prog="MSWriterProcessor",
        )
        parser.add_argument("output_ms", help="The Measurement Set to write the output to")
        parser.add_argument(
            "--use_plasmastman",
            action="store_true",
            help=(
                "Whether to use the PlasmaStMan to store Plasma Object"
                " references in the measurement set"
            ),
        )
        parser.add_argument(
            "--timestamp-output",
            action="store_true",
            help="Whether to generate timestamped ms local filenames",
        )
        parser.add_argument(
            "-c",
            "--ms-command",
            type=lambda s: s.split(" "),
            help="Command to execute on each new Measurement Set",
        )
        data_queues_group = parser.add_argument_group("General Data Queue arguments")
        data_queues_group.add_argument(
            "--kafka-server",
            type=str,
            help="The Kafka server to connect to",
        )
        data_queues_group.add_argument(
            "--ms-closed-notification-topic",
            type=str,
            help="The topic where MS closing events will be posted to",
        )
        pointing_group = parser.add_argument_group("Pointing arguments")
        pointing_group.add_argument(
            "--commanded-pointing-topic",
            type=str,
            help="The topic where commanded pointings are stored",
        )
        pointing_group.add_argument(
            "--actual-pointing-topic",
            type=str,
            help="The topic where actual pointings are stored",
        )
        pointing_group.add_argument(
            "--source-offset-topic",
            type=str,
            help="The topic where source offsets are stored",
        )
        return parser

    @staticmethod
    @override
    def create(argv: Sequence[str]) -> MSWriterProcessor:
        parser = MSWriterProcessor._create_parser()
        args, _ = parser.parse_known_args(argv)

        event_handlers: list[MsClosedEventHandler] = []
        if args.ms_command:
            event_handlers.append(
                FileExecutorMsClosedEventHander(CommandFileExecutor(args.ms_command))
            )
        eb_id = None
        sdp_config: ska_sdp_config.Config | None = None
        if pb_id := os.environ.get("SDP_PB_ID"):
            sdp_config_event_handler = SdpConfigMsClosedEventHandler(pb_id)
            event_handlers.append(sdp_config_event_handler)
            sdp_config = ska_sdp_config.Config()
            for txn in sdp_config.txn():
                pb = txn.processing_block.get(pb_id)
                if pb:
                    eb_id = pb.eb_id

        kafka_server = args.kafka_server or os.getenv("SDP_KAFKA_HOST")

        # Kafka notifier for MS closed events
        if kafka_server and args.ms_closed_notification_topic and eb_id:
            notifier = KafkaNotifier(kafka_server, args.ms_closed_notification_topic)
            notify_handler = DataQueueNotifyMsClosedEventHandler(notifier, eb_id)
            event_handlers.append(notify_handler)

        pointing_source: Optional[PointingKafkaSourceConfig] = None
        if kafka_server and args.commanded_pointing_topic and args.actual_pointing_topic:
            if not args.source_offset_topic:
                warnings.warn(
                    "Not providing a source offset topic results in populating the MS "
                    "POINTING.SOURCE_OFFSET with `actual - commanded` rather than a true source"
                    " offset",
                    DeprecationWarning,
                )

            pointing_source = PointingKafkaSourceConfig(
                kafka_server,
                args.commanded_pointing_topic,
                args.actual_pointing_topic,
                args.source_offset_topic,
            )

        return MSWriterProcessor(
            output_ms_path=args.output_ms,
            use_plasmastman=args.use_plasmastman,
            timestamp_output=args.timestamp_output,
            pointing_source_config=pointing_source,
            event_handlers=event_handlers,
            sdp_config=sdp_config,
        )

    def _generate_output_path(self, scan_id) -> str:
        """
        Generates an output path using UTC DateTime formatted extension
        from when the payload was received
        """
        # UTC Date Time Format
        timestamp_part = (
            f".{datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')}"
            if self._timestamp_output
            else ""
        )
        return (
            f"{self._output_ms_template.parent}/{self._output_ms_template.stem}.scan-{scan_id}"
            f"{timestamp_part}{self._output_ms_template.suffix}"
        )

    async def start_scan(self, scan_id: int) -> None:
        logger.info("Scan %d started", scan_id)
        self._current_scan_id = scan_id

    @override
    async def process(self, dataset: Visibility) -> None:
        """
        Runs a plasma mswriter that receives plasma payloads and constructs
        plasma measurement sets.
        """
        logger.info("Received payload for scan %d", self._current_scan_id)
        ms_adapter = _MSVisibilityAdapter(dataset)
        scan_number = ms_adapter.scan.scan_number
        # this tries to predict the number of end of scans we should expect
        if self._end_scans_expected == 0:
            self._end_scans_expected = ms_adapter.num_slices
        elif self._end_scans_expected != ms_adapter.num_slices:
            logger.warning(
                "Received data for scan %d with %d slices, but expected %d slices",
                scan_number,
                ms_adapter.num_slices,
                self._end_scans_expected,
            )

        # We might get data for a scan for which we didn't receive a start_scan
        # (e.g., the processor was restarted, or the plasma store was full).
        if scan_number != self._current_scan_id:
            logger.warning(
                "Received data with a scan we're not writing (current=%s, received=%d), "
                "assuming start_scan was missed, continuing with current=%d",
                self._current_scan_id,
                scan_number,
                scan_number,
            )
            self._current_scan_id = scan_number

        if not self.mswriter:
            assert not self._ms_file
            ms_output_path = self._generate_output_path(scan_number)

            self._ms_file = self.storage.declare_new_file(  # pylint: disable=no-member
                ms_output_path, f"MS for scan {scan_number}"
            )
            logger.info("Writing to %s", self._ms_file.local_path)
            self.mswriter = msutils.MSWriter(
                self._ms_file.local_path,
                ms_adapter.scan,
                ms_adapter.antennas,
                ms_adapter.baselines,
                plasma_socket=self._plasma_socket if self._use_plasmastman else None,
            )

            # Run as task in the case where the connection to Kafka
            # takes too long this interrupts processing from plasma
            self._pointing_runner.start_background_task(
                ms_adapter.antennas,
                # Assuming first visibility timestamp is the earliest
                next(ms_adapter.by_time()).mjd_time,
            )

        # TODO (yan-1125) determine which beam this payload corresponds to
        # (probably based on channel id?)
        beam_index = 0  # Assuming only one beam per scan
        beam = ms_adapter.scan.scan_type.beams[beam_index]
        for data_point in ms_adapter.by_time():
            await self._arun_in_executor(
                self.mswriter.write_data,
                scan_id=ms_adapter.scan.scan_number,
                beam=beam,
                sw=ms_adapter.spectral_window,
                payload_seq_no=data_point.time_index,
                mjd_time=data_point.mjd_time,
                interval=data_point.interval,
                exposure=data_point.exposure,
                first_chan=ms_adapter.spectral_window_slice.start,
                chan_count=(
                    ms_adapter.spectral_window_slice.stop - ms_adapter.spectral_window_slice.start
                ),
                uvw=data_point.uvw,
                vis=data_point.visibilities,
                flag=data_point.flags,
            )
        self.received_payloads += 1
        self._pointing_runner.update_latest_ts(dataset)

    async def end_scan(self, scan_id: int) -> None:
        # end_scan could be called without a corresponding call to start_scan() and/or process()
        self._end_scans_received += 1
        if self._end_scans_received is not self._end_scans_expected:
            logger.info(
                "%d of %d End scans received for %d",
                self._end_scans_received,
                self._end_scans_expected,
                scan_id,
            )
            return

        logger.info("All End scans received for %d", scan_id)
        self._end_scans_received = 0
        self._end_scans_expected = 0

        logger.info("Scan %d ended", scan_id)
        if self._current_scan_id is None:
            logger.warning(
                "No call to start_scan() or process() has been issued, there is no MS to close"
            )
            return
        if self.mswriter is None:
            logger.warning("No call to process() has been issued, there is no MS to close")
            return

        await self._finish_writing()
        self._current_scan_id = None

    async def _finish_writing(self):
        assert self.mswriter is not None
        assert self._ms_file is not None

        if pointing_consumer := await self._pointing_runner.astop_and_get():
            pointings = await self._arun_in_executor(pointing_consumer.calculate_pointings)
            if pointings:
                logger.info("Writing %d pointing entries to MS", len(pointings))
                await self._arun_in_executor(self.mswriter.write_pointings, pointings)
            else:
                logger.warning("No pointing entries to write to the MS")

        self.received_payloads = 0
        ms_filename = self.mswriter.ms.name
        logger.info("Closing MS %s", ms_filename)
        await self._arun_in_executor(self.mswriter.close)
        self._ms_file.update_status("done")
        self._closed_event.invoke(
            MsClosedEventData(self._ms_file.local_path, self._ms_file.sdp_path)
        )

        self._ms_file = None
        self.mswriter = None
        logger.info("Finished writing %s", ms_filename)
        self.ms_written += 1

    @override
    async def close(self):
        """Signal this processor to stop its activity and return"""
        if self.mswriter:
            await self._finish_writing()
        self._closed_event.stop()
        if self._sdp_config:
            self._sdp_config.close()

    def _arun_in_executor(self, func, *args, **kwargs):
        return asyncio.get_running_loop().run_in_executor(
            self.executor, functools.partial(func, *args, **kwargs)
        )


class _PointingConsumerRunner:
    """
    Handles the complexity of optionally running pointing consumption
    in a background task.
    """

    def __init__(self, source: PointingKafkaSourceConfig | None):
        self.__source = source
        self._consumer_task: Optional[Task[PointingConsumer | None]] = None
        self.__latest_ts: float | None = None

    def start_background_task(self, antennas: list[Antenna], mjd_start_time: float):
        """If configured, starts consuming pointings in a background task"""
        if not self.__source or self._consumer_task:
            return

        self._consumer_task = asyncio.create_task(self._start(antennas, mjd_start_time))

    def update_latest_ts(self, dataset: Visibility):
        """Update the latest visibility timestamp that has been observed"""
        max_ts = max(time_utils.mjd_to_unix(ts.item()) for ts in dataset.coords["time"])
        if not self.__latest_ts or max_ts > self.__latest_ts:
            self.__latest_ts = max_ts

    async def astop_and_get(self):
        """If running, stops pointing consumption and returns the consumer"""
        if not self._consumer_task:
            return None

        consumer = await self._consumer_task
        if consumer:
            await consumer.stop(stop_after_ts=self.__latest_ts)

        self._consumer_task = None
        return consumer

    async def _start(self, antennas: list[Antenna], mjd_start_time: float):
        try:
            consumer = PointingConsumer(self.__source, antennas)
            await consumer.start(time_utils.mjd_to_unix(mjd_start_time))
            return consumer
        except KafkaError:
            logger.exception(
                "Unable to connect to %s, continuing without pointings",
                self.__source.server,
            )
            return None


def _plasma_refs(visibility: Visibility) -> dict[str, pa.Table]:
    return visibility.attrs["meta"]["plasma_refs"]


class _MSVisibilityTimePoint:
    """
    Helper class to encapsulate accessing data for MSWriter operations
    at a specific time index.
    Note: This class could be greatly simplified if the `plasma_refs`
    that use time indexing get moved to the Visibility `data_vars`.
    """

    def __init__(self, visibility: Visibility, time_idx: int):
        self._visibility = visibility
        self._time_idx = time_idx

    @property
    def uvw(self) -> np.ndarray:
        """Get the UVWs for the Dataset"""
        uvw = self._visibility.data_vars["uvw"][self._time_idx]
        return uvw.to_numpy()

    @property
    def interval(self) -> float:
        """Get the interval for the Dataset"""
        times = self._visibility.data_vars["integration_time"]
        return times.item(self._time_idx)

    @property
    def exposure(self) -> float:
        """Get the exposure for the Dataset"""
        exposures: np.ndarray = self._plasma_attrs["exposures_ref"]
        return exposures.item(self._time_idx)

    @property
    def time_index(self) -> int:
        """Get the time_index for the Dataset"""
        time_indexes: np.ndarray = self._plasma_attrs["time_index_ref"]
        return time_indexes.item(self._time_idx)

    @property
    def mjd_time(self) -> float:
        """Get the timestamp of the data as a MJD"""
        return self._visibility.time.item(self._time_idx)

    @property
    def visibilities(self) -> np.ndarray:
        """Get the raw visibilities of the data"""
        return self._visibility.vis[self._time_idx].values

    @property
    def flags(self) -> np.ndarray:
        """Get the raw flags of the data"""
        return self._visibility.flags[self._time_idx].values

    @property
    def _plasma_attrs(self):
        return self._visibility.attrs["meta"]["plasma_refs"]


# pylint: disable-next=too-few-public-methods
class _MSVisibilityAdapter:
    """
    Adapter to encapsulate the logic required to construct required
    objects for MSWriter operations from a visibiliy datamodel.
    """

    @staticmethod
    def _adapt_scan(visibility: Visibility) -> Scan:
        fields = {
            f["field_id"]: Field(
                f["field_id"],
                PhaseDirection(
                    Angle(f["phase_dir"]["ra"], units.rad),
                    Angle(f["phase_dir"]["dec"], units.rad),
                    f["phase_dir"]["reference_time"],
                    f["phase_dir"]["reference_frame"],
                ),
            )
            for f in _plasma_refs(visibility)["field_ref"].to_pylist()
        }

        spectral_window_table = _plasma_refs(visibility)["spectral_window_ref"].to_pylist()
        spectral_window = [
            SpectralWindow(
                sw["spectral_window_id"],
                sw["count"],
                sw["start"],
                sw["freq_min"],
                sw["freq_max"],
                sw["stride"],
            )
            for sw in spectral_window_table
        ]
        channels = {
            c["channels_id"]: Channels(
                c["channels_id"],
                spectral_windows=[
                    spectral_window[index]
                    for index, sw in enumerate(spectral_window_table)
                    if sw["channels_id"] == c["channels_id"]
                ],
            )
            for c in _plasma_refs(visibility)["channels_ref"].to_pylist()
        }
        polarisations = {
            p["polarizations_id"]: Polarisations(
                p["polarizations_id"], [StokesType[s] for s in p["corr_type"]]
            )
            for p in _plasma_refs(visibility)["polarisations_ref"].to_pylist()
        }
        beams = [
            Beam(
                b["beam_id"],
                b["function"],
                channels[b["channels_id"]],
                polarisations[b["polarizations_id"]],
                fields[b["field_id"]],
            )
            for b in _plasma_refs(visibility)["beam_ref"].to_pylist()
        ]

        scan_id = int(_plasma_refs(visibility)["scan_id_ref"][0])
        # TODO(yan-1125) this isn't currently used by MSWriter, but we should
        # populate it properly
        scan_type_id = str(scan_id)
        return Scan(scan_id, ScanType(scan_type_id, beams))

    @staticmethod
    def _adapt_antennas(visibility: Visibility) -> list[Antenna]:
        antennas_config: Configuration = visibility.attrs["configuration"]

        # TODO(yan-1125) Populate these
        fixed_delays = {}
        niao = 0.0

        def as_geocentric(xyz_location):
            return {
                "geocentric": {
                    "x": xyz_location[0],
                    "y": xyz_location[1],
                    "z": xyz_location[2],
                }
            }

        antennas = [
            Antenna(
                station_id=id,
                interface="test",
                station_label=antennas_config.data_vars["names"].values[id],
                diameter=antennas_config.data_vars["diameter"].values[id],
                location=as_geocentric(antennas_config.data_vars["xyz"].values[id]),
                fixed_delays=fixed_delays,
                niao=niao,
            )
            for id in antennas_config.coords["id"].values
        ]

        return antennas

    @staticmethod
    def _adapt_baselines(visibility: Visibility) -> Baselines:
        baselines = visibility.coords["baselines"]
        return Baselines(
            baselines["antenna1"].to_numpy(),
            baselines["antenna2"].to_numpy(),
        )

    def __init__(self, visibility: Visibility) -> None:
        self._visibility = visibility
        self._scan = self._adapt_scan(visibility)
        self._antennas = self._adapt_antennas(visibility)
        self._baselines = self._adapt_baselines(visibility)

    @property
    def scan(self) -> Scan:
        """Get the scan information."""
        return self._scan

    @property
    def antennas(self) -> list[Antenna]:
        """Get the antenna list."""
        return self._antennas

    @property
    def baselines(self) -> Baselines:
        """Get the baseline antenna index combinations."""
        return self._baselines

    @cached_property
    def spectral_window(self) -> SpectralWindow:
        """
        Get the spectral window that the received dataset belongs to
        """
        freqs = self._visibility.frequency
        for window in self.scan.scan_type.beams[0].channels.spectral_windows:
            if window.freq_min <= freqs[0] and window.freq_max >= freqs[-1]:
                return window
        raise ValueError("No spectral window found for the data set")

    @cached_property
    def spectral_window_slice(self) -> slice:
        """
        Get the slice of the spectral window that the received
        dataset represents
        """
        freqs = self._visibility.frequency
        sw_freqs = self.spectral_window.frequencies
        for index, freq in enumerate(sw_freqs):
            if freq == freqs[0]:
                start = index
                return slice(start, start + len(freqs))
        raise ValueError("No spectral window slice found for the data set")

    @cached_property
    def num_slices(self):
        """
        Get the number of spectral window slices
        THis is a proxy for the number of expected receivers.

        It is required as the processor has to know that it should
        wait until all the receivers have sent their data before shutting
        the file.
        """

        return len(self.spectral_window.frequencies) // (
            self.spectral_window_slice.stop - self.spectral_window_slice.start
        )

    def by_time(self) -> Generator[_MSVisibilityTimePoint, None, None]:
        """Iterate over data points by time"""
        for time_idx in range(len(self._visibility.coords["time"])):
            yield _MSVisibilityTimePoint(self._visibility, time_idx=time_idx)
