"""
dataset_queuing_processor for realtime.receive.processors.rpc.
"""

from __future__ import annotations

import argparse
import logging
from typing import Sequence

import astropy.units as u
import numpy as np
from astropy.time import TimeDelta
from ska_sdp_datamodels.visibility.vis_model import Visibility

from realtime.receive.processors.sdp.base_processor import BaseProcessor
from realtime.receive.processors.utils.block_visibility import combine_visibilities

logger = logging.getLogger(__name__)


class FreqAccumulatingProcessor(BaseProcessor):
    """
    A processor that buffers visibilities over a specified frequency window before
    combining them using an outer join.

    Attributes:
        num_subbands:   The number of subbands to accumulate into.
                        A sub-band is a group of contiguous frequency channels.
        time_limit:     The maximum time to wait for all the sub-bands
                        to arrive before flushing the buffer.


    """

    @staticmethod
    def create(argv: Sequence[str]) -> FreqAccumulatingProcessor:
        parser = argparse.ArgumentParser(
            description="A processor that accumulates visibilities across Frequency",
            prog="FreqAccumulatingProcessor",
        )
        parser.add_argument("--num-subbands", type=str, default="1")
        parser.add_argument("--time-limit", type=str, default=None)

        args, _ = parser.parse_known_args()
        num_subbands = int(args.num_subbands) if args.num_subbands else 1
        time_limit = u.Quantity(args.time_limit, u.s) if args.time_limit else None

        return FreqAccumulatingProcessor(num_subbands=num_subbands, time_limit=time_limit)

    def __init__(self, time_limit: TimeDelta | int = None, num_subbands: str | int = 1):
        self._visibilities_received = {}
        self._num_subbands = int(num_subbands)

        self._time_limit = (
            time_limit.to(u.s).value if isinstance(time_limit, TimeDelta) else time_limit
        )
        super().__init__()

    async def start_scan(self, scan_id: int) -> None:
        logger.info("Starting scan %s", scan_id)

    async def end_scan(self, scan_id: int) -> Visibility | None:
        logger.info("Ending scan %s", scan_id)
        for time_step, values in self._visibilities_received.items():
            logger.info("Flushing time step %s", time_step)
            sub_bands = int(values["sub_bands"])
            logger.info(
                "Time step %s has %d of %d subbands", time_step, sub_bands, self._num_subbands
            )
            return self.flush(time_step)

        return None

    def flush(self, timestamp: float) -> Visibility | None:
        """Flushes the buffer returning a combined visibility."""
        res = None
        if timestamp in self._visibilities_received:

            res = combine_visibilities(self._visibilities_received[timestamp]["visibilities"])
            del self._visibilities_received[timestamp]

        return res

    async def process(self, dataset: Visibility) -> Visibility | None:

        res = None
        current_time = dataset.time.values[0]

        if current_time not in self._visibilities_received:
            logger.info("Creating new entry for time %s", current_time)
            self._visibilities_received[current_time] = {"sub_bands": "0", "visibilities": []}

        if current_time in self._visibilities_received:
            logger.info("Adding to existing entry for time %s", current_time)

            number_of_sub_bands = int(self._visibilities_received[current_time]["sub_bands"])
            number_of_sub_bands += 1
            self._visibilities_received[current_time]["sub_bands"] = str(number_of_sub_bands)
            self._visibilities_received[current_time]["visibilities"].append(dataset)

            for time_step, values in self._visibilities_received.items():
                sub_bands = int(values["sub_bands"])
                if sub_bands == self._num_subbands:
                    logger.info(
                        "Time step %s has received %s of %s subbands ... flushing",
                        time_step,
                        sub_bands,
                        self._num_subbands,
                    )
                    res = self.flush(time_step)
                    return res

                this_entry_time = values["visibilities"][0].time.values[0]
                delta = current_time - this_entry_time

                if self._time_limit is not None and delta >= self._time_limit:
                    logger.warning("Time limit exceeded for time %s", current_time)
                    res = self.flush(time_step)
                    return res
        else:
            logger.warning("Time %s not found", current_time)


class TimeAccumulatingProcessor(BaseProcessor):
    """
    A processors that buffers visibilities over a specified time window before
    combining using an outer join.

    Attributes:
        time_window: The time window over which visibilities are buffered.


    """

    @staticmethod
    def create(argv: Sequence[str]) -> TimeAccumulatingProcessor:
        parser = argparse.ArgumentParser(
            description="A processor that accumulates visibilities on time",
            prog="TimeAccumulatingProcessor",
        )
        parser.add_argument("--time-interval", type=str)
        args, _ = parser.parse_known_args()
        return TimeAccumulatingProcessor(time_interval=u.Quantity(args.time_interval, u.s))

    def __init__(self, time_interval: TimeDelta):
        self._visibilities_received = []
        self.__barrier_time = float("NaN")
        self._time_interval = time_interval.to(u.s).value
        super().__init__()

    async def start_scan(self, scan_id: int) -> None:
        pass

    async def end_scan(self, scan_id: int) -> Visibility | None:
        return self.flush()

    def flush(self) -> Visibility | None:
        """Flushes the buffer returning a combined visibility."""
        res = None
        if len(self._visibilities_received) > 0:
            res = combine_visibilities(self._visibilities_received)
            self._visibilities_received.clear()
        return res

    async def process(self, dataset: Visibility) -> Visibility | None:
        # select an initial time barrier
        if np.isnan(self.__barrier_time):
            self.__barrier_time = dataset.time.values[0]

        res = None
        current_time = dataset.time.values[0]
        if current_time - self.__barrier_time >= self._time_interval:
            self.__barrier_time = current_time
            res = self.flush()

        self._visibilities_received.append(dataset)
        return res
