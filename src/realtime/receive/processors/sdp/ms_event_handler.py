"""Code pertaining to MSWriterProcessor events."""

from __future__ import annotations

import asyncio
import logging
from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from datetime import datetime

import ska_sdp_config
from overrides import override
from ska_sdp_config.entity import Flow

from realtime.receive.processors.file_executor import FileExecutor
from realtime.receive.processors.utils.data_queue_notifications import (
    KafkaNotifier,
    NotificationMessage,
)

logger = logging.getLogger(__name__)


class MsClosedEventHandler(metaclass=ABCMeta):
    """Event that occurs after a measurement set is finalized and closed."""

    def start(self):
        """Optional hook for logic to execute prior to receiving events"""

    @abstractmethod
    def invoke(self, data: MsClosedEventData):
        """Called when a measurement set has been closed"""
        raise NotImplementedError

    def stop(self):
        """Optional hook for logic to execute after all events have been received"""


@dataclass
class MsClosedEventData:
    """Data related to the MsClosed event"""

    local_path: str
    """The path to the measurement set on the local filesystem"""

    sdp_path: str | None
    """The path to the measurement set in the context of SDP (if running)"""


class MsClosedEvent(MsClosedEventHandler):
    """
    Propegates an MsClosedEvent to multiple handlers that also
    ensures that exceptions in one handler don't prevent execution
    of other handlers.
    """

    # pylint: disable=broad-exception-caught

    def __init__(self, handlers: list[MsClosedEventHandler]):
        self.__handlers = handlers
        self.__started_handlers: list[MsClosedEventHandler] = []

    @override
    def start(self):
        for handler in self.__handlers:
            try:
                handler.start()
                self.__started_handlers.append(handler)
            except Exception:
                logger.exception("Failed to start %s handler", type(handler).__name__)

    @override
    def invoke(self, data: MsClosedEventData):
        for handler in self.__started_handlers:
            try:
                handler.invoke(data)
            except Exception:
                logger.exception(
                    "Failed to run %s on %s handler",
                    MsClosedEventHandler.invoke.__name__,
                    type(handler).__name__,
                )

    @override
    def stop(self):
        for handler in self.__started_handlers:
            try:
                handler.stop()
            except BaseException:
                logger.exception("Failed to stop %s handler", type(handler).__name__)

        self.__started_handlers = []


class FileExecutorMsClosedEventHander(MsClosedEventHandler):
    """Schedules closed MSs for execution on the given file executor"""

    def __init__(self, file_executor: FileExecutor) -> None:
        self.__file_executor = file_executor

    @override
    def start(self):
        self.__file_executor.start()

    @override
    def invoke(self, data: MsClosedEventData):
        self.__file_executor.schedule(data.local_path)

    @override
    def stop(self):
        self.__file_executor.stop()


class SdpConfigMsClosedEventHandler(MsClosedEventHandler):
    """Updates SDP Config with the path of written measurement sets"""

    def __init__(self, pb_id: str) -> None:
        self.target_flow_uri = Flow.Key(
            pb_id=pb_id, kind="data-product", name="vis-receive-mswriter-processor"
        )
        self.__sdp_config = ska_sdp_config.Config()

    @override
    def invoke(self, data: MsClosedEventData):
        path = data.sdp_path
        if not path:
            path = data.local_path
            logger.warning("sdp_path not present, falling back to local_path: %s", path)

        for txn in self.__sdp_config.txn():
            flow_state = txn.flow.state(self.target_flow_uri).get()
            flow_state["paths"].append(path)
            txn.flow.state(self.target_flow_uri).update(flow_state)

    @override
    def stop(self):
        self.__sdp_config.close()


class DataQueueNotifyMsClosedEventHandler(MsClosedEventHandler):
    """Handler that sends a data queue notification when an MS is closed."""

    def __init__(self, kafka_notifier: KafkaNotifier, eb_id: str):
        self._kafka_notifier = kafka_notifier
        self._eb_id = eb_id
        self._tasks = set()

    def _schedule(self, coro):
        task = asyncio.create_task(coro)
        self._tasks.add(task)
        task.add_done_callback(self._tasks.discard)

    @override
    def start(self):
        self._schedule(self._kafka_notifier.start())

    @override
    def stop(self):
        self._schedule(self._kafka_notifier.stop())

    @override
    def invoke(self, data: MsClosedEventData):
        """Handle the MS closed event by sending a notification to Kafka."""
        message = NotificationMessage(
            file=data.local_path,
            time=datetime.now().isoformat(),
            eb_id=self._eb_id,
        )
        self._schedule(self._kafka_notifier.send_notification(message))
