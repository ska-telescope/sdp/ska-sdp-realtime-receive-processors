"""Example CopyProcessor implementation"""

from __future__ import annotations

from typing import Iterable

from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility.vis_model import Visibility

from .base_processor import BaseProcessor


class CopyProcessor(BaseProcessor):
    """A processor that only counts datasets, useful for tests"""

    @staticmethod
    def create(_argv: Iterable[str]) -> CopyProcessor:
        return CopyProcessor()

    async def process(self, dataset: Visibility) -> Visibility:
        return Visibility.constructor(
            frequency=dataset["frequency"],
            channel_bandwidth=dataset["channel_bandwidth"],
            phasecentre=dataset.phasecentre,
            configuration=dataset.configuration,
            uvw=dataset["uvw"],
            time=dataset["time"],
            vis=dataset["vis"],
            weight=dataset["weight"],
            integration_time=dataset["integration_time"],
            flags=dataset["flags"],
            baselines=dataset["baselines"],
            polarisation_frame=PolarisationFrame(dataset.attrs["_polarisation_frame"]),
            source=dataset.attrs["source"],
            scan_id=dataset.attrs["scan_id"],
            scan_intent=dataset.attrs["scan_intent"],
            execblock_id=dataset.attrs["execblock_id"],
            meta=dataset.attrs["meta"],
        )

    async def close(self):
        pass
