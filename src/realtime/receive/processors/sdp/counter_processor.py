"""Built-in CounterProcessor implementation"""

from __future__ import annotations

from typing import Iterable

from ska_sdp_datamodels.visibility.vis_model import Visibility

from realtime.receive.processors.sdp.base_processor import BaseProcessor


class CounterProcessor(BaseProcessor):
    """A processor that only counts datasets, useful for tests"""

    @staticmethod
    def create(_argv: Iterable[str]) -> CounterProcessor:
        return CounterProcessor()

    def __init__(self):
        super().__init__()
        self._datasets_processsed = 0

    async def process(self, dataset: Visibility) -> Visibility:
        self._datasets_processsed += 1
        return dataset

    async def close(self):
        pass
