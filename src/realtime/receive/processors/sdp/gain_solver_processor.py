"""Example GainSolver implementation"""

from __future__ import annotations

import argparse
from typing import Sequence

from ska_sdp_datamodels.calibration import GainTable
from ska_sdp_datamodels.visibility import Visibility
from ska_sdp_func_python.calibration.solvers import solve_gaintable

from realtime.receive.processors.sdp.base_processor import BaseProcessor


class GainSolverProcessor(BaseProcessor):
    """A processor that only counts datasets, useful for tests"""

    @staticmethod
    def create(argv: Sequence[str]) -> GainSolverProcessor:
        parser = argparse.ArgumentParser(description="A processor that solves a GainTables")
        parser.add_argument("--phase_only", default=True, action="store_true")
        parser.add_argument("--niter", type=int, default=True)
        parser.add_argument("--tol", type=float, default=0.000001)
        args, _ = parser.parse_known_args(argv)
        return GainSolverProcessor(args.phase_only, args.niter, args.tol)

    def __init__(self, phase_only: bool, niter: int, tol: float):
        self._phase_only = phase_only
        self._niter = niter
        self._tol = tol
        super().__init__()

    async def process(self, dataset: Visibility) -> GainTable:
        return solve_gaintable(
            dataset,
            phase_only=self._phase_only,
            niter=self._niter,
            tol=self._tol,
        )

    async def close(self):
        pass
