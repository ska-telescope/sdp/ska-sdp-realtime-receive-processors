"""SDP Processors modules."""

from .accumulating_processor import FreqAccumulatingProcessor, TimeAccumulatingProcessor
from .base_processor import BaseProcessor
from .counter_processor import CounterProcessor
from .mswriter_processor import MSWriterProcessor

# for backwards compatibility
NullProcessor = CounterProcessor

__all__ = [
    "BaseProcessor",
    "CounterProcessor",
    "MSWriterProcessor",
    "TimeAccumulatingProcessor",
    "FreqAccumulatingProcessor",
    "NullProcessor",
]
