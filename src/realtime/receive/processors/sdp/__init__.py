"""SDP Processors modules."""

from .accumulating_processor import FreqAccumulatingProcessor, TimeAccumulatingProcessor
from .average_processor import AverageFreqProcessor, AverageTimeProcessor
from .base_processor import BaseProcessor
from .copy_processor import CopyProcessor
from .counter_processor import CounterProcessor
from .gain_solver_processor import GainSolverProcessor
from .mswriter_processor import MSWriterProcessor
from .print_processor import PrintProcessor
from .rfi_mask_processor import RfiMaskProcessor

# for backwards compatibility
NullProcessor = CounterProcessor

__all__ = [
    "BaseProcessor",
    "CounterProcessor",
    "MSWriterProcessor",
    "TimeAccumulatingProcessor",
    "FreqAccumulatingProcessor",
    "NullProcessor",
    "AverageFreqProcessor",
    "AverageTimeProcessor",
    "CopyProcessor",
    "GainSolverProcessor",
    "PrintProcessor",
    "RfiMaskProcessor",
]
