"""Example AverageProcessor implementation"""

from __future__ import annotations

import argparse
import logging
from typing import Sequence

import numpy
from ska_sdp_datamodels.visibility.vis_model import Visibility
from ska_sdp_func_python.preprocessing import averaging_frequency, averaging_time

from realtime.receive.processors.sdp.base_processor import BaseProcessor

logger = logging.getLogger(__name__)


class AverageFreqProcessor(BaseProcessor):
    """A processor that averages visibilities in frequency."""

    @staticmethod
    def create(argv: Sequence[str]) -> AverageFreqProcessor:
        parser = argparse.ArgumentParser(
            description="A processor that applies averaging with frequency"
        )
        parser.add_argument(
            "--freqstep",
            type=int,
            required=True,
        )
        parser.add_argument(
            "--flag-threshold",
            type=int,
            default=0,
        )
        args, _ = parser.parse_known_args(argv)
        return AverageFreqProcessor(freqstep=args.freqstep, flag_threshold=args.flag_threshold)

    def __init__(self, freqstep: float, flag_threshold: float):
        self._freqstep = freqstep
        self._flag_threshold = flag_threshold
        super().__init__()

    async def process(self, dataset: Visibility) -> Visibility:
        return averaging_frequency(
            dataset, freqstep=self._freqstep, flag_threshold=self._flag_threshold
        )

    async def close(self):
        pass


class AverageTimeProcessor(BaseProcessor):
    """A processor that averages visibilities in time."""

    @staticmethod
    def create(argv: Sequence[str]) -> AverageTimeProcessor:
        parser = argparse.ArgumentParser(
            description="A processor that applies averaging with time"
        )
        parser.add_argument(
            "--timestep",
            type=int,
            required=True,
        )
        parser.add_argument(
            "--flag-threshold",
            type=float,
            default=0.5,
        )
        args, _ = parser.parse_known_args(argv)
        return AverageTimeProcessor(args.timestep, args.flag_threshold)

    def __init__(self, timestep: int, flag_threshold: float):
        self._timestep = timestep
        self._flag_threshold = flag_threshold
        super().__init__()

    def _check_timesteps(self, dataset: Visibility):
        """Check the number of timesteps in the dataset"""
        num_timesteps = len(dataset.time)
        return num_timesteps

    async def process(self, dataset: Visibility) -> Visibility:
        """Average visibilities in time

        Assumptions are:
        - The incoming dataset has the same number of timesteps.
        - The number of timesteps in the datasets is a multiple of the required timestep.
        """

        #   Check the number of timesteps in the dataset.
        #   The dataset is either the incoming dataset or the combined dataset.

        num_timesteps = self._check_timesteps(dataset)
        if num_timesteps is None:
            return None

        #   Check if the number of timesteps is less
        #   than the required timestep or not a multiple of it

        if num_timesteps < self._timestep or num_timesteps % self._timestep != 0:

            raise ValueError(
                f"The average requires {self._timestep} timesteps (or some multiple)."
            )

        average = averaging_time(
            dataset, timestep=self._timestep, flag_threshold=self._flag_threshold
        )
        average = self._update_refs(average, time_integration_factor=self._timestep)

        return average

    async def close(self):
        pass

    def _update_refs(self, dataset: Visibility, time_integration_factor: int = 1):
        """Update the plasma references to the dataset"""
        time_indexes = dataset.meta["plasma_refs"]["time_index_ref"]

        old_time_len = len(time_indexes)
        new_time_len = old_time_len // time_integration_factor
        new_time_start = time_indexes[0] // time_integration_factor

        new_time_indexes = numpy.arange(start=new_time_start, stop=new_time_start + new_time_len)

        dataset.meta["plasma_refs"]["time_index_ref"] = new_time_indexes

        return dataset
