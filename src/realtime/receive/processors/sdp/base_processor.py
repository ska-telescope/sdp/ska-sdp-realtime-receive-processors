"""
base_processor for realtime.receive.processors.
"""

from __future__ import annotations

from abc import ABCMeta, abstractmethod
from contextlib import AbstractAsyncContextManager
from typing import Any, Iterable

from overrides import overrides
from ska_sdp_datamodels.visibility import Visibility

from ..storage import Storage


class BaseProcessor(AbstractAsyncContextManager, metaclass=ABCMeta):
    """
    Base class for all Processors

    Subclasses should override the create, process, timeout and close methods
    as appropriate.
    """

    def __init__(self):
        """
        Creates a new BaseProcessor.
        """
        self._plasma_socket = "/tmp/plasma_socket"

    @property
    def storage(self) -> Storage:
        """Gets the Storage object associated to this BaseProcessor."""
        return self._storage

    @storage.setter
    def storage(self, storage: Storage):
        """Sets the Storage object associated to this BaseProcessor."""
        self._storage: Storage = storage

    @staticmethod
    @abstractmethod
    def create(argv: Iterable[str]) -> BaseProcessor:
        """
        Creates an instance of this class from the given command line
        parameters. This allows user-provided classes to have their own command
        line parsing logic, and receive arbitrary user-provided parameters.

        :param argv: A list of command line parameters.
        :returns: A new instance of this class.
        """
        raise NotImplementedError

    def set_plasma_socket(self, plasma_socket: str):
        """
        Sets the path to the socket through which communication with Plasma
        takes place. The processor should not usually need to communicate with
        Plasma, but it can if necessary.

        :param plasma_socket: The plasma socket used by this processor.
        """
        self._plasma_socket = plasma_socket

    async def start_scan(self, scan_id: int) -> None:
        """
        Called when a new scan has started. The default implementation ignores
        this event, but subclasses might want to react to this.

        :param scan_id: the ID of the scan that has started.
        """

    async def end_scan(self, scan_id: int) -> Any:
        """
        Called when a scan has ended. The default implementation ignores this
        event, but subclasses might want to react to this.

        :param scan_id: the ID of the scan that has ended.
        """

    @abstractmethod
    async def process(self, dataset: Visibility) -> Any:
        """Processes the given visibility dataset.

        Args:
            dataset (Visibility): the visibility to process

        Returns:
            Any: Value to pass to the next processor, or None to
            skip subsequent processors.
        """

    async def close(self):
        """
        Stop any pending actions in this processor.
        """

    @overrides
    async def __aenter__(self):
        return self

    @overrides
    async def __aexit__(self, *args, **kwargs):
        await self.close()
