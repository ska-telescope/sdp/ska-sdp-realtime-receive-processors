"""Example FlaggerProcessor implementation"""

from __future__ import annotations

import argparse
import ast
from typing import Sequence

import numpy as np
from ska_sdp_datamodels.visibility.vis_model import Visibility
from ska_sdp_func_python.preprocessing import apply_rfi_masks

from realtime.receive.processors.sdp.base_processor import BaseProcessor


class RfiMaskProcessor(BaseProcessor):
    """A processor that applies a static RFI mask to the flag data variables."""

    @staticmethod
    def create(argv: Sequence[str]) -> RfiMaskProcessor:
        parser = argparse.ArgumentParser(description="A processor that applies RFI Masking")
        parser.add_argument(
            "--rfi-mask",
        )
        args, _ = parser.parse_known_args(argv)
        rfi_mask = np.array(ast.literal_eval(args.rfi_mask))
        return RfiMaskProcessor(rfi_mask)

    def __init__(self, rfi_mask: np.ndarray):
        self._rfi_mask = np.array(rfi_mask)
        super().__init__()

    async def process(self, dataset: Visibility) -> Visibility:
        return apply_rfi_masks(dataset, self._rfi_mask)

    async def close(self):
        pass
