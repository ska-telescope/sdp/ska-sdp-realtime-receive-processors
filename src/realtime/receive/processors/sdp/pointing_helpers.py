"""Classes to assist with generating measurement set pointing rows"""

import asyncio
import logging
from dataclasses import dataclass
from datetime import datetime
from enum import IntEnum
from io import BytesIO
from typing import Generator

import numpy as np
from aiokafka import AIOKafkaConsumer, ConsumerRecord, ConsumerStoppedError
from realtime.receive.core.antenna import Antenna
from realtime.receive.core.pointing import Pointing
from scipy.interpolate import InterpolatedUnivariateSpline

from realtime.receive.processors.utils.numpy_validators import (
    ValidationError,
    has_subdtype_field,
    validate_structured_ndarray,
)

logger = logging.getLogger(__name__)

# We allocate with the next largest multiple of 2 above the number of samples
# needed for 5min @ 10Hz (5 * 60 * 10 = 3000)
# This will mean we allocate approx 200kB per antenna below, or ~20MB for
# an AA* telescope of 100 antenna.
DEFAULT_INITIAL_CAPACITY = 4096


class PointingType(IntEnum):
    """An enumeration of the different pointing values that can be received"""

    COMMANDED = 0
    ACTUAL = 1
    SOURCE_OFFSET = 2


class PointingCoord(IntEnum):
    """An enumeration of each pointing coordinates"""

    AZIMUTH = 0
    ELEVATION = 1


@dataclass
class PointingKafkaSourceConfig:
    """Describes where to retrieve pointings from Kafka"""

    server: str
    commanded_topic: str
    actual_topic: str
    source_offset_topic: str | None = None


class PointingConsumer:
    """
    Processes pointing samples from Kafka and stores them so pointing
    entries can be generated for writing to a measurement set pointing table.
    """

    def __init__(self, source: PointingKafkaSourceConfig, antennas: list[Antenna]):
        self.__source = source

        source_topics = [source.commanded_topic, source.actual_topic]
        if source.source_offset_topic:
            source_topics.append(source.source_offset_topic)

        self.__kafka_consumer = AIOKafkaConsumer(
            *source_topics,
            bootstrap_servers=source.server,
        )
        self.__calculator = PointingCalculator(
            len(antennas), calculate_source_offset=not source.source_offset_topic
        )
        self.__task: asyncio.Task | None = None
        self.__stop_evt = asyncio.Event()
        self.__stop_after_ts: float | None = None
        self.__latest_ts: float | None = None
        self.__antenna_ids = {antenna.name.lower(): i for i, antenna in enumerate(antennas)}

    async def start(self, first_unix_ts: float):
        """
        Starts processing pointings from Kafka, starting from messages
        sent after the given timestamp. This allows messages to be processed
        that have been sent prior to this consumer starting up.
        """
        await self.__kafka_consumer.start()

        logger.info(
            "Processing messages after %s",
            datetime.utcfromtimestamp(first_unix_ts).isoformat(),
        )

        # So we can process pointings that are already present in the message
        # queue, set our offset such that we will process all messages after
        # the given timestamp
        topic_partitions = self.__kafka_consumer.assignment()
        ts_in_ms = int(first_unix_ts * 1e3)
        offsets = await self.__kafka_consumer.offsets_for_times(
            {tp: ts_in_ms for tp in topic_partitions}
        )
        for topic_partition, offset_and_ts in offsets.items():
            if offset_and_ts is None:
                continue

            self.__kafka_consumer.seek(topic_partition, offset_and_ts.offset)
            logger.info(
                "%s offset set to %d",
                topic_partition.topic,
                offset_and_ts.offset,
            )

        self.__task = asyncio.create_task(self._populate_generator())

    def _pointing_type(self, message) -> PointingType | None:
        if message.topic == self.__source.actual_topic:
            return PointingType.ACTUAL
        if message.topic == self.__source.commanded_topic:
            return PointingType.COMMANDED
        if message.topic == self.__source.source_offset_topic:
            return PointingType.SOURCE_OFFSET
        return None

    async def _populate_generator(self):
        try:
            logger.info("Initiating Kafka subscription")

            message: ConsumerRecord
            async for message in self.__kafka_consumer:
                logger.debug("Received message from %s topic", message.topic)

                self.__latest_ts = message.timestamp / 1e3
                if self.__stop_after_ts and self.__latest_ts > self.__stop_after_ts:
                    logger.info(
                        "Stopping since latest message (at %s) has passed stopping timestamp (%s)",
                        datetime.fromtimestamp(self.__latest_ts).isoformat(),
                        datetime.fromtimestamp(self.__stop_after_ts).isoformat(),
                    )
                    self.__stop_evt.set()
                    break

                pointing_type = self._pointing_type(message)
                if pointing_type is None:
                    logger.warning(
                        "Kafka message from unknown topic (%s), dropping",
                        message.topic,
                    )
                    continue

                # Validate pointings from message
                try:
                    raw_samples = np.load(BytesIO(message.value))
                except ValueError as e:
                    logger.warning(
                        "Dropping Kafka pointings that are an invalid NPY message:\n%s", e
                    )
                    continue
                try:
                    samples = validate_structured_ndarray(
                        raw_samples,
                        [
                            [("antenna_name", np.str_), ("antenna_id", np.integer)],
                            ("ts", np.datetime64),
                            ("az", np.floating),
                            ("el", np.floating),
                        ],
                    )
                except ValidationError as e:
                    logger.warning("Dropping Kafka pointings with unsupported dtype:\n%s", e)
                    continue

                # Obtain antenna IDs (i.e., antenna indices) when antenna names are given
                if has_subdtype_field(samples, ("antenna_name", np.str_)):
                    antenna_ids = [
                        self._get_antenna_id(sample["antenna_name"]) for sample in samples
                    ]
                    if any(antenna_id is None for antenna_id in antenna_ids):
                        logger.warning("Dropping payload with unknown antenna name(s)")
                        continue
                else:
                    antenna_ids = [sample["antenna_id"] for sample in samples]

                logger.debug(
                    "Adding %d %s samples from Kafka",
                    samples.size,
                    pointing_type,
                )

                for antenna_id, sample in zip(antenna_ids, np.nditer(samples)):
                    self.__calculator.add_sample(
                        antenna_id,
                        pointing_type,
                        sample["ts"],
                        sample["az"],
                        sample["el"],
                    )
        except ConsumerStoppedError:
            logger.info("Kafka consumer has been stopped")

    def _get_antenna_id(self, antenna_name: str):
        antenna_id = self.__antenna_ids.get(antenna_name.lower())
        if antenna_id is None:
            logger.info("Antenna name %s is unknown", antenna_name)
        return antenna_id

    async def stop(self, timeout: float | None = 1.0, stop_after_ts: float | None = None):
        """Stop reading from Kafka"""
        if self.__latest_ts and stop_after_ts and self.__latest_ts > stop_after_ts:
            logger.info(
                "Stopping immediately since we've already received a message (at %s) after %s",
                datetime.fromtimestamp(self.__latest_ts).isoformat(),
                datetime.fromtimestamp(stop_after_ts).isoformat(),
            )
        elif timeout is not None:
            logger.info(
                "Waiting up to %fs to allow final messages to be received%s",
                timeout,
                (
                    f" up until {datetime.fromtimestamp(stop_after_ts).isoformat()}"
                    if stop_after_ts
                    else ""
                ),
            )
            self.__stop_after_ts = stop_after_ts
            try:
                await asyncio.wait_for(self.__stop_evt.wait(), timeout)
            except asyncio.TimeoutError:
                pass

        await self.__kafka_consumer.stop()
        await self.__task

        self.__stop_evt.clear()
        self.__latest_ts = None
        self.__stop_after_ts = None

        logger.debug("Kafka has stopped successfully")

    def calculate_pointings(self):
        """
        Calculate pointing data from the samples that were received from Kafka
        """
        return list(self.__calculator.calc_entries())

    def num_samples(self, antenna_id: int, pointing_type: PointingType):
        """
        Returns the number of samples for the given antenna and pointing type
        """
        return self.__calculator.length(antenna_id, pointing_type)


class PointingCalculator:
    """
    Handles storing pointing samples and calculating corresponding
    entries that can be used to write rows to a measurement set pointing
    table.
    """

    SPLINE_MIN_NUM_POINTS = 5

    def __init__(
        self,
        antenna_count: int,
        initial_capacity=DEFAULT_INITIAL_CAPACITY,
        calculate_source_offset=True,
    ):
        """
        Construct a new pointing calculator with the provided options:
        :param antenna_count: The number of antennas to allocate capacity for
        :param initial_capacity: The number of entries per antenna to allocate capacity for
        :param calculate_source_offset: Calculate source_offset from actual & commanded pointings
        """
        assert initial_capacity > 0

        self.__calculate_source_offset = calculate_source_offset
        num_pointing_types = 2 if calculate_source_offset else 3

        self.__antenna_count = antenna_count
        self.__timestamps = np.full(
            [antenna_count, num_pointing_types, initial_capacity],
            fill_value=np.nan,
            dtype="datetime64[ns]",
        )
        self.__pointings = np.full(
            [
                antenna_count,
                num_pointing_types,
                len(PointingCoord),
                initial_capacity,
            ],
            fill_value=np.nan,
            dtype="f8",
        )
        self.__indexes = np.zeros([antenna_count, num_pointing_types], dtype=np.int32)

    @property
    def capacity(self):
        """
        Determines how many pointing samples can be held before
        the numpy array storing them has to be increased in size.
        """
        return self.__pointings.shape[-1]

    def length(self, antenna_id: int, pointing_type: PointingType):
        """
        Returns how many entries are stored for the given antenna and
        pointing type
        """
        return self.__indexes[antenna_id, pointing_type]

    def add_sample(
        self,
        antenna_id: int,
        pointing_type: PointingType,
        timestamp: np.datetime64,
        azimuth: float,
        elevation: float,
    ):
        """
        Add a pointing sample to be used when generating Measurement Set
        Pointing table rows
        """

        next_index = self.__indexes[antenna_id, pointing_type]
        curr_capacity = self.capacity
        if next_index >= curr_capacity:
            # Double capacity
            self.__timestamps = np.pad(
                self.__timestamps,
                ((0, 0), (0, 0), (0, curr_capacity)),
                constant_values=np.nan,
            )
            self.__pointings = np.pad(
                self.__pointings,
                ((0, 0), (0, 0), (0, 0), (0, curr_capacity)),
                constant_values=np.nan,
            )

        self.__timestamps[antenna_id, pointing_type, next_index] = timestamp
        self.__pointings[antenna_id, pointing_type, PointingCoord.AZIMUTH, next_index] = azimuth
        self.__pointings[antenna_id, pointing_type, PointingCoord.ELEVATION, next_index] = (
            elevation
        )
        self.__indexes[antenna_id, pointing_type] = next_index + 1

        n_entries = self.length(antenna_id, pointing_type)
        if n_entries % 1000 == 0:
            logger.debug(
                "antenna_id=%s;%s pointings: %d entries",
                antenna_id,
                pointing_type,
                n_entries,
            )

    def calc_entries(self) -> Generator[Pointing, None, None]:
        """
        Calculate a pointing entry for each antenna at every ACTUAL pointing
        timestamp, interpolating COMMANDED values where necessary.
        """

        for antenna_id in range(self.__antenna_count):
            commanded_index = self.__indexes[antenna_id, PointingType.COMMANDED]
            actual_index = self.__indexes[antenna_id, PointingType.ACTUAL]
            logger.debug(
                "Attempting to generate pointings for antenna_id=%s"
                " with %d commanded entries and %d actual entries",
                antenna_id,
                commanded_index,
                actual_index,
            )

            if commanded_index < PointingCalculator.SPLINE_MIN_NUM_POINTS:
                logger.error(
                    "Minimum of %d points required to interpolate commanded pointings, "
                    "%d given for antenna_id=%s",
                    PointingCalculator.SPLINE_MIN_NUM_POINTS,
                    commanded_index,
                    antenna_id,
                )
                continue

            sorted_actual_indexes = np.argsort(
                self.__timestamps[antenna_id, PointingType.ACTUAL, :actual_index]
            )
            act_az = self.__pointings[
                antenna_id,
                PointingType.ACTUAL,
                PointingCoord.AZIMUTH,
                sorted_actual_indexes,
            ]
            act_el = self.__pointings[
                antenna_id,
                PointingType.ACTUAL,
                PointingCoord.ELEVATION,
                sorted_actual_indexes,
            ]

            sorted_act_ts: np.ndarray[np.datetime64] = self.__timestamps[
                antenna_id, PointingType.ACTUAL, sorted_actual_indexes
            ]
            cmd_az, cmd_el = self._interpolate_pointings(
                antenna_id, PointingType.COMMANDED, sorted_act_ts
            )

            if not self.__calculate_source_offset:
                # Check must be gated by self.__calculate_source_offset check as the size
                # of the second dimension self.__indexes is dynamically generated
                # based on that variable
                source_offset_index = self.__indexes[antenna_id, PointingType.SOURCE_OFFSET]
                if source_offset_index < PointingCalculator.SPLINE_MIN_NUM_POINTS:
                    logger.error(
                        "Minimum of %d points required to interpolate SOURCE_OFFSET pointings, "
                        "%d given for antenna_id=%s",
                        PointingCalculator.SPLINE_MIN_NUM_POINTS,
                        source_offset_index,
                        antenna_id,
                    )
                    continue

                offset_az, offset_el = self._interpolate_pointings(
                    antenna_id, PointingType.SOURCE_OFFSET, sorted_act_ts
                )
            else:
                offset_az = act_az - cmd_az
                offset_el = act_el - cmd_el

            logger.info(
                "Generating %d pointing entries for antenna_id=%s",
                actual_index,
                antenna_id,
            )

            for values in zip(
                sorted_act_ts.view(np.int64) / 1e9,  # nanoseconds -> seconds
                act_az,
                act_el,
                cmd_az,
                cmd_el,
                offset_az,
                offset_el,
                strict=True,
            ):
                yield Pointing(
                    antenna_id,
                    values[0],
                    (values[1], values[2]),
                    (values[3], values[4]),
                    source_offset=(values[5], values[6]),
                )

    def _interpolate_pointings(
        self,
        antenna_id: int,
        pointing_type: PointingType,
        timestamps: np.ndarray[np.datetime64],
    ):
        """
        Calculates commanded pointings for the given antenna at the
        specified timestamps.
        """
        pointing_index = self.__indexes[antenna_id, pointing_type]
        assert pointing_index >= PointingCalculator.SPLINE_MIN_NUM_POINTS

        _, sorted_indexes = np.unique(
            self.__timestamps[antenna_id, pointing_type, :pointing_index],
            return_index=True,
        )
        sorted_ts: np.ndarray[np.datetime64] = self.__timestamps[
            antenna_id,
            pointing_type,
            sorted_indexes,
        ]

        def interpolate_coord(coord: PointingCoord):
            model = InterpolatedUnivariateSpline(
                sorted_ts.view(np.int64),
                self.__pointings[
                    antenna_id,
                    pointing_type,
                    coord,
                    sorted_indexes,
                ],
                ext="const",
            )
            return model(timestamps.view(np.int64))

        return (
            interpolate_coord(PointingCoord.AZIMUTH),
            interpolate_coord(PointingCoord.ELEVATION),
        )
