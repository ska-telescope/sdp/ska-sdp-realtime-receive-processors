SDP Receive Processors
======================

This package contains the shared code for the SDP receiver processors.

*Processors* are programs that connect to a Plasma store
and receive payloads from the receiver pipeline.
This package aims to ease the development of such programs,
dealing with all the complexity of talking to plasma,
unpacking payloads,
allowing developers to concentrate on their business logic.


Development Requirements
------------------------

* Python 3.10 or greater
* Poetry 1.2.2 or greater

Clone the repository, then ``poetry install``
to install all the dependencies into a virtual environment.

To run the tests, execute ``make python-test``.


Docker
------

A docker image is generated in GitLab CI and stored in the SKAO CAR. The default docker image will run the MSWriterProcessor on startup with only the output file being required.

    docker compose build

Can be used to build a docker image.


The following will start an interactive MSWriterProcessor

    docker run --interactive --tty --rm --mount type=bind,source=/full-path-to-plasma-socket,destination=/plasma/socket plasma-processor-mswriter <ms output>


An alternative processor can be run by changing the entrypoint

    docker run --interactive --tty --rm --mount type=bind,source=/full-path-to-plasma-socket --entrypoint <processor class> plasma-processor-mswriter <-s plasma_socket> <ms output>
