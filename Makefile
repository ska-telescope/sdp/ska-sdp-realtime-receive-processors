DOCS_SPHINXOPTS = -W --keep-going
PYTHON_LINE_LENGTH = 99

# So we don't need all our tests to be in packages
# https://github.com/PyCQA/pylint/pull/5682
PYTHON_SWITCHES_FOR_PYLINT ?= --recursive=true

PYTHON_VARS_AFTER_PYTEST ?= --benchmark-skip

include .make/base.mk
include .make/python.mk
include .make/oci.mk
include .make/dependencies.mk

CASACORE_MEASURES_DIR ?= /usr/share/casacore/data

# We use GitlabCI services in CI so only use docker compose locally
python-pre-test:
	[[ -z $$GITLAB_CI ]] \
		&& $(MAKE) docker-compose-up \
		|| echo "Not starting docker-compose containers in CI"
	./install_measures.sh "$(CASACORE_MEASURES_DIR)"

python-post-test:
	[[ -z $$GITLAB_CI ]] \
		&& $(MAKE) docker-compose-down \
		|| echo "Not stopping docker-compose containers in CI"

docs-pre-build:
	poetry config virtualenvs.create $(POETRY_CONFIG_VIRTUALENVS_CREATE)
	poetry install --only main,docs

docs-serve:
	sphinx-autobuild docs/src docs/build

docker-compose-up:
	docker compose --file docker/test-services.docker-compose.yml up --detach --wait

docker-compose-down:
	docker compose --file docker/test-services.docker-compose.yml down

python-benchmark: PYTHON_VARS_AFTER_PYTEST = --benchmark-only --benchmark-autosave
python-benchmark: python-test

python-benchmark-plot:
	mkdir -p build/plots
	python scripts/plot_mswriter_processor.py --output=build/plots/mswriter_processor.svg
	python scripts/plot_np_load.py --output=build/plots/np_load.svg
	python scripts/plot_aiokafka_throughput.py --output=build/plots/aiokafka_throughput.svg
	python scripts/plot_pointing_calculator.py --output=build/plots/pointing_calculator.svg
