ARG PYTHON_VERSION=3.10

FROM python:${PYTHON_VERSION} AS buildenv
ARG POETRY_VERSION=1.8.2
RUN pip install poetry==${POETRY_VERSION}

WORKDIR /app
COPY ./ ./
RUN ./install_measures.sh /usr/share/casacore/data
RUN poetry config virtualenvs.in-project true \
    && poetry install --only main --no-root \
    && . .venv/bin/activate \
    && pip install --no-deps .

FROM python:${PYTHON_VERSION}-slim AS runtime

# Best practice not to run as root - UID of this user will need to correspond
# with the user/UID in realtime-receive-modules (the receiver) so the same UID
# is used to access the Plasma store socket.
RUN useradd receive
USER receive

# Copy all Python packages, console scripts & data to our runtime container
COPY --from=buildenv /usr/share/casacore/data /usr/share/casacore/data
COPY --from=buildenv /app/.venv /app/.venv/
ENV PATH="/app/.venv/bin:${PATH}"

ENTRYPOINT ["plasma-processor"]
CMD ["realtime.receive.processors.sdp.mswriter_processor.MSWriterProcessor","--plasma_socket","/plasma/socket"]